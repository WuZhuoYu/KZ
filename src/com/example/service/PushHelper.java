//package com.example.service;
//
//import java.util.ArrayList;
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.util.Log;
//
//
//
///**
// * push helper
// * 鏈夊叧鎺ㄩ�佸姛鑳界殑甯姪绫�
// * @author 18047
// */
//public class PushHelper implements C2DMRegisterListener {
//    private static final String   TAG = "PushHelper";
//    private static final String   PREFERENCE = "com.google.android.c2dm";
//    private static final String   PREKEY     = "dm_registration";
//    private static final String   PREREPUSHDID  = "repush_devid";
//    private static final String   PRENOTICEREPUSH  = "notice_repush";
//    private static final String   PRETIME    = "time";
//    private static final String   PUSH_SERVER_ADDRESS       = "https://cellphonepush.quickddns.com/gcm/send";
//    private static final String   PUSH_SERVER_ADDRESS_MAIN  = "https://android.googleapis.com/gcm/send";
//    private final static String   PUSH_TYPE_VIDEO_MOTION        		= "VideoMotion";
//    private final static String   PUSH_TYPE_VIDEO_BLIND         		= "VideoBlind";
//    private final static String   PUSH_TYPE_ALARM_LOCAL         		= "AlarmLocal";
//    private final static String   PUSH_TYPE_NOANSWER_CALL       		= "NoAnswerCall";
//    private final static String   PUSH_TYPE_CALL_NOANSWERED     		= "CallNoAnswered";
//    private final static String   PUSH_TYPE_STORAGE_NOT_EXIST   		= "StorageNotExist";
//    private final static String   PUSH_TYPE_STORAGE_LOW_SPACE   		= "StorageLowSpace";
//    private final static String   PUSH_TYPE_STORAGE_FAILURE     		= "StorageFailure";
//
//    /// API KEY get from google
//    /// The mApiKey is not "api_key":"current_key" form google-services.json
//    private String mApiKey = "AIzaSyAiPvjX0qzfdq72G41CssfGHQAIea7tl4g";
//    private String mGcmSenderId = "163569807662";
//    
//    private final static String[] PUSHTYPE = {PUSH_TYPE_VIDEO_MOTION,PUSH_TYPE_VIDEO_BLIND,PUSH_TYPE_ALARM_LOCAL,
//        PUSH_TYPE_NOANSWER_CALL,PUSH_TYPE_STORAGE_NOT_EXIST,PUSH_TYPE_STORAGE_LOW_SPACE,PUSH_TYPE_STORAGE_FAILURE};
//    private static PushHelper  pushHelper;
//    private final Boolean mEvent = new Boolean(false);
//
//    public static synchronized PushHelper instance(){
//        if(pushHelper == null){
//            pushHelper = new PushHelper();
//        }
//        return pushHelper;
//    }
//    
//    private PushHelper() {
//    }
//
//    public String getApiKey() {
//        return mApiKey;
//    }
//
//    public String getGcmSenderId() {
//        return mGcmSenderId;
//    }
//
//    /**
//     * 鑾峰彇registerID锛�30澶╄繃鏈熼渶瑕侀噸鏂颁粠google鑾峰彇
//     * Get registerID, 30 days later, should get from google again.
//     * @param context
//     * @return  null: error 鑾峰彇澶辫触
//     */  
//    public  String  getRegisterID(Context context) {
//        String registerID = null;
//        SharedPreferences preference = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
//        registerID = preference.getString(PREKEY, null);
//        long regTime = preference.getLong(PRETIME, System.currentTimeMillis());
//        long extTime = System.currentTimeMillis() - regTime;
//        long time = (long)3600*24*30*1000;//30澶�,杩囨湡
//        if( extTime - time > 0) {
//            registerID = null;
//        }
//        if(registerID == null || registerID.equals("")) {
//            // 寮傚父鎹曡幏锛岄槻姝㈤儴鍒嗗浗鍐呮墜鏈轰笉鏀寔璋锋瓕妗嗘灦锛屽鑷村穿婧�
//        	try	{
//        		 Log.d(TAG, "go to regsister");
//        		 C2DMessaging.register(context, mGcmSenderId, this);
//			}catch (Exception e) {
//				return null;
//			}
//             
//            try {
//                 synchronized (mEvent) {
//                     Log.d(TAG, "wait.");
//                     mEvent.wait(10000);
//                     Log.d(TAG, "notify.");
//                 }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            
//            registerID = C2DMessaging.getRegistrationId(context);
//            if(registerID == null || registerID.equals("")) {
//                registerID = null;
//            }
//        }
//        return registerID;
//    }
//
//    /**
//     * 鑾峰彇鏄惁闇�瑕佹樉绀洪噸鏂拌闃呯殑鎻愮ず锛堟洿鏂扮▼搴忓悗绗竴娆¤繘鍏ョ洃瑙嗙晫闈㈡椂鐨勬彁绀烘锛�
//     * @param context
//     * @return
//     */
//    public boolean  isNoticeRePush(Context context) {
//        SharedPreferences preference = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
//        return preference.getBoolean(PRENOTICEREPUSH, true);
//    }
//
//    /**
//     * 璁剧疆閲嶆柊璁㈤槄鐨勬彁绀哄凡鏄剧ず杩囷紝涓嬫涓嶅啀鎻愮ず
//     * @param context
//     */
//    public void setHasNoticedRePush(Context context) {
//        SharedPreferences preference = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = preference.edit();
//        editor.putBoolean(PRENOTICEREPUSH, false);
//        editor.apply();
//    }
//    
//    /**
//     * 鑾峰彇闇�瑕侀噸鏂拌闃呯殑璁惧ID闆嗗悎    缁勮鏍煎紡锛氳澶嘔D锛氳澶嘔D
//     * @param context
//     * @return  杩斿洖null:琛ㄧず绗竴娆℃洿鏂拌闃咃紝闇�瑕佽缃緟鏇存柊鐨勮澶嘔D闆嗗悎  绌洪泦鍚堬細琛ㄧず寰呮洿鏂扮殑璁惧宸叉洿鏂板畬姣�   鏈夊厓绱犵殑闆嗗悎锛氳〃绀轰綑涓嬪緟鏇存柊鐨勮澶嘔D
//     */
//    public ArrayList<Integer>  getRePushDeviceID(Context context) {
//        ArrayList<Integer> devIds = null;
//        String rePushDeviceID = null;
//        SharedPreferences preference = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
//        rePushDeviceID = preference.getString(PREREPUSHDID, null);
//        if (rePushDeviceID == null) {
//            devIds = null; 
//        } else if(rePushDeviceID.equals("")) {
//            devIds = new ArrayList<Integer>();
//        } else {
//            devIds = new ArrayList<Integer>();
//            String[] ids = rePushDeviceID.split(":");
//            for(int i =0;i< ids.length;i++) {
//              devIds.add(Integer.parseInt(ids[i]));
//            }
//        }
//        return devIds;
//    }
//    
//    
//    /**
//     * 鏇存柊闇�瑕侀噸鏂拌闃呯殑璁惧闆嗗悎
//     * @param context
//     * @param hasPushedDid
//     */
//    public void updateRePushDeviceID(Context context,ArrayList<Integer> hasPushedDid)
//    {
//        if(hasPushedDid == null || hasPushedDid.isEmpty())
//        {
//            return;
//        }
//        String rePushDeviceID = null;
//        SharedPreferences preference = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = preference.edit();
//        try
//        {
//            rePushDeviceID = preference.getString(PREREPUSHDID, null);
//            if(rePushDeviceID == null)
//            {
//                editor.putString(PREREPUSHDID, "");
//                editor.apply();
//            }
//            else
//            {
//                StringBuffer sb = new StringBuffer();
//                ArrayList<Integer>  allPushDid = getRePushDeviceID(context);
//                if(allPushDid != null)
//                {
//                	allPushDid.removeAll(hasPushedDid);
//	                for(Integer did : allPushDid)
//	                {
//	                    sb.append(did).append(":");
//	                }
//	                String remainDid = sb.substring(0, sb.length() -1);
//	                editor.putString(PREREPUSHDID, remainDid);
//	                editor.apply();
//                }
//            } 
//        }
//        catch (Exception e)
//        {
//            editor.putString(PREREPUSHDID, "");
//            editor.apply();
//        }
//    }
//    
//    
//    /**
//     * 璁剧疆闇�瑕侀噸鏂拌闃呯殑璁惧ID
//     * @param context
//     * @param rePushedDids
//     */
//    public void setRePushDeviceID(Context context,ArrayList<Integer> rePushedDids)
//    {
//        SharedPreferences preference = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = preference.edit();
//        if(rePushedDids == null || rePushedDids.isEmpty())
//        {
//            editor.putString(PREREPUSHDID, "");
//            editor.apply();
//            return;
//        }
//        StringBuffer sb = new StringBuffer();  
//        for(Integer did : rePushedDids)
//        {
//            sb.append(did).append(":");
//        }
//        String pushDids = sb.substring(0, sb.length() -1);
//        editor.putString(PREREPUSHDID, pushDids);
//        editor.apply();
//    }
//
//    /**
//     * 鑾峰彇璁㈤槄鏃惰缃殑鏈夋晥鏃堕棿澶╂暟
//     * @param context
//     * @param devID
//     */
//    public int getPeriodDay(Context context,int devID) {
//        SharedPreferences preference = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
//        String day = preference.getString(String.valueOf(devID), null);
//        if(day != null && !day.equals("")) {
//            return Integer.parseInt(day);
//        } else  {
//            return 0;
//        }
//    }
//
//    /**
//     * 浠巊oogle鑾峰彇registerID鎴愬姛鍚庣殑鍥炶皟閫氱煡
//     */
//    @Override
//    public void getRegisiterId(String id)
//    {
//        synchronized (mEvent) {
//            mEvent.notify();
//        }
//    }
//}
