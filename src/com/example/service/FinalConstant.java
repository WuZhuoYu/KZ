package com.example.service;

public class FinalConstant {
	// 用户账户
	public static final String USERNAME = "admin";
	public static final String USERNAME0 = "admin";
	public static final String PASSWORD = "admin123";
	
	// 系统超时时间
	public static final long WAIT_TIME = 10000;
	
	// HTTP通信相关
	public static final String ENCODING = "GB2312";
	public static final String QUERY_DATA = "queryData";
	public static final String QUERY_STATE = "queryState";
	public static final String QUERY_PARAMS = "queryParams";
	public static final String CONTROL = "control";
	public static final String SET_PARAMS = "setParams";
	
	public static final String SENSOR_DATA_HEAD = "SENSOR";
	public static final String CONTROL_STATE_HEAD = "EQUIPMENT";
	public static final String PARAMS_DATA_HEAD = "PARAMS";
	public static final String CONTROL_SUCCESS = "success";
	
	public static final int[] EQUIPMENT = {0, 1, 2, 3, 4};
	public static final int ON = 1;
	public static final int OFF = 0;
	
	public static final int QUERY_BACK_DATA = 1;
	public static final String BACK_INFO = "backMessageFromServer";
	public static final int CONTROL_BACK_DATA = 2;
	public static final String CONTROL_INFO = "controlbackMessageFromServer";
	
	public static final int TIME_OUT = 3;
	
	public static final String PC_BACK_INFO = "backMessageFromServer";
	public static final int PC_QUERY_BACK_DATA = 1;
	
	/**批次下拉菜单请求信息*/
	public static final int GT_QUERY_BACK_DATA = 1;
	
	/**批次下拉菜单返回信息*/
	public static final String GT_BACK_INFO = "backMessageFromServer";
	
	/**提示请求信息*/
	public static final int NOTICE_QUERY_BACK_DATA = 1;	
	
	/** 返回提示 */
	public static final String NOTICE_BACK_INFO = "backMessageFromServer";
	
	/**监测点1四圣村查询请求服务器的命令信息*/
	public static  final  String SQZ1FIND_REQUEST_SERVER = "SQZ1SHOW";
	
	/**监测点1四圣村查询请求服务器的返回命令信息*/
	public static  final  String SQZ1FIND_REBACK_SERVER = "SQZ1SHOW_REBACK";
	
	
	/**监测点2乌杨村查询请求服务器的命令信息*/
	public static  final  String SQZ2FIND_REQUEST_SERVER = "SQZ2SHOW";
	
	/**监测点2乌杨村查询请求服务器的返回命令信息*/
	public static  final  String SQZ2FIND_REBACK_SERVER = "SQZ2SHOW_REBACK";
	
	/**监测点3南雅新全村查询请求服务器的命令信息*/
	public static  final  String SQZ3FIND_REQUEST_SERVER = "SQZ3SHOW";
	
	/**监测点3南雅新全村查询请求服务器的返回命令信息*/
	public static  final  String SQZ3FIND_REBACK_SERVER = "SQZ3SHOW_REBACK";
	
	
	/**临江气象站查询请求服务器的命令信息*/
	public static  final  String QXZFIND_REQUEST_SERVER = "QXZSHOW";
	
	/**临江气象站查询请求服务器的返回命令信息*/
	public static  final  String QXZFIND_REBACK_SERVER = "QXZSHOW_REBACK";
	
	
	/**硬件状态查询请求服务器的命令信息*/
	public static  final  String DEVICEFIND_REQUEST_SERVER = "DEVICEFIND";
	
	/**硬件状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICEFIND_REBACK_SERVER = "DEVICEFIND_REBACK";
	
	/**硬件状态查询请求服务器的命令信息*/
	public static  final  String DEVICE1FIND_REQUEST_SERVER = "DEVICE1FIND";
	
	/**硬件状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICE1FIND_REBACK_SERVER = "DEVICE1FIND_REBACK";
	/**设备状态改变请求服务器的命令信息*/
	public static  final  String DEVICECHANGE_REQUEST_SERVER = "DEVICECHANGE";
	
	/**种植信息提交请求服务器的命令信息*/
	public static final String PLANTSUBMIT_REQUEST_SERVER = "PLANTSUBMIT";
	/**种植信息提交请求服务器的返回命令信息*/
	public static final String PLANTSUBMIT_REBACK_SERVER = "PLANTSUBMIT_REBACK";
	
	/**电磁阀1状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE1_REBACK_SERVER = "DEVICECHANGE1_REBACK";
	/**电磁阀2状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE2_REBACK_SERVER = "DEVICECHANGE2_REBACK";
	/**电磁阀3状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE3_REBACK_SERVER = "DEVICECHANGE3_REBACK";
	/**电磁阀4状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE4_REBACK_SERVER = "DEVICECHANGE4_REBACK";
	/**电磁阀5状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE5_REBACK_SERVER = "DEVICECHANGE5_REBACK";
	/**电磁阀6状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE6_REBACK_SERVER = "DEVICECHANGE6_REBACK";
	/**电磁阀7状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE7_REBACK_SERVER = "DEVICECHANGE7_REBACK";
	/**网络参数请求*/
	public static final String NETPARA_REQUEST_SERVER = "NETPARASHOW";
	/**网络参数请求返回*/
	public static final String NETPARA_REBACK_SERVER = "NETPARA_REBACK";
	
	/*政府端事件信息请求参数*/
	public static final String REQUEST_EVENTINFO="REQUEST_EVENTINFO";
	/*政府端事件信息请求返回参数*/
	public static final String REQUEST_EVENTINFO_REBACK="REQUEST_EVENTINFO_REBACK";
	
	/*政府端历史事件信息请求参数*/
	public static final String REQUEST_OLD_EVENTINFO="REQUEST_OLD_EVENTINFO";
	/*政府端历史事件信息请求返回参数*/
	public static final String REQUEST_OLD_EVENTINFO_REBACK="REQUEST_OLD_EVENTINFO_REBACK";
	
	/*临江镇意见信息请求*/
	public static final String REQUEST_SUGGEST_LJ="REQUEST_SUGGEST_LJ";
	/*四圣村意见信息请求*/
	public static final String REQUEST_SUGGEST_SS="REQUEST_SUGGEST_SS";
	/*乌羊村意见信息请求*/
	public static final String REQUEST_SUGGEST_WY="REQUEST_SUGGEST_WY";
	/*南雅意见信息请求*/
	public static final String REQUEST_SUGGEST_LY="REQUEST_SUGGEST_LY";
	/*赵家意见信息返回*/
	public static final String REQUEST_SUGGEST_ZJ="REQUEST_SUGGEST_ZJ";
	/*意见信息请求返回*/
	public static final String REQUEST_SUGGEST_REBACK="REQUEST_SUGGEST_REBACK";
	/**价格上报提交请求服务器的命令信息*/
	public static final String PRICESUBMIT_REQUEST_SERVER = "PRICESUBMIT";
	/**价格上报提交请求服务器的返回命令信息*/
	public static final String PRICESUBMIT_REBACK_SERVER = "PRICESUBMIT_REBACK";
	
	/*阈值设置提交部分*/
	
	/*阈值设置气象站提交请求服务器*/
	public static final String YZSZ_QXZ_SUBMIT_REQUEST_SERVER ="YZSZSUBMIT_QXZ";
	/*阈值设置气象站提交请求返回服务器*/
	public static final String YZSZ_QXZ_SUBMIT_REBACK_SERVER = "YZSZSUBMIT_REBACK_QXZ";
	/*阈值设置墒情监测站提交请求服务器*/
	public static final String YZSZ_SQJC_SUBMIT_REQUEST_SERVER = "YZSZSUBMIT_SQJC";
	/*阈值设置墒情监测站提交请求返回服务器*/
	public static final String YZSZ_SQJC_SUBMIT_REBACK_SERVER = "YZSZSUBMIT_REBACK_SQJC";
	
	/*阈值设置查询部分*/
	
	/*阈值警报气象站查询请求服务器*/
	public static final String YZSZ_QXZ_WARN_REQUEST_SERVER = "REQUEST_WARN_QXZ";
	/*阈值警报气象站查询请求返回服务器*/
	public static final String YZSZ_QXZ_WARN_REBACK_SERVER = "REBACK_WARN_QXZ";
	/*阈值警报墒情站查询请求服务器*/
	public static final String YZSZ_SQJC_WARN_REQUEST_SERVER = "REQUEST_WARN_SQJC";
	/*阈值警报墒情站查询请求返回服务器*/
	public static final String YZSZ_SQJC_WARN_REBACK_SERVER = "REBACK_WARN_SQJC";
	
	/*阈值报警气象站数据传输标志*/
	public static final int GT_QUERY_BACK_QXZ_DATA = 1;
	/*阈值报警四圣村墒情监测站数据传输标志*/
	public static final int GT_QUERY_BACK_SQJC1_DATA = 1;
	/*阈值报警乌杨村墒情监测站数据传输标志*/
	public static final int GT_QUERY_BACK_SQJC2_DATA = 1;
	/*阈值报警南雅新全村墒情监测站数据传输标志*/
	public static final int GT_QUERY_BACK_SQJC3_DATA = 1;
	
	

	/**政府端意见反馈提交请求服务器的命令信息*/
	public static final String ZF_SUGGESTSUBMIT_REQUEST_SERVER = "ZF_SUGGESTSUBMIT";
	/**政府端意见反馈提交请求服务器的返回命令信息*/
	public static final String ZF_SUGGESTSUBMIT_REBACK_SERVER = "ZF_SUGGESTSUBMIT_REBACK";
	
	//临江部分
	/*临江未处理意见信息请求参数*/
	public static final String YH_LJ_NOT_SUGGESTSUBMIT_REQUEST_SERVER = "YH_LJ_NOT_SUGGESTSUBMIT";
	/*临江未处理意见信息请求返回参数*/
	public static final String YH_LJ_NOT_SUGGESTSUBMIT_REBACK_SERVER = "YH_LJ_NOT_SUGGESTSUBMIT_REBACK";
	/*临江已处理意见信息请求参数*/
	public static final String YH_LJ_OLD_SUGGESTSUBMIT_REQUEST_SERVER = "YH_LJ_OLD_SUGGESTSUBMIT";
	/*临江已处理意见信息请求返回参数*/
	public static final String YH_LJ_OLD_SUGGESTSUBMIT_REBACK_SERVER = "YH_LJ_OLD_SUGGESTSUBMIT_REBACK";
	
	//四圣村部分
	/*四圣村未处理意见信息请求参数*/
	public static final String YH_SS_NOT_SUGGESTSUBMIT_REQUEST_SERVER = "YH_SS_NOT_SUGGESTSUBMIT";
	/*四圣村未处理意见信息请求返回参数*/
	public static final String YH_SS_NOT_SUGGESTSUBMIT_REBACK_SERVER = "YH_SS_NOT_SUGGESTSUBMIT_REBACK";
	/*四圣村已处理意见信息请求参数*/
	public static final String YH_SS_OLD_SUGGESTSUBMIT_REQUEST_SERVER = "YH_SS_OLD_SUGGESTSUBMIT";
	/*四圣村已处理意见信息请求返回参数*/
	public static final String YH_SS_OLD_SUGGESTSUBMIT_REBACK_SERVER = "YH_SS_OLD_SUGGESTSUBMIT_REBACK";
	
	//乌杨村部分
	/*乌杨村未处理意见信息请求参数*/
	public static final String YH_WY_NOT_SUGGESTSUBMIT_REQUEST_SERVER = "YH_WY_NOT_SUGGESTSUBMIT";
	/*乌杨村未处理意见信息请求返回参数*/
	public static final String YH_WY_NOT_SUGGESTSUBMIT_REBACK_SERVER = "YH_WY_NOT_SUGGESTSUBMIT_REBACK";
	/*乌杨村已处理意见信息请求参数*/
	public static final String YH_WY_OLD_SUGGESTSUBMIT_REQUEST_SERVER = "YH_WY_OLD_SUGGESTSUBMIT";
	/*乌杨村已处理意见信息请求返回参数*/
	public static final String YH_WY_OLD_SUGGESTSUBMIT_REBACK_SERVER = "YH_WY_OLD_SUGGESTSUBMIT_REBACK";
	
	//南雅部分
	/*南雅村未处理意见信息请求参数*/
	public static final String YH_NY_NOT_SUGGESTSUBMIT_REQUEST_SERVER = "YH_NY_NOT_SUGGESTSUBMIT";
	/*南雅村未处理意见信息请求返回参数*/
	public static final String YH_NY_NOT_SUGGESTSUBMIT_REBACK_SERVER = "YH_NY_NOT_SUGGESTSUBMIT_REBACK";
	/*南雅村已处理意见信息请求参数*/
	public static final String YH_NY_OLD_SUGGESTSUBMIT_REQUEST_SERVER = "YH_NY_OLD_SUGGESTSUBMIT";
	/*南雅村已处理意见信息请求返回参数*/
	public static final String YH_NY_OLD_SUGGESTSUBMIT_REBACK_SERVER = "YH_NY_OLD_SUGGESTSUBMIT_REBACK";
	
	//赵家部分
	/*赵家未处理意见信息请求参数*/
	public static final String YH_ZJ_NOT_SUGGESTSUBMIT_REQUEST_SERVER = "YH_ZJ_NOT_SUGGESTSUBMIT";
	/*赵家未处理意见信息请求返回参数*/
	public static final String YH_ZJ_NOT_SUGGESTSUBMIT_REBACK_SERVER = "YH_ZJ_NOT_SUGGESTSUBMIT_REBACK";
	/*赵家已处理意见信息请求参数*/
	public static final String YH_ZJ_OLD_SUGGESTSUBMIT_REQUEST_SERVER = "YH_ZJ_OLD_SUGGESTSUBMIT";
	/*赵家已处理意见信息请求返回参数*/
	public static final String YH_ZJ_OLD_SUGGESTSUBMIT_REBACK_SERVER = "YH_ZJ_OLD_SUGGESTSUBMIT_REBACK";
}
