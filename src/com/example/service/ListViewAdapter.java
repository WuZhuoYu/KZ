package com.example.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.example.kxapp.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListViewAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<HashMap<String, Object>> eventinfo;
	public ListViewAdapter(Context context,ArrayList<HashMap<String, Object>> eventinfo){
		this.context=context;
		this.eventinfo=eventinfo;
		
	}
	@Override
	public int getCount() {//设置item的长度
		return eventinfo.size();
	}

	@Override
	public Object getItem(int position) {
		
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		Viewholer viewholer = null;
		if(convertView!=null) {
			convertView.getTag();
		}else {
			viewholer=new Viewholer();
			convertView=LayoutInflater.from(context).inflate(R.layout.item_zf_eventinfo,null);//加载布局
			viewholer.tv_base=(TextView) convertView.findViewById(R.id.tv_base);
			viewholer.tv_info=(TextView) convertView.findViewById(R.id.tv_info);
			viewholer.tv_name=(TextView) convertView.findViewById(R.id.tv_name);
			viewholer.tv_time=(TextView) convertView.findViewById(R.id.tv_time);
			viewholer.tv_eventname=(TextView) convertView.findViewById(R.id.tv_event_name);
			viewholer.tv_accept_unit=(TextView) convertView.findViewById(R.id.tv_accept_unit);
			viewholer.tv_eventype=(TextView) convertView.findViewById(R.id.tv_type);
			convertView.setTag(viewholer);//保存到缓存中
			
		}
		HashMap<String, Object> data=eventinfo.get(position);
		//显示输入根据响应的键值取数据并显示至UI
		Log.d("hello", "---第三次最新----"+data.toString());
		viewholer.tv_base.setText((CharSequence) data.get("TAG"));
		viewholer.tv_name.setText((CharSequence) data.get("report_person"));
		viewholer.tv_info.setText((CharSequence) data.get("event_description"));
		viewholer.tv_time.setText((CharSequence) data.get("report_time"));
		viewholer.tv_accept_unit.setText((CharSequence) data.get("accept_unit"));
		viewholer.tv_eventname.setText((CharSequence) data.get("event_name"));
		viewholer.tv_eventype.setText((CharSequence) data.get("event_type"));
		
		//LayoutInflater inflater=LayoutInflater.from(context);
		//return inflater.inflate(R.layout.item_zf_eventinfo, null);
		return  convertView;
	}
	//控件类
	class Viewholer{
		TextView tv_base,
				tv_info,
				tv_name,
				tv_time,
				tv_accept_unit,
				tv_eventname,
				tv_eventype;
	}
}
