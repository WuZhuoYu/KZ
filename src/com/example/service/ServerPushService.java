package com.example.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.kxapp.R;
import com.example.kxapp.WarnActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;


/**
@Name:后台推送阈值警报信息 *
@Description: 做了一个区分用户的flag，在service里面先执行的oncreate方法，再执行onStartCommand，但是从activity传过来的flag（string型）只能在onStartCommand里面
 			   才能被接收到，所以所有的操作都放在了onStartCommand里面来执行，oncreate方法里面只存放了加载状态栏的数据容器。实现推送是用的消息机制模式，取阈值，
 			   取传感器数据进行比较，后台一直运行message这个独立进程（见清单文件）
 			   
 			   参考：https://blog.csdn.net/hgl868/article/details/6862724
 			   	   https://blog.csdn.net/bfboys/article/details/52577243 * 
@author wuzhuoyu * 
@Version:V1.00 * 
@Create Date:2018-09-10 *
*/
//接收服务器消息送
public class ServerPushService extends Service {

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	// 获取系统消息推送线程(取阈值)
	private MessageThread_qxz message_qxz;
	private MessageThread_sqjc message_sqjc;
	// 通知点击响应
	private Intent messageintent;
	private PendingIntent pendingintent;
	// 通知栏消息
	private int messageNotificationID = 1000;// 消息ID 避免消息覆盖
	private Notification messagennotification = null; // 消息
	private NotificationManager notificationmanager = null;;// 消息管理器
	// 服务器地址
	private String serverIP = "120.79.76.116:8000";
	// HTTP请求参数
	private Map<String, Object> reqparams;
	// 请求命令
	private String cmd;
	// 获取网络配置
	private PreferencesService preservice;
	private NotificationCompat.Builder builder;
	private ArrayList<HashMap<String, Object>> thresholdValue_qxz, thresholdValue_sqjc;
	private String flag;

	@Override
	public IBinder onBind(Intent intent) {
		return null;

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		flag = intent.getStringExtra("flag");
		// Toast.makeText(getApplicationContext(), "onStartCommand"+flag, 0).show();

		if (flag.equals("1")) {
			page_one();// 福德村
		} else if (flag.equals("2")) {
			page_two();// 四圣村
		} else if (flag.equals("3")) {
			page_three();// 乌杨村
		} else if (flag.equals("4")) {
			page_four();// 南雅
		}

		return super.onStartCommand(intent, flags, startId);
	}

	// onStart方法也可以取到flag值
	// @Override
	// public void onStart(Intent intent, int startId) {
	//
	// flag = intent.getStringExtra("flag");
	// Toast.makeText(getApplicationContext(), "onStart"+flag, 0).show();
	// super.onStart(intent, startId);
	// }

	@Override
	public void onCreate() {
		messagennotification = new Notification();
		notificationmanager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		// flag=getSharedPreferences("f",
		// Context.MODE_MULTI_PROCESS).getString("flag","");
		// flag = Flag.getFlag();
		// 通知栏响应跳转到APP主页面
		messageintent = new Intent(this, WarnActivity.class);
		pendingintent = PendingIntent.getActivity(getApplicationContext(), 0, messageintent, 0);
		builder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_launcher).setAutoCancel(true)
				.setContentTitle("监测站有新情况，请点击查看！").setDefaults(Notification.DEFAULT_ALL)// 消息提示声音+振动
				.setContentIntent(pendingintent);

	}

	// 临江镇福德村
	private void page_one() {
		// 开启接收消息线程（取阈值）
		message_qxz = new MessageThread_qxz();
		message_qxz.isRunning = true;
		message_qxz.start();

		// 取传感器数据
		ljjd qxz = new ljjd();
		qxz.start();

	}

	// 白鹤街道四圣村
	private void page_two() {

		message_sqjc = new MessageThread_sqjc();
		message_sqjc.isRunning = true;
		message_sqjc.start();

		ssjd sqjc1 = new ssjd();
		sqjc1.start();
	}

	// 丰乐街道乌杨村
	private void page_three() {

		message_sqjc = new MessageThread_sqjc();
		message_sqjc.isRunning = true;
		message_sqjc.start();

		wyjd sqjc2 = new wyjd();
		sqjc2.start();
	}

	// 南雅镇新全村
	private void page_four() {

		message_sqjc = new MessageThread_sqjc();
		message_sqjc.isRunning = true;
		message_sqjc.start();

		nyjd sqjc3 = new nyjd();
		sqjc3.start();
	}

	//气象站取阈值线程
	class MessageThread_qxz extends Thread {
		public boolean isRunning = true;

		public void run() {
			while (isRunning) {
				try {
					// 5秒询问一次服务器
					Thread.sleep(5000);
					try {
						getServerMessage();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		}

		private void getServerMessage() throws Exception {
			// 获取服务器地址
			preservice = new PreferencesService(getApplicationContext());
			Map<String, String> params = preservice.getPreferences();
			String serviceIP = params.get("serviceIP");
			Log.i("serverip", serviceIP);
			if (serviceIP.equals("")) {
				preservice.save("120.79.76.116:8000", "", "", "", "", "", "");
			} else {
				serverIP = serviceIP;
			}
			// 组拼参数
			cmd = FinalConstant.YZSZ_QXZ_WARN_REQUEST_SERVER;
			reqparams = new HashMap<String, Object>();
			reqparams.put("cmd", cmd);
			String path = "http://" + serverIP + "/AppService.php";
			// 获取返回数据
			String reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
			Log.d("debugTest", "arr_data--阈值返回 " + reslut);
			if (reslut != null) {
				if (reslut.equals("1")) {
					Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
				} else {
					JSONArray array = new JSONArray(reslut);
					JSONObject cmd = (JSONObject) array.get(0);
					String str_cmd = cmd.getString("cmd");
					int len = 0;
					len = array.length();
					if (len > 1) {
						if (str_cmd.equals(FinalConstant.YZSZ_QXZ_WARN_REBACK_SERVER)) {
							if (!array.get(1).equals(false)) {
								thresholdValue_qxz = new ArrayList<HashMap<String, Object>>();
//								Log.i("info", "取阈值里面" + thresholdValue_qxz.toString());
								for (int i = 1; i < 2; i++) {
									JSONObject temp = (JSONObject) array.get(i);
									HashMap<String, Object> map = new HashMap<String, Object>();
									map.put("qxzNum", 
											Integer.parseInt(temp.getString("qxzNum")));
									map.put("et_Air_Temp_Up", 
											Integer.parseInt(temp.getString("et_Air_Temp_Up"))); // 空气温度上限
									map.put("et_Air_Temp_Floor",
											Integer.parseInt(temp.getString("et_Air_Temp_Floor"))); // 空气温度下限
									map.put("et_Air_Damp_Up", 
											Integer.parseInt(temp.getString("et_Air_Damp_Up"))); // 空气湿度上限
									map.put("et_Air_Damp_Floor",
											Integer.parseInt(temp.getString("et_Air_Damp_Floor")));// 空气湿度下限
									map.put("et_Dew_Point_Up",
											Integer.parseInt(temp.getString("et_Dew_Point_Up")));// 露点上限
									map.put("et_Dew_Point_Floor",
											Integer.parseInt(temp.getString("et_Dew_Point_Floor"))); // 露点下限
									map.put("et_Illuminance_Up", 
											Integer.parseInt(temp.getString("et_Illuminance_Up"))); // 光照度上限
									map.put("et_Illuminance_Floor",
											Integer.parseInt(temp.getString("et_Illuminance_Floor"))); // 光照度下限
									map.put("et_Solar_Radiation_Up",
											Integer.parseInt(temp.getString("et_Solar_Radiation_Up"))); // 太阳辐射上限
									map.put("et_Solar_Radiation_Floor",
											Integer.parseInt(temp.getString("et_Solar_Radiation_Floor"))); // 太阳辐射下限
									map.put("et_Surface_Soil_Moisture_Up",
											Integer.parseInt(temp.getString("et_Surface_Soil_Moisture_Up"))); // 表层土壤水分上限
									map.put("et_Surface_Soil_Moisture_Floor",
											Integer.parseInt(temp.getString("et_Surface_Soil_Moisture_Floor"))); // 表层土壤水分下限
									map.put("et_Shallow_Soil_Moisture_Up",
											Integer.parseInt(temp.getString("et_Shallow_Soil_Moisture_Up"))); // 浅层土壤水分上限
									map.put("et_Shallow_Soil_Moisture_Floor",
											Integer.parseInt(temp.getString("et_Shallow_Soil_Moisture_Floor"))); // 浅层土壤水分下限
									map.put("et_Intermediate_Soil_Moisture_Up",
											Integer.parseInt(temp.getString("et_Intermediate_Soil_Moisture_Up"))); // 中层土壤水分上限
									map.put("et_Intermediate_Soil_Moisture_Floor",
											Integer.parseInt(temp.getString("et_Intermediate_Soil_Moisture_Floor"))); // 中层土壤水分下限
									map.put("et_Deep_Soil_Moisture_Up",
											Integer.parseInt(temp.getString("et_Deep_Soil_Moisture_Up"))); // 深层土壤水分上限
									map.put("et_Deep_Soil_Moisture_Floor",
											Integer.parseInt(temp.getString("et_Deep_Soil_Moisture_Floor"))); // 深层土壤水分下限
									map.put("et_Surface_Soil_Temp_Up",
											Integer.parseInt(temp.getString("et_Surface_Soil_Temp_Up"))); // 表层土壤温度上限
									map.put("et_Surface_Soil_Temp_Floor",
											Integer.parseInt(temp.getString("et_Surface_Soil_Temp_Floor"))); // 表层土壤温度下限
									map.put("et_Shallow_Soil_Temp_Up",
											Integer.parseInt(temp.getString("et_Shallow_Soil_Temp_Up"))); // 浅层土壤温度上限
									map.put("et_Shallow_Soil_Temp_Floor",
											Integer.parseInt(temp.getString("et_Shallow_Soil_Temp_Floor"))); // 浅层土壤温度下限
									map.put("et_Intermediate_Soil_Temp_Up",
											Integer.parseInt(temp.getString("et_Intermediate_Soil_Temp_Up"))); // 中层土壤温度上限
									map.put("et_Intermediate_Soil_Temp_Floor",
											Integer.parseInt(temp.getString("et_Intermediate_Soil_Temp_Floor"))); // 中层土壤温度下限
									map.put("et_Deep_Soil_Temp_Up",
											Integer.parseInt(temp.getString("et_Deep_Soil_Temp_Up"))); // 深层土壤温度上限
									map.put("et_Deep_Soil_Temp_Floor",
											Integer.parseInt(temp.getString("et_Deep_Soil_Temp_Floor"))); // 深层土壤温度下限

									thresholdValue_qxz.add(map);
									// Log.d("helloworld", "【Runnable】就是这里阈值 -- " + thresholdValue_qxz.toString());
									Log.i("info", map.toString());

								}
							}
						}
					}
				}
			}
		}
	}
//气象站取传感器数据，并进行比较
	class ljjd extends Thread {
		private boolean isRunning = true;

		public void run() {
			while (isRunning) {
				try {
					// 获取服务器地址
					preservice = new PreferencesService(getApplicationContext());
					Map<String, String> params = preservice.getPreferences();
					String serviceIP = params.get("serviceIP");
					if (serviceIP.equals("")) {
						preservice.save("120.79.76.116:8000", "222.180.45.174", "8083", "admin", "zs123456", "", "");
					} else {
						serverIP = serviceIP;
					}

					// 组拼请求参数
					cmd = FinalConstant.QXZFIND_REQUEST_SERVER;// 查询请求命令
					reqparams = new HashMap<String, Object>();
					reqparams.put("cmd", cmd);
					String path = "http://" + serverIP + "/AppService.php";
					// 获取返回数据
					String reslut;
					reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
					Log.d("info2", "arr_data 传感器返回-- " + reslut);
					if (reslut != null) {
						if (reslut.equals("1")) {
							Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
						} else {
							JSONArray array = new JSONArray(reslut);
							JSONObject cmd = (JSONObject) array.get(0);
							String str_cmd = cmd.getString("cmd");
							int len = 0;
							len = array.length();
							if (len > 1) {
								if (str_cmd.equals(FinalConstant.QXZFIND_REBACK_SERVER)) {
									if (!array.get(1).equals(false)) {
										String m_Air_Temp_str = "";
										String m_Air_Damp_str = "";
										String m_Dew_Point_str = "";
										String m_Illuminance_str = "";
										String m_Solar_Radiation_str = "";
										String m_Surface_Soil_Moisture_str = "";
										String m_Shallow_Soil_Moisture_str = "";
										String m_Intermediate_Soil_Moisture_str = "";
										String m_Deep_Soil_Moisture_str = "";
										String m_Surface_Soil_Temp_str = "";
										String m_Shallow_Soil_Temp_str = "";
										String m_Intermediate_Soil_Temp_str = "";
										String m_Deep_Soil_Temp_str = "";
										JSONArray arr_data = (JSONArray) array.get(1);
										JSONObject info = (JSONObject) arr_data.get(0);
										
										//取传感器数据
										// 打印每一条数据确定都取到了数据
//										Log.i("info", info.toString());
										double m_Air_Temp = Double.parseDouble(info.getString("yn_air_temp"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Air_Temp));

										double m_Air_Damp = Double.parseDouble(info.getString("yn_air_humi"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Air_Damp));

										double m_Dew_Point = Double.parseDouble(info.getString("yn_dew_point"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Dew_Point));

										double m_Illuminance = Double.parseDouble(info.getString("yn_illuminance"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Illuminance));

										double m_Solar_Radiation = Double.parseDouble(info.getString("yn_radiation"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Solar_Radiation));

										double m_Surface_Soil_Moisture = Double.parseDouble(info.getString("yn_water_1content"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Surface_Soil_Moisture));

										double m_Shallow_Soil_Moisture = Double.parseDouble(info.getString("yn_water_2content"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Shallow_Soil_Moisture));

										double m_Intermediate_Soil_Moisture = Double.parseDouble(info.getString("yn_water_3content"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Intermediate_Soil_Moisture));

										double m_Deep_Soil_Moisture = Double.parseDouble(info.getString("yn_water_4content"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Deep_Soil_Moisture));

										double m_Surface_Soil_Temp = Double.parseDouble(info.getString("yn_solid_1temp"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Surface_Soil_Temp));

										double m_Shallow_Soil_Temp = Double.parseDouble(info.getString("yn_solid_2temp"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Shallow_Soil_Temp));

										double m_Intermediate_Soil_Temp = Double.parseDouble(info.getString("yn_solid_3temp"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Intermediate_Soil_Temp));

										double m_Deep_Soil_Temp = Double.parseDouble(info.getString("yn_solid_4temp"));
										Log.i("info", "---hhhh----" + String.valueOf(m_Deep_Soil_Temp));

										//取阈值
										HashMap<String, Object> sensordata = thresholdValue_qxz.get(FinalConstant.GT_QUERY_BACK_QXZ_DATA - 1);

										int et_Air_Temp_Up = (Integer) sensordata.get("et_Air_Temp_Up");// 空气温度上限

										int et_Air_Temp_Floor = (Integer) sensordata.get("et_Air_Temp_Floor");// 空气温度下限

										int et_Air_Damp_Up = (Integer) sensordata.get("et_Air_Damp_Up");// 空气湿度上限

										int et_Air_Damp_Floor = (Integer) sensordata.get("et_Air_Damp_Floor");// 空气湿度下限

										int et_Dew_Point_Up = (Integer) sensordata.get("et_Dew_Point_Up");// 露点上限

										int et_Dew_Point_Floor = (Integer) sensordata.get("et_Dew_Point_Floor");// 露点下限

										int et_Illuminance_Up = (Integer) sensordata.get("et_Illuminance_Up");// 光照度上限

										int et_Illuminance_Floor = (Integer) sensordata.get("et_Illuminance_Floor");// 光照度下限

										int et_Solar_Radiation_Up = (Integer) sensordata.get("et_Solar_Radiation_Up");// 太阳辐射上限

										int et_Solar_Radiation_Floor = (Integer) sensordata.get("et_Solar_Radiation_Floor");// 太阳辐射下限

										int et_Surface_Soil_Moisture_Up = (Integer) sensordata.get("et_Surface_Soil_Moisture_Up");// 表层土壤水分上限

										int et_Surface_Soil_Moisture_Floor = (Integer) sensordata.get("et_Surface_Soil_Moisture_Floor");// 表层土壤水分下限

										int et_Shallow_Soil_Moisture_Up = (Integer) sensordata.get("et_Shallow_Soil_Moisture_Up");// 浅层土壤水分上限

										int et_Shallow_Soil_Moisture_Floor = (Integer) sensordata.get("et_Shallow_Soil_Moisture_Floor");// 浅层土壤水分下限

										int et_Intermediate_Soil_Moisture_Up = (Integer) sensordata.get("et_Intermediate_Soil_Moisture_Up");// 中层土壤水分上限

										int et_Intermediate_Soil_Moisture_Floor = (Integer) sensordata.get("et_Intermediate_Soil_Moisture_Floor");// 中层土壤水分下限

										int et_Deep_Soil_Moisture_Up = (Integer) sensordata.get("et_Deep_Soil_Moisture_Up");// 深层土壤水分上限

										int et_Deep_Soil_Moisture_Floor = (Integer) sensordata.get("et_Deep_Soil_Moisture_Floor");// 深层土壤水分上限

										int et_Surface_Soil_Temp_Up = (Integer) sensordata.get("et_Surface_Soil_Temp_Up");// 表层土壤温度上限

										int et_Surface_Soil_Temp_Floor = (Integer) sensordata.get("et_Surface_Soil_Temp_Floor");// 表层土壤温度下限

										int et_Shallow_Soil_Temp_Up = (Integer) sensordata.get("et_Shallow_Soil_Temp_Up");// 浅层土壤温度上限

										int et_Shallow_Soil_Temp_Floor = (Integer) sensordata.get("et_Shallow_Soil_Temp_Floor");// 浅层土壤温度下限

										int et_Intermediate_Soil_Temp_Up = (Integer) sensordata.get("et_Intermediate_Soil_Temp_Up");// 中层土壤温度上限

										int et_Intermediate_Soil_Temp_Floor = (Integer) sensordata.get("et_Intermediate_Soil_Temp_Floor");// 中层土壤温度下限

										int et_Deep_Soil_Temp_Up = (Integer) sensordata.get("et_Deep_Soil_Temp_Up");// 深层土壤温度上限

										int et_Deep_Soil_Temp_Floor = (Integer) sensordata.get("et_Deep_Soil_Temp_Floor");// 深层土壤温度下限

										// 气象站传感器数据和阈值比较
										if (m_Air_Temp < et_Air_Temp_Floor || m_Air_Temp > et_Air_Temp_Up) {
											m_Air_Temp_str = "空气温度";

										}
										if (m_Air_Damp < et_Air_Damp_Floor || m_Air_Damp > et_Air_Damp_Up) {
											m_Air_Damp_str = "空气湿度";
										}
										if (m_Dew_Point < et_Dew_Point_Floor || m_Dew_Point > et_Dew_Point_Up) {
											m_Dew_Point_str = "露点";
										}
										if (m_Illuminance < et_Illuminance_Floor || m_Illuminance > et_Illuminance_Up) {
											m_Illuminance_str = "光照";
										}
										if (m_Solar_Radiation < et_Solar_Radiation_Floor|| m_Solar_Radiation > et_Solar_Radiation_Up) {
											m_Solar_Radiation_str = "太阳辐射";
										}
										if (m_Surface_Soil_Moisture < et_Surface_Soil_Moisture_Floor|| m_Surface_Soil_Moisture > et_Surface_Soil_Moisture_Up) {
											m_Surface_Soil_Moisture_str = "表层土壤水分";
										}
										if (m_Shallow_Soil_Moisture < et_Shallow_Soil_Moisture_Floor|| m_Shallow_Soil_Moisture > et_Shallow_Soil_Moisture_Up) {
											m_Shallow_Soil_Moisture_str = "浅层土壤水分";
										}
										if (m_Intermediate_Soil_Moisture < et_Intermediate_Soil_Moisture_Floor
												|| m_Intermediate_Soil_Moisture > et_Intermediate_Soil_Moisture_Up) {
											m_Intermediate_Soil_Moisture_str = "中层土壤水分";
										}
										if (m_Deep_Soil_Moisture < et_Deep_Soil_Moisture_Floor
												|| m_Deep_Soil_Moisture > et_Deep_Soil_Moisture_Up) {
											m_Deep_Soil_Moisture_str = "深层土壤水分";
										}
										if (m_Surface_Soil_Temp < et_Surface_Soil_Temp_Floor
												|| m_Surface_Soil_Temp > et_Surface_Soil_Temp_Up) {
											m_Surface_Soil_Temp_str = "表层土壤温度";
										}
										if (m_Shallow_Soil_Temp < et_Shallow_Soil_Temp_Floor
												|| m_Shallow_Soil_Temp > et_Shallow_Soil_Temp_Up) {
											m_Shallow_Soil_Temp_str = "浅层土壤温度";
										}
										if (m_Intermediate_Soil_Temp < et_Intermediate_Soil_Temp_Floor
												|| m_Intermediate_Soil_Temp > et_Intermediate_Soil_Temp_Up) {
											m_Intermediate_Soil_Temp_str = "中层土壤温度";
										}
										if (m_Deep_Soil_Temp < et_Deep_Soil_Temp_Floor
												|| m_Deep_Soil_Temp > et_Deep_Soil_Temp_Up) {
											m_Deep_Soil_Temp_str = "深层土壤温度";
										}
										if ((m_Air_Temp_str 
												+ m_Air_Damp_str 
												+ m_Dew_Point_str 
												+ m_Illuminance_str
												+ m_Solar_Radiation_str 
												+ m_Surface_Soil_Moisture_str
												+ m_Shallow_Soil_Moisture_str 
												+ m_Intermediate_Soil_Moisture_str
												+ m_Deep_Soil_Moisture_str 
												+ m_Surface_Soil_Temp_str
												+ m_Shallow_Soil_Temp_str 
												+ m_Intermediate_Soil_Temp_str
												+ m_Deep_Soil_Temp_str).length() > 0) {
											
											builder.setContentText("临江镇福德村气象站" 
												+ m_Air_Temp_str 
												+ m_Air_Damp_str
												+ m_Dew_Point_str 
												+ m_Illuminance_str 
												+ m_Solar_Radiation_str
												+ m_Surface_Soil_Moisture_str 
												+ m_Shallow_Soil_Moisture_str
												+ m_Intermediate_Soil_Moisture_str 
												+ m_Deep_Soil_Moisture_str
												+ m_Surface_Soil_Temp_str
												+ m_Shallow_Soil_Temp_str
												+ m_Intermediate_Soil_Temp_str
												+ m_Deep_Soil_Temp_str
												+ "没有在设置阈值范围内");
											notificationmanager.notify(messageNotificationID, builder.build());
										}
									}
								}
							}
						}
					}
					Thread.sleep(10000);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
	}
	
	
//取墒情监测站的阈值数据
	class MessageThread_sqjc extends Thread {
		public boolean isRunning = true;

		public void run() {
			while (isRunning) {
				try {
					// 5秒询问一次服务器
					Thread.sleep(5000);
					try {
						getServerMessage_sqjc();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		}

		private void getServerMessage_sqjc() throws Exception {
			// 获取服务器地址
			preservice = new PreferencesService(getApplicationContext());
			Map<String, String> params = preservice.getPreferences();
			String serviceIP = params.get("serviceIP");
			Log.i("serverip", serviceIP);
			if (serviceIP.equals("")) {
				preservice.save("120.79.76.116:8000", "", "", "", "", "", "");
			} else {
				serverIP = serviceIP;
			}
			// 组拼参数
			cmd = FinalConstant.YZSZ_SQJC_WARN_REQUEST_SERVER;
			reqparams = new HashMap<String, Object>();
			reqparams.put("cmd", cmd);
			String path = "http://" + serverIP + "/AppService.php";
			// 获取返回数据
			String reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
			Log.d("debugTest", "arr_data--阈值返回 " + reslut);
			if (reslut != null) {
				if (reslut.equals("1")) {
					Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
				} else {
					JSONArray array = new JSONArray(reslut);
					JSONObject cmd = (JSONObject) array.get(0);
					String str_cmd = cmd.getString("cmd");
					int len = 0;
					len = array.length();
					if (len > 1) {
						if (str_cmd.equals(FinalConstant.YZSZ_SQJC_WARN_REBACK_SERVER)) {
							if (!array.get(1).equals(false)) {
								thresholdValue_sqjc = new ArrayList<HashMap<String, Object>>();
								Log.i("info", "取阈值里面" + thresholdValue_sqjc.toString());
								for (int i = 1; i < 2; i++) {
									JSONObject temp = (JSONObject) array.get(i);
									HashMap<String, Object> map = new HashMap<String, Object>();
									map.put("sqjcNum",
											Integer.parseInt(temp.getString("sqjcNum")));
									map.put("et_Sqjc_Air_Temp_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Air_Temp_Up")));
									map.put("et_Sqjc_Air_Temp_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Air_Temp_Floor")));
									map.put("et_Sqjc_Air_Damp_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Air_Damp_Up")));
									map.put("et_Sqjc_Air_Damp_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Air_Damp_Floor")));
									map.put("et_Sqjc_Barometric_Pressure_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Barometric_Pressure_Up")));
									map.put("et_Sqjc_Barometric_Pressure_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Barometric_Pressure_Floor")));
									map.put("et_Sqjc_Soil_Moisture_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_Moisture_Up")));
									map.put("et_Sqjc_Soil_Moisture_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_Moisture_Floor")));
									map.put("et_Sqjc_Soil_Temp_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_Temp_Up")));
									map.put("et_Sqjc_Soil_Temp_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_Temp_Floor")));
									map.put("et_Sqjc_Soil_PH_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_PH_Up")));
									map.put("et_Sqjc_Soil_PH_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_PH_Floor")));
									map.put("et_Sqjc_Soil_NH3N_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_NH3N_Up")));
									map.put("et_Sqjc_Soil_NH3N_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_NH3N_Floor")));
									map.put("et_Sqjc_Soil_NH4NO3_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_NH4NO3_Up")));
									map.put("et_Sqjc_Soil_NH4NO3_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_NH4NO3_Floor")));
									map.put("et_Sqjc_Soil_K_Up", 
											Integer.parseInt(temp.getString("et_Sqjc_Soil_K_Up")));
									map.put("et_Sqjc_Soil_K_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_K_Floor")));
									thresholdValue_sqjc.add(map);
									Log.i("info", map.toString());

								}
							}
						}
					}
				}
			}
		}
	}
	
//四圣村
	class ssjd extends Thread {
		private boolean isRunning = true;

		public void run() {
			while (isRunning) {
				try {
					// 获取服务器地址
					preservice = new PreferencesService(getApplicationContext());
					Map<String, String> params = preservice.getPreferences();
					String serviceIP = params.get("serviceIP");
					if (serviceIP.equals("")) {
						preservice.save("120.79.76.116:8000", "222.180.45.174", "8083", "admin", "zs123456", "", "");
					} else {
						serverIP = serviceIP;
					}

					// 组拼请求参数
					cmd = FinalConstant.SQZ1FIND_REQUEST_SERVER;// 查询请求命令
					reqparams = new HashMap<String, Object>();
					reqparams.put("cmd", cmd);
					String path = "http://" + serverIP + "/AppService.php";
					// 获取返回数据
					String reslut;
					reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
					Log.d("info2", "arr_data 传感器返回-- " + reslut);
					if (reslut != null) {
						if (reslut.equals("1")) {
							Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
						} else {
							JSONArray array = new JSONArray(reslut);
							JSONObject cmd = (JSONObject) array.get(0);
							String str_cmd = cmd.getString("cmd");
							int len = 0;
							len = array.length();
							if (len > 1) {
								if (str_cmd.equals(FinalConstant.SQZ1FIND_REBACK_SERVER)) {
									if (!array.get(1).equals(false)) {
										String m_Sqjc_Air_Temp_str = "";
										String m_Sqjc_Air_Damp_str = "";
										String mSqjc_Barometric_Pressure_str = "";
										String m_Sqjc_Soil_Moisture_str = "";
										String m_Sqjc_Soil_Temp_str = "";
										String m_Sqjc_Soil_PH_str = "";
										String m_Sqjc_Soil_NH3N_str = "";
										String m_Sqjc_Soil_NH4NO3_str = "";
										String m_Sqjc_Soil_K_str = "";

										JSONArray arr_data = (JSONArray) array.get(1);
										JSONObject info = (JSONObject) arr_data.get(0);

										// 打印每一条数据确定都取到了数据
										// Log.i("info", info.toString());
										double m_Sqjc_Air_Temp = Double.parseDouble(info.getString("yn_air_temp"));
										Log.i("info", String.valueOf(m_Sqjc_Air_Temp));
										double m_Sqjc_Air_Damp = Double.parseDouble(info.getString("yn_air_humi"));
										Log.i("info", String.valueOf(m_Sqjc_Air_Damp));
										double m_Sqjc_Barometric_Pressure = Double.parseDouble(info.getString("yn_air_pressure"));
										double m_Sqjc_Soil_Moisture = Double.parseDouble(info.getString("yn_water_content"));
										double m_Sqjc_Soil_Temp = Double.parseDouble(info.getString("yn_soil_temp"));
										double m_Sqjc_Soil_PH = Double.parseDouble(info.getString("yn_soil_ph"));
										double m_Sqjc_Soil_NH3N = Double.parseDouble(info.getString("yn_soil_nh3n"));
										double m_Sqjc_Soil_NH4NO3 = Double.parseDouble(info.getString("yn_soil_nh4no3"));
										double m_Sqjc_Soil_K = Double.parseDouble(info.getString("yn_soil_k"));


										HashMap<String, Object> sensordata = thresholdValue_sqjc
												.get(FinalConstant.GT_QUERY_BACK_SQJC1_DATA - 1);
										Log.d("debugtest1", "--比较函数----");

										int et_Sqjc_Air_Temp_Up = (Integer) sensordata.get("et_Sqjc_Air_Temp_Up");// 空气温度上限

										int et_Sqjc_Air_Temp_Floor = (Integer) sensordata.get("et_Sqjc_Air_Temp_Floor");// 空气温度下限

										int et_Sqjc_Air_Damp_Up = (Integer) sensordata.get("et_Sqjc_Air_Damp_Up");// 空气湿度上限

										int et_Sqjc_Air_Damp_Floor = (Integer) sensordata.get("et_Sqjc_Air_Damp_Floor");// 空气湿度下限

										int et_Sqjc_Barometric_Pressure_Up = (Integer) sensordata.get("et_Sqjc_Barometric_Pressure_Up");// 大气压力上限

										int et_Sqjc_Barometric_Pressure_Floor = (Integer) sensordata.get("et_Sqjc_Barometric_Pressure_Floor"); // 大气压力下限

										int et_Sqjc_Soil_Moisture_Up = (Integer) sensordata.get("et_Sqjc_Soil_Moisture_Up");// 土壤水分上限

										int et_Sqjc_Soil_Moisture_Floor = (Integer) sensordata.get("et_Sqjc_Soil_Moisture_Floor");// 土壤水分下限

										int et_Sqjc_Soil_Temp_Up = (Integer) sensordata.get("et_Sqjc_Soil_Temp_Up");// 土壤温度上限

										int et_Sqjc_Soil_Temp_Floor = (Integer) sensordata.get("et_Sqjc_Soil_Temp_Floor");// 土壤温度下限

										int et_Sqjc_Soil_PH_Up = (Integer) sensordata.get("et_Sqjc_Soil_PH_Up");// 土壤PH上限

										int et_Sqjc_Soil_PH_Floor = (Integer) sensordata.get("et_Sqjc_Soil_PH_Floor"); // 土壤PH下限

										int et_Sqjc_Soil_NH3N_Up = (Integer) sensordata.get("et_Sqjc_Soil_NH3N_Up");// 土壤氨氮上限

										int et_Sqjc_Soil_NH3N_Floor = (Integer) sensordata.get("et_Sqjc_Soil_NH3N_Floor");// 土壤氨氮下限

										int et_Sqjc_Soil_NH4NO3_Up = (Integer) sensordata.get("et_Sqjc_Soil_NH4NO3_Up");// 土壤硝铵上限

										int et_Sqjc_Soil_NH4NO3_Floor = (Integer) sensordata.get("et_Sqjc_Soil_NH4NO3_Floor"); // 土壤硝铵下限

										int et_Sqjc_Soil_K_Up = (Integer) sensordata.get("et_Sqjc_Soil_K_Up");// 土壤钾离子上限

										int et_Sqjc_Soil_K_Floor = (Integer) sensordata.get("et_Sqjc_Soil_K_Floor");// 土壤钾离子下限

										// 比较
										if (m_Sqjc_Air_Temp < et_Sqjc_Air_Temp_Floor
												|| m_Sqjc_Air_Temp > et_Sqjc_Air_Temp_Up) {
											m_Sqjc_Air_Temp_str = "空气温度";

										}
										if (m_Sqjc_Air_Damp < et_Sqjc_Air_Damp_Floor
												|| m_Sqjc_Air_Damp > et_Sqjc_Air_Damp_Up) {
											m_Sqjc_Air_Damp_str = "空气湿度";
										}
										if (m_Sqjc_Barometric_Pressure < et_Sqjc_Barometric_Pressure_Floor
												|| m_Sqjc_Barometric_Pressure > et_Sqjc_Barometric_Pressure_Up) {
											mSqjc_Barometric_Pressure_str = "大气压力";
										}
										if (m_Sqjc_Soil_Moisture < et_Sqjc_Soil_Moisture_Floor
												|| m_Sqjc_Soil_Moisture > et_Sqjc_Soil_Moisture_Up) {
											m_Sqjc_Soil_Moisture_str = "土壤水分";
										}
										if (m_Sqjc_Soil_Temp < et_Sqjc_Soil_Temp_Floor
												|| m_Sqjc_Soil_Temp > et_Sqjc_Soil_Temp_Up) {
											m_Sqjc_Soil_Temp_str = "土壤温度";
										}
										if (m_Sqjc_Soil_PH < et_Sqjc_Soil_PH_Floor
												|| m_Sqjc_Soil_PH > et_Sqjc_Soil_PH_Up) {
											m_Sqjc_Soil_PH_str = "土壤PH";
										}
										if (m_Sqjc_Soil_NH3N < et_Sqjc_Soil_NH3N_Floor
												|| m_Sqjc_Soil_NH3N > et_Sqjc_Soil_NH3N_Up) {
											m_Sqjc_Soil_NH3N_str = "土壤氨氮";
										}
										if (m_Sqjc_Soil_NH4NO3 < et_Sqjc_Soil_NH3N_Floor
												|| m_Sqjc_Soil_NH4NO3 > et_Sqjc_Soil_NH3N_Up) {
											m_Sqjc_Soil_NH4NO3_str = "土壤硝铵";
										}
										if (m_Sqjc_Soil_K < et_Sqjc_Soil_K_Floor || m_Sqjc_Soil_K > et_Sqjc_Soil_K_Up) {
											m_Sqjc_Soil_K_str = "土壤钾离子";
										}
										if ((m_Sqjc_Air_Temp_str 
												+ m_Sqjc_Air_Damp_str 
												+ mSqjc_Barometric_Pressure_str
												+ m_Sqjc_Soil_Moisture_str 
												+ m_Sqjc_Soil_Temp_str 
												+ m_Sqjc_Soil_PH_str
												+ m_Sqjc_Soil_NH3N_str
												+ m_Sqjc_Soil_NH4NO3_str 
												+ m_Sqjc_Soil_K_str)
												.length() > 0) {

											builder.setContentText("白鹤街道四圣村墒情监测站" 
												+ m_Sqjc_Air_Temp_str
												+ m_Sqjc_Air_Damp_str 
												+ mSqjc_Barometric_Pressure_str
												+ m_Sqjc_Soil_Moisture_str
												+ m_Sqjc_Soil_Temp_str
												+ m_Sqjc_Soil_PH_str
												+ m_Sqjc_Soil_NH3N_str
												+ m_Sqjc_Soil_NH4NO3_str
												+ m_Sqjc_Soil_K_str 
												+ "没有在设置阈值范围内");
											notificationmanager.notify(messageNotificationID, builder.build());
										}
									}
								}
							}
						}
					}
					Thread.sleep(10000);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
	}

	class wyjd extends Thread {
		private boolean isRunning = true;

		public void run() {
			while (isRunning) {
				try {
					// 获取服务器地址
					preservice = new PreferencesService(getApplicationContext());
					Map<String, String> params = preservice.getPreferences();
					String serviceIP = params.get("serviceIP");
					if (serviceIP.equals("")) {
						preservice.save("120.79.76.116:8000", "222.180.45.174", "8083", "admin", "zs123456", "", "");
					} else {
						serverIP = serviceIP;
					}

					// 组拼请求参数
					cmd = FinalConstant.SQZ2FIND_REQUEST_SERVER;// 查询请求命令
					reqparams = new HashMap<String, Object>();
					reqparams.put("cmd", cmd);
					String path = "http://" + serverIP + "/AppService.php";
					// 获取返回数据
					String reslut;
					reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
					Log.d("info2", "arr_data 传感器返回-- " + reslut);
					if (reslut != null) {
						if (reslut.equals("1")) {
							Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
						} else {
							JSONArray array = new JSONArray(reslut);
							JSONObject cmd = (JSONObject) array.get(0);
							String str_cmd = cmd.getString("cmd");
							int len = 0;
							len = array.length();
							if (len > 1) {
								if (str_cmd.equals(FinalConstant.SQZ2FIND_REBACK_SERVER)) {
									if (!array.get(1).equals(false)) {
										String m_Sqjc_Air_Temp_str = "";
										String m_Sqjc_Air_Damp_str = "";
										String mSqjc_Barometric_Pressure_str = "";
										String m_Sqjc_Soil_Moisture_str = "";
										String m_Sqjc_Soil_Temp_str = "";
										String m_Sqjc_Soil_PH_str = "";
										String m_Sqjc_Soil_NH3N_str = "";
										String m_Sqjc_Soil_NH4NO3_str = "";
										String m_Sqjc_Soil_K_str = "";

										JSONArray arr_data = (JSONArray) array.get(1);
										JSONObject info = (JSONObject) arr_data.get(0);

										// 打印每一条数据确定都取到了数据
										// Log.i("info", info.toString());
										double m_Sqjc_Air_Temp = Double.parseDouble(info.getString("yn_air_temp"));
										Log.i("info", String.valueOf(m_Sqjc_Air_Temp));
										double m_Sqjc_Air_Damp = Double.parseDouble(info.getString("yn_air_humi"));
										Log.i("info", String.valueOf(m_Sqjc_Air_Damp));
										double m_Sqjc_Barometric_Pressure = Double.parseDouble(info.getString("yn_air_pressure"));
										double m_Sqjc_Soil_Moisture = Double.parseDouble(info.getString("yn_water_content"));
										double m_Sqjc_Soil_Temp = Double.parseDouble(info.getString("yn_soil_temp"));
										double m_Sqjc_Soil_PH = Double.parseDouble(info.getString("yn_soil_ph"));
										double m_Sqjc_Soil_NH3N = Double.parseDouble(info.getString("yn_soil_nh3n"));
										double m_Sqjc_Soil_NH4NO3 = Double.parseDouble(info.getString("yn_soil_nh4no3"));
										double m_Sqjc_Soil_K = Double.parseDouble(info.getString("yn_soil_k"));

										HashMap<String, Object> sensordata = thresholdValue_sqjc
												.get(FinalConstant.GT_QUERY_BACK_SQJC2_DATA - 1);
										Log.d("debugtest1", "--比较函数----");

										int et_Sqjc_Air_Temp_Up = (Integer) sensordata.get("et_Sqjc_Air_Temp_Up");// 空气温度上限

										int et_Sqjc_Air_Temp_Floor = (Integer) sensordata.get("et_Sqjc_Air_Temp_Floor");// 空气温度下限

										int et_Sqjc_Air_Damp_Up = (Integer) sensordata.get("et_Sqjc_Air_Damp_Up");// 空气湿度上限

										int et_Sqjc_Air_Damp_Floor = (Integer) sensordata.get("et_Sqjc_Air_Damp_Floor");// 空气湿度下限

										int et_Sqjc_Barometric_Pressure_Up = (Integer) sensordata.get("et_Sqjc_Barometric_Pressure_Up");// 大气压力上限

										int et_Sqjc_Barometric_Pressure_Floor = (Integer) sensordata.get("et_Sqjc_Barometric_Pressure_Floor"); // 大气压力下限

										int et_Sqjc_Soil_Moisture_Up = (Integer) sensordata.get("et_Sqjc_Soil_Moisture_Up");// 土壤水分上限

										int et_Sqjc_Soil_Moisture_Floor = (Integer) sensordata.get("et_Sqjc_Soil_Moisture_Floor");// 土壤水分下限

										int et_Sqjc_Soil_Temp_Up = (Integer) sensordata.get("et_Sqjc_Soil_Temp_Up");// 土壤温度上限

										int et_Sqjc_Soil_Temp_Floor = (Integer) sensordata.get("et_Sqjc_Soil_Temp_Floor");// 土壤温度下限

										int et_Sqjc_Soil_PH_Up = (Integer) sensordata.get("et_Sqjc_Soil_PH_Up");// 土壤PH上限

										int et_Sqjc_Soil_PH_Floor = (Integer) sensordata.get("et_Sqjc_Soil_PH_Floor"); // 土壤PH下限

										int et_Sqjc_Soil_NH3N_Up = (Integer) sensordata.get("et_Sqjc_Soil_NH3N_Up");// 土壤氨氮上限

										int et_Sqjc_Soil_NH3N_Floor = (Integer) sensordata.get("et_Sqjc_Soil_NH3N_Floor");// 土壤氨氮下限

										int et_Sqjc_Soil_NH4NO3_Up = (Integer) sensordata.get("et_Sqjc_Soil_NH4NO3_Up");// 土壤硝铵上限

										int et_Sqjc_Soil_NH4NO3_Floor = (Integer) sensordata.get("et_Sqjc_Soil_NH4NO3_Floor"); // 土壤硝铵下限

										int et_Sqjc_Soil_K_Up = (Integer) sensordata.get("et_Sqjc_Soil_K_Up");// 土壤钾离子上限

										int et_Sqjc_Soil_K_Floor = (Integer) sensordata.get("et_Sqjc_Soil_K_Floor");// 土壤钾离子下限
										// 比较
										if (m_Sqjc_Air_Temp < et_Sqjc_Air_Temp_Floor
												|| m_Sqjc_Air_Temp > et_Sqjc_Air_Temp_Up) {
											m_Sqjc_Air_Temp_str = "空气温度";

										}
										if (m_Sqjc_Air_Damp < et_Sqjc_Air_Damp_Floor
												|| m_Sqjc_Air_Damp > et_Sqjc_Air_Damp_Up) {
											m_Sqjc_Air_Damp_str = "空气湿度";
										}
										if (m_Sqjc_Barometric_Pressure < et_Sqjc_Barometric_Pressure_Floor
												|| m_Sqjc_Barometric_Pressure > et_Sqjc_Barometric_Pressure_Up) {
											mSqjc_Barometric_Pressure_str = "大气压力";
										}
										if (m_Sqjc_Soil_Moisture < et_Sqjc_Soil_Moisture_Floor
												|| m_Sqjc_Soil_Moisture > et_Sqjc_Soil_Moisture_Up) {
											m_Sqjc_Soil_Moisture_str = "土壤水分";
										}
										if (m_Sqjc_Soil_Temp < et_Sqjc_Soil_Temp_Floor
												|| m_Sqjc_Soil_Temp > et_Sqjc_Soil_Temp_Up) {
											m_Sqjc_Soil_Temp_str = "土壤温度";
										}
										if (m_Sqjc_Soil_PH < et_Sqjc_Soil_PH_Floor
												|| m_Sqjc_Soil_PH > et_Sqjc_Soil_PH_Up) {
											m_Sqjc_Soil_PH_str = "土壤PH";
										}
										if (m_Sqjc_Soil_NH3N < et_Sqjc_Soil_NH3N_Floor
												|| m_Sqjc_Soil_NH3N > et_Sqjc_Soil_NH3N_Up) {
											m_Sqjc_Soil_NH3N_str = "土壤氨氮";
										}
										if (m_Sqjc_Soil_NH4NO3 < et_Sqjc_Soil_NH3N_Floor
												|| m_Sqjc_Soil_NH4NO3 > et_Sqjc_Soil_NH3N_Up) {
											m_Sqjc_Soil_NH4NO3_str = "土壤硝铵";
										}
										if (m_Sqjc_Soil_K < et_Sqjc_Soil_K_Floor || m_Sqjc_Soil_K > et_Sqjc_Soil_K_Up) {
											m_Sqjc_Soil_K_str = "土壤钾离子";
										}
										if ((m_Sqjc_Air_Temp_str 
												+ m_Sqjc_Air_Damp_str 
												+ mSqjc_Barometric_Pressure_str
												+ m_Sqjc_Soil_Moisture_str 
												+ m_Sqjc_Soil_Temp_str 
												+ m_Sqjc_Soil_PH_str
												+ m_Sqjc_Soil_NH3N_str 
												+ m_Sqjc_Soil_NH4NO3_str
												+ m_Sqjc_Soil_K_str)
												.length() > 0) {

											builder.setContentText("丰乐街道乌杨村墒情监测站" 
												+ m_Sqjc_Air_Temp_str
												+ m_Sqjc_Air_Damp_str 
												+ mSqjc_Barometric_Pressure_str
												+ m_Sqjc_Soil_Moisture_str 
												+ m_Sqjc_Soil_Temp_str
												+ m_Sqjc_Soil_PH_str 
												+ m_Sqjc_Soil_NH3N_str
												+ m_Sqjc_Soil_NH4NO3_str
												+ m_Sqjc_Soil_K_str 
												+ "没有在设置阈值范围内");
											notificationmanager.notify(messageNotificationID, builder.build());
										}
									}
								}
							}
						}
					}
					Thread.sleep(10000);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
	}

	class nyjd extends Thread {
		private boolean isRunning = true;

		public void run() {
			while (isRunning) {
				try {
					// 获取服务器地址
					preservice = new PreferencesService(getApplicationContext());
					Map<String, String> params = preservice.getPreferences();
					String serviceIP = params.get("serviceIP");
					if (serviceIP.equals("")) {
						preservice.save("120.79.76.116:8000", "222.180.45.174", "8083", "admin", "zs123456", "", "");
					} else {
						serverIP = serviceIP;
					}

					// 组拼请求参数
					cmd = FinalConstant.SQZ3FIND_REQUEST_SERVER;// 查询请求命令
					reqparams = new HashMap<String, Object>();
					reqparams.put("cmd", cmd);
					String path = "http://" + serverIP + "/AppService.php";
					// 获取返回数据
					String reslut;
					reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
					Log.d("info2", "arr_data 传感器返回-- " + reslut);
					if (reslut != null) {
						if (reslut.equals("1")) {
							Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
						} else {
							JSONArray array = new JSONArray(reslut);
							JSONObject cmd = (JSONObject) array.get(0);
							String str_cmd = cmd.getString("cmd");
							int len = 0;
							len = array.length();
							if (len > 1) {
								if (str_cmd.equals(FinalConstant.SQZ3FIND_REBACK_SERVER)) {
									if (!array.get(1).equals(false)) {
										String m_Sqjc_Air_Temp_str = "";
										String m_Sqjc_Air_Damp_str = "";
										String mSqjc_Barometric_Pressure_str = "";
										String m_Sqjc_Soil_Moisture_str = "";
										String m_Sqjc_Soil_Temp_str = "";
										String m_Sqjc_Soil_PH_str = "";
										String m_Sqjc_Soil_NH3N_str = "";
										String m_Sqjc_Soil_NH4NO3_str = "";
										String m_Sqjc_Soil_K_str = "";

										JSONArray arr_data = (JSONArray) array.get(1);
										JSONObject info = (JSONObject) arr_data.get(0);

										// 打印每一条数据确定都取到了数据
										// Log.i("info", info.toString());
										double m_Sqjc_Air_Temp = Double.parseDouble(info.getString("yn_air_temp"));
										Log.i("info", String.valueOf(m_Sqjc_Air_Temp));
										double m_Sqjc_Air_Damp = Double.parseDouble(info.getString("yn_air_humi"));
										Log.i("info", String.valueOf(m_Sqjc_Air_Damp));
										double m_Sqjc_Barometric_Pressure = Double.parseDouble(info.getString("yn_air_pressure"));
										double m_Sqjc_Soil_Moisture = Double.parseDouble(info.getString("yn_water_content"));
										double m_Sqjc_Soil_Temp = Double.parseDouble(info.getString("yn_soil_temp"));
										double m_Sqjc_Soil_PH = Double.parseDouble(info.getString("yn_soil_ph"));
										double m_Sqjc_Soil_NH3N = Double.parseDouble(info.getString("yn_soil_nh3n"));
										double m_Sqjc_Soil_NH4NO3 = Double.parseDouble(info.getString("yn_soil_nh4no3"));
										double m_Sqjc_Soil_K = Double.parseDouble(info.getString("yn_soil_k"));

										HashMap<String, Object> sensordata = thresholdValue_sqjc
												.get(FinalConstant.GT_QUERY_BACK_SQJC3_DATA - 1);
										Log.d("debugtest1", "--比较函数----");

										int et_Sqjc_Air_Temp_Up = (Integer) sensordata.get("et_Sqjc_Air_Temp_Up");// 空气温度上限

										int et_Sqjc_Air_Temp_Floor = (Integer) sensordata.get("et_Sqjc_Air_Temp_Floor");// 空气温度下限

										int et_Sqjc_Air_Damp_Up = (Integer) sensordata.get("et_Sqjc_Air_Damp_Up");// 空气湿度上限

										int et_Sqjc_Air_Damp_Floor = (Integer) sensordata.get("et_Sqjc_Air_Damp_Floor");// 空气湿度下限

										int et_Sqjc_Barometric_Pressure_Up = (Integer) sensordata.get("et_Sqjc_Barometric_Pressure_Up");// 大气压力上限

										int et_Sqjc_Barometric_Pressure_Floor = (Integer) sensordata.get("et_Sqjc_Barometric_Pressure_Floor"); // 大气压力下限

										int et_Sqjc_Soil_Moisture_Up = (Integer) sensordata.get("et_Sqjc_Soil_Moisture_Up");// 土壤水分上限

										int et_Sqjc_Soil_Moisture_Floor = (Integer) sensordata.get("et_Sqjc_Soil_Moisture_Floor");// 土壤水分下限

										int et_Sqjc_Soil_Temp_Up = (Integer) sensordata.get("et_Sqjc_Soil_Temp_Up");// 土壤温度上限

										int et_Sqjc_Soil_Temp_Floor = (Integer) sensordata.get("et_Sqjc_Soil_Temp_Floor");// 土壤温度下限

										int et_Sqjc_Soil_PH_Up = (Integer) sensordata.get("et_Sqjc_Soil_PH_Up");// 土壤PH上限

										int et_Sqjc_Soil_PH_Floor = (Integer) sensordata.get("et_Sqjc_Soil_PH_Floor"); // 土壤PH下限

										int et_Sqjc_Soil_NH3N_Up = (Integer) sensordata.get("et_Sqjc_Soil_NH3N_Up");// 土壤氨氮上限

										int et_Sqjc_Soil_NH3N_Floor = (Integer) sensordata.get("et_Sqjc_Soil_NH3N_Floor");// 土壤氨氮下限

										int et_Sqjc_Soil_NH4NO3_Up = (Integer) sensordata.get("et_Sqjc_Soil_NH4NO3_Up");// 土壤硝铵上限

										int et_Sqjc_Soil_NH4NO3_Floor = (Integer) sensordata.get("et_Sqjc_Soil_NH4NO3_Floor"); // 土壤硝铵下限

										int et_Sqjc_Soil_K_Up = (Integer) sensordata.get("et_Sqjc_Soil_K_Up");// 土壤钾离子上限

										int et_Sqjc_Soil_K_Floor = (Integer) sensordata.get("et_Sqjc_Soil_K_Floor");// 土壤钾离子下限
										// 比较
										if (m_Sqjc_Air_Temp < et_Sqjc_Air_Temp_Floor
												|| m_Sqjc_Air_Temp > et_Sqjc_Air_Temp_Up) {
											m_Sqjc_Air_Temp_str = "空气温度";

										}
										if (m_Sqjc_Air_Damp < et_Sqjc_Air_Damp_Floor
												|| m_Sqjc_Air_Damp > et_Sqjc_Air_Damp_Up) {
											m_Sqjc_Air_Damp_str = "空气湿度";
										}
										if (m_Sqjc_Barometric_Pressure < et_Sqjc_Barometric_Pressure_Floor
												|| m_Sqjc_Barometric_Pressure > et_Sqjc_Barometric_Pressure_Up) {
											mSqjc_Barometric_Pressure_str = "大气压力";
										}
										if (m_Sqjc_Soil_Moisture < et_Sqjc_Soil_Moisture_Floor
												|| m_Sqjc_Soil_Moisture > et_Sqjc_Soil_Moisture_Up) {
											m_Sqjc_Soil_Moisture_str = "土壤水分";
										}
										if (m_Sqjc_Soil_Temp < et_Sqjc_Soil_Temp_Floor
												|| m_Sqjc_Soil_Temp > et_Sqjc_Soil_Temp_Up) {
											m_Sqjc_Soil_Temp_str = "土壤温度";
										}
										if (m_Sqjc_Soil_PH < et_Sqjc_Soil_PH_Floor
												|| m_Sqjc_Soil_PH > et_Sqjc_Soil_PH_Up) {
											m_Sqjc_Soil_PH_str = "土壤PH";
										}
										if (m_Sqjc_Soil_NH3N < et_Sqjc_Soil_NH3N_Floor
												|| m_Sqjc_Soil_NH3N > et_Sqjc_Soil_NH3N_Up) {
											m_Sqjc_Soil_NH3N_str = "土壤氨氮";
										}
										if (m_Sqjc_Soil_NH4NO3 < et_Sqjc_Soil_NH3N_Floor
												|| m_Sqjc_Soil_NH4NO3 > et_Sqjc_Soil_NH3N_Up) {
											m_Sqjc_Soil_NH4NO3_str = "土壤硝铵";
										}
										if (m_Sqjc_Soil_K < et_Sqjc_Soil_K_Floor || m_Sqjc_Soil_K > et_Sqjc_Soil_K_Up) {
											m_Sqjc_Soil_K_str = "土壤钾离子";
										}
										if ((m_Sqjc_Air_Temp_str 
												+ m_Sqjc_Air_Damp_str 
												+ mSqjc_Barometric_Pressure_str
												+ m_Sqjc_Soil_Moisture_str 
												+ m_Sqjc_Soil_Temp_str 
												+ m_Sqjc_Soil_PH_str
												+ m_Sqjc_Soil_NH3N_str 
												+ m_Sqjc_Soil_NH4NO3_str 
												+ m_Sqjc_Soil_K_str)
												.length() > 0) {

											builder.setContentText("南雅镇新全村墒情监测站" 
												+ m_Sqjc_Air_Temp_str
												+ m_Sqjc_Air_Damp_str
												+ mSqjc_Barometric_Pressure_str
												+ m_Sqjc_Soil_Moisture_str 
												+ m_Sqjc_Soil_Temp_str
												+ m_Sqjc_Soil_PH_str 
												+ m_Sqjc_Soil_NH3N_str
												+ m_Sqjc_Soil_NH4NO3_str
												+ m_Sqjc_Soil_K_str 
												+ "没有在设置阈值范围内");
											notificationmanager.notify(messageNotificationID, builder.build());
										}
									}
								}
							}
						}
					}
					Thread.sleep(10000);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
	}
}