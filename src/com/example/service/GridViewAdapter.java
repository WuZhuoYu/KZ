package com.example.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.kxapp.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class GridViewAdapter extends BaseAdapter {
	private ArrayList<HashMap<String, Object>> data;
	private Context context;
	public GridViewAdapter(Context context,ArrayList<HashMap<String, Object>> data) {
			this.context=context;
			this.data=data;
	}
	
	
	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Viewholer viewholer=null;
		if(convertView!=null) {
			convertView.getTag();
		}else {
			viewholer=new Viewholer();
			convertView=LayoutInflater.from(context).inflate(R.layout.item_suggestinfo, null);
			viewholer.tv_info=(TextView) convertView.findViewById(R.id.tv_suggestinfo);
			viewholer.tv_name=(TextView) convertView.findViewById(R.id.event_name);
			viewholer.tv_time=(TextView) convertView.findViewById(R.id.tv_time);
			convertView.setTag(viewholer);
		}
		HashMap<String, Object> hMap=data.get(position);
		viewholer.tv_name.setText((CharSequence) hMap.get("name"));
		viewholer.tv_info.setText((CharSequence) hMap.get("info"));
		viewholer.tv_time.setText((CharSequence) hMap.get("time"));
		LayoutInflater inflater=LayoutInflater.from(context);
		return inflater.inflate(R.layout.item_suggestinfo, null);
	}
	class Viewholer{
		TextView tv_info,tv_name,tv_time;
	}
}
