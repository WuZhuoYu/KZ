package com.example.service;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UserDbHelper extends SQLiteOpenHelper {
	private  final static String NAME="userinfo.db";
	public UserDbHelper(Context context) {
		super(context, NAME, null, 1);
	}
	//创建表，并添加默认用户
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table usertable (user text not null,password text not null,juris text not null)");
		db.beginTransaction();
		db.execSQL("insert into usertable ([user],password,juris) select 'admin','admin123','0' union all select 'ljjd','ljjd123','1' union all select 'nyjd','nyjd123','4' union all select 'zjjd','zjjd123','5' union all select 'wyjd','wyjd123','3' union all select 'ssjd','ssjd123','2'union all select 'price','price123','6'");
		db.setTransactionSuccessful();
		db.endTransaction();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}
	//登录查询
	public Cursor select_UserData(SQLiteDatabase db,String user,String pwd) {
		return db.rawQuery("select * from usertable where user=? and password=?", new String[] {user,pwd});
	} 
	//修改用户数据
	public int  update_User(SQLiteDatabase db,ContentValues values,String user) {
		return db.update("usertable", values,"user=?", new String[] {user});
	}
}
