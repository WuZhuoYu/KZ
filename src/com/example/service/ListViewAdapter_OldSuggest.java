package com.example.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.example.kxapp.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListViewAdapter_OldSuggest extends BaseAdapter {
	private Context context;
	private ArrayList<HashMap<String, Object>> eventinfo;
	public ListViewAdapter_OldSuggest(Context context,ArrayList<HashMap<String, Object>> eventinfo){
		this.context=context;
		this.eventinfo=eventinfo;
		
	}
	@Override
	public int getCount() {//设置item的长度
		return eventinfo.size();
	}

	@Override
	public Object getItem(int position) {
		
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		Viewholer viewholer = null;
		if(convertView!=null) {
			convertView.getTag();
		}else {
			viewholer=new Viewholer();
			convertView=LayoutInflater.from(context).inflate(R.layout.item_yh_not_suggest_listview,null);//加载布局
			viewholer.tv_event_name=(TextView) convertView.findViewById(R.id.tv_event_name);
			viewholer.tv_not_suggest=(TextView) convertView.findViewById(R.id.tv_not_suggest);
			viewholer.tv_time=(TextView) convertView.findViewById(R.id.tv_time);
			viewholer.TAG=(TextView) convertView.findViewById(R.id.TAG);
			convertView.setTag(viewholer);//保存到缓存中
			
		}
		HashMap<String, Object> data=eventinfo.get(position);
		//显示输入根据响应的键值取数据并显示至UI
		Log.d("hello", "---第三次最新----"+data.toString());
		viewholer.tv_event_name.setText((CharSequence) data.get("event_name"));
		viewholer.tv_not_suggest.setText((CharSequence) data.get("suggest"));
		viewholer.tv_time.setText((CharSequence) data.get("time"));
		viewholer.TAG.setText((CharSequence) data.get("TAG"));
		//LayoutInflater inflater=LayoutInflater.from(context);
		//return inflater.inflate(R.layout.item_zf_eventinfo, null);
		return  convertView;
	}
	//控件类
	class Viewholer{
		TextView tv_event_name,
				tv_not_suggest,
				TAG,
				tv_time;
	}
}
