package com.example.kxapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.ListViewAdapter;
import com.example.service.ListViewAdapter_NotSuggest;
import com.example.service.PreferencesService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class YH_Not_SuggestBackActivity extends Activity {
	private String flag = "";

	private PreferencesService preservice;
	private String serverIP = "120.79.76.116:8000";
	private ListView lv_yh_suggest;
	private String cmd;
	private HashMap<String, Object> reqparams_lj,reqparams_ss,reqparams_wy,reqparams_ny,reqparams_zj;
	private ArrayList<HashMap<String, Object>> lj_suggest_info, ss_suggest_info, wy_suggest_info, ny_suggest_info,
			zj_suggest_info;

	private boolean Thread;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.yh_not_suggest_back_activity);
//		Intent intent=getIntent();
//		flag=intent.getStringExtra("flag");
		flag=Flag.getFlag();
		
		//返回点击事件
		findViewById(R.id.back).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		//历史数据点击事件
		findViewById(R.id.yh_old_suggest).setOnClickListener(new OnClickListener() {
					
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.putExtra("flag", flag);
				
				intent.setClass(YH_Not_SuggestBackActivity.this, YH_Old_SuggestBackActivity.class);
				startActivity(intent);
			}
		});
		
		// 获取网络配置参数
		preservice = new PreferencesService(getApplicationContext());
		Map<String, String> params = preservice.getPreferences();
		String serverip = params.get("serviceIP");
		if (serverip.equals("")) {
			preservice.save("120.79.76.116:8000", "192.168.16.245", "502", "61.157.134.34", "8082", "admin",
					"cdjx1234cdjx1234");
		} else {
			serverIP = serverip;
		}
		
		initview();
	}

	private void initview() {
		

		lv_yh_suggest = (ListView) findViewById(R.id.lv_yh_suggest);

		if (flag.equals("1")) {
			lj_not_suggest();
		} else if (flag.equals("2")) {
			ss_not_suggest();
		} else if (flag.equals("3")) {
			wy_not_suggest();
		} else if (flag.equals("4")) {
			ny_not_suggest();
		} else if (flag.equals("5")) {
			zj_not_suggest();
		}

	}

	// 临江请求未处理意见反馈信息
	private void lj_not_suggest() {
		cmd = FinalConstant.YH_LJ_NOT_SUGGESTSUBMIT_REQUEST_SERVER;
		Thread = true;
		reqparams_lj = new HashMap<String, Object>();
		reqparams_lj.put("cmd", cmd);
		new Thread(runnable_lj).start();
	}
	// 四圣村请求未处理意见反馈信息
	private void ss_not_suggest() {
		cmd = FinalConstant.YH_SS_NOT_SUGGESTSUBMIT_REQUEST_SERVER;
		reqparams_ss = new HashMap<String, Object>();
		reqparams_ss.put("cmd", cmd);
		new Thread(runnable_ss).start();
	}
	// 乌杨村请求未处理意见反馈信息
	private void wy_not_suggest() {
		cmd = FinalConstant.YH_WY_NOT_SUGGESTSUBMIT_REQUEST_SERVER;
		reqparams_wy = new HashMap<String, Object>();
		reqparams_wy.put("cmd", cmd);
		new Thread(runnable_wy).start();
	}
	// 南雅请求未处理意见反馈信息
	private void ny_not_suggest() {
		cmd = FinalConstant.YH_NY_NOT_SUGGESTSUBMIT_REQUEST_SERVER;
		reqparams_ny = new HashMap<String, Object>();
		reqparams_ny.put("cmd", cmd);
		new Thread(runnable_ny).start();
	}
	// 赵家请求未处理意见反馈信息
	private void zj_not_suggest() {
		cmd = FinalConstant.YH_ZJ_NOT_SUGGESTSUBMIT_REQUEST_SERVER;
		reqparams_zj = new HashMap<String, Object>();
		reqparams_zj.put("cmd", cmd);
		new Thread(runnable_zj).start();
	}

	private Runnable runnable_lj = new Runnable() {

		@Override
		public void run() {
			while(Thread) {
			String path = "http://" + serverIP + "/AppService.php";
			try {
				String reqdata = HttpReqService.postRequest(path, reqparams_lj, "GB2312");
				if (reqdata != null) {
					Message msg = mhandler_lj.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
					Bundle bundle = new Bundle();
					bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);
					msg.setData(bundle);
					mhandler_lj.sendMessage(msg);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		}

	};
	private Handler mhandler_lj = new Handler() {

		public void handleMessage(android.os.Message msg) {
			String jsondata = msg.getData().getString(FinalConstant.GT_BACK_INFO);
			Log.d("debugTest", "reqdata -- " + jsondata);
			if (jsondata.equals("1")) {
				Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
			} else {
				try {
					JSONArray arr = new JSONArray(jsondata);
					JSONObject tmp_cmd = (JSONObject) arr.get(0);
					String str_cmd = tmp_cmd.getString("cmd");
					int len = 0;
					len = arr.length();
					if (len > 1) {
						if (str_cmd.equals(FinalConstant.YH_LJ_NOT_SUGGESTSUBMIT_REBACK_SERVER)) {
							lj_suggest_info = new ArrayList<HashMap<String, Object>>();
							for (int i = 1; i <= (arr.length() - 1); i++) {
								JSONObject data = (JSONObject) arr.get(i);
								HashMap<String, Object> suggestdata = new HashMap<String, Object>();
								suggestdata.put("id", data.get("id"));
								suggestdata.put("TAG", data.getString("TAG"));
								suggestdata.put("event_name", data.getString("event_name"));
								suggestdata.put("time", data.getString("time"));
								suggestdata.put("suggest", data.getString("suggest"));
								suggestdata.put("state", data.getString("state"));
								lj_suggest_info.add(suggestdata);
							}
							info(lj_suggest_info);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		private void info(final ArrayList<HashMap<String, Object>> lj_suggest_info) {
			if (lj_suggest_info != null) {
				ListViewAdapter_NotSuggest adapter = new ListViewAdapter_NotSuggest(YH_Not_SuggestBackActivity.this, lj_suggest_info);
				lv_yh_suggest.setAdapter(adapter);
				lv_yh_suggest.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						TextView tv_event_name = (TextView) view.findViewById(R.id.tv_event_name);
						TextView tv_not_suggest = (TextView) view.findViewById(R.id.tv_not_suggest);
						TextView tv_time = (TextView) view.findViewById(R.id.tv_time);
						TextView TAG = (TextView) view.findViewById(R.id.TAG);
						HashMap<String, Object> lj_suggest = lj_suggest_info.get(position);
						
						Bundle bundle = new Bundle();
						bundle.putString("event_name", (String) lj_suggest.get("event_name"));
						bundle.putString("suggest", (String) lj_suggest.get("suggest"));
						bundle.putString("time", (String) lj_suggest.get("time"));
						bundle.putString("TAG", (String) lj_suggest.get("TAG"));
						Intent intent = new Intent();
						intent.putExtras(bundle);
						intent.setClass(YH_Not_SuggestBackActivity.this, YH_Not_Details_SuggestBackActivity.class);
						startActivity(intent);
					}
				});
			}else {
				Toast.makeText(getApplicationContext(), "请求不到网络数据！", Toast.LENGTH_SHORT).show();
			}

		}
	};

	

	private Runnable runnable_ss = new Runnable() {

		@Override
		public void run() {
			String path = "http://" + serverIP + "/AppService.php";
			try {
				String reqdata = HttpReqService.postRequest(path, reqparams_ss, "GB2312");
				Log.d("debugTest", "reqdata -- " + reqdata);
				if (reqdata != null) {
					Message msg = mhandler_ss.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
					Bundle bundle = new Bundle();
					bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);
					msg.setData(bundle);
					mhandler_ss.sendMessage(msg);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private Handler mhandler_ss = new Handler() {

		public void handleMessage(android.os.Message msg) {
			String jsondata = msg.getData().getString(FinalConstant.GT_BACK_INFO);
			if (jsondata.equals("1")) {
				Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
			} else {
				try {
					JSONArray arr = new JSONArray(jsondata);
					JSONObject tmp_cmd = (JSONObject) arr.get(0);
					String str_cmd = tmp_cmd.getString("cmd");
					int len = 0;
					len = arr.length();
					if (len > 1) {
						if (str_cmd.equals(FinalConstant.YH_SS_NOT_SUGGESTSUBMIT_REBACK_SERVER)) {
							ss_suggest_info = new ArrayList<HashMap<String, Object>>();
							for (int i = 1; i <= (arr.length() - 1); i++) {
								JSONObject data = (JSONObject) arr.get(i);
								HashMap<String, Object> suggestdata = new HashMap<String, Object>();
								suggestdata.put("id", data.get("id"));
								suggestdata.put("TAG", data.getString("TAG"));
								suggestdata.put("event_name", data.getString("event_name"));
								suggestdata.put("time", data.getString("time"));
								suggestdata.put("suggest", data.getString("suggest"));
								suggestdata.put("state", data.getString("state"));
								ss_suggest_info.add(suggestdata);
							}
							info(ss_suggest_info);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		private void info(final ArrayList<HashMap<String, Object>> ss_suggest_info) {
			if (ss_suggest_info != null) {
				ListViewAdapter_NotSuggest adapter = new ListViewAdapter_NotSuggest(YH_Not_SuggestBackActivity.this, ss_suggest_info);
				lv_yh_suggest.setAdapter(adapter);
				lv_yh_suggest.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						TextView tv_event_name = (TextView) view.findViewById(R.id.tv_event_name);
						TextView tv_not_suggest = (TextView) view.findViewById(R.id.tv_not_suggest);
						TextView tv_time = (TextView) view.findViewById(R.id.tv_time);
						TextView TAG = (TextView) view.findViewById(R.id.TAG);
						HashMap<String, Object> ss_suggest = ss_suggest_info.get(position);
						
						Bundle bundle = new Bundle();
						bundle.putString("event_name", (String) ss_suggest.get("event_name"));
						bundle.putString("suggest", (String) ss_suggest.get("suggest"));
						bundle.putString("time", (String) ss_suggest.get("time"));
						bundle.putString("TAG", (String) ss_suggest.get("TAG"));
						Intent intent = new Intent();
						intent.putExtras(bundle);
						intent.setClass(YH_Not_SuggestBackActivity.this, YH_Not_Details_SuggestBackActivity.class);
						startActivity(intent);
					}
				});
			}else {
				Toast.makeText(getApplicationContext(), "请求不到网络数据！", Toast.LENGTH_SHORT).show();
			}

		}
	};

	

	private Runnable runnable_wy = new Runnable() {

		@Override
		public void run() {
			String path = "http://" + serverIP + "/AppService.php";
			try {
				String reqdata = HttpReqService.postRequest(path, reqparams_wy, "GB2312");
				Log.d("debugTest", "reqdata -- " + reqdata);
				if (reqdata != null) {
					Message msg = mhandler_wy.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
					Bundle bundle = new Bundle();
					bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);
					msg.setData(bundle);
					mhandler_wy.sendMessage(msg);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private Handler mhandler_wy = new Handler() {

		public void handleMessage(android.os.Message msg) {
			String jsondata = msg.getData().getString(FinalConstant.GT_BACK_INFO);
			if (jsondata.equals("1")) {
				Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
			} else {
				try {
					JSONArray arr = new JSONArray(jsondata);
					JSONObject tmp_cmd = (JSONObject) arr.get(0);
					String str_cmd = tmp_cmd.getString("cmd");
					int len = 0;
					len = arr.length();
					if (len > 1) {
						if (str_cmd.equals(FinalConstant.YH_WY_NOT_SUGGESTSUBMIT_REBACK_SERVER)) {
							wy_suggest_info = new ArrayList<HashMap<String, Object>>();
							for (int i = 1; i <= (arr.length() - 1); i++) {
								JSONObject data = (JSONObject) arr.get(i);
								HashMap<String, Object> suggestdata = new HashMap<String, Object>();
								suggestdata.put("id", data.get("id"));
								suggestdata.put("TAG", data.getString("TAG"));
								suggestdata.put("event_name", data.getString("event_name"));
								suggestdata.put("time", data.getString("time"));
								suggestdata.put("suggest", data.getString("suggest"));
								suggestdata.put("state", data.getString("state"));
								wy_suggest_info.add(suggestdata);
							}
							info(wy_suggest_info);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		private void info(final ArrayList<HashMap<String, Object>> wy_suggest_info) {
			if (wy_suggest_info != null) {
				ListViewAdapter_NotSuggest adapter = new ListViewAdapter_NotSuggest(YH_Not_SuggestBackActivity.this, wy_suggest_info);
				lv_yh_suggest.setAdapter(adapter);
				lv_yh_suggest.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						TextView tv_event_name = (TextView) view.findViewById(R.id.tv_event_name);
						TextView tv_not_suggest = (TextView) view.findViewById(R.id.tv_not_suggest);
						TextView tv_time = (TextView) view.findViewById(R.id.tv_time);
						TextView TAG = (TextView) view.findViewById(R.id.TAG);
						HashMap<String, Object> wy_suggest = wy_suggest_info.get(position);
						
						Bundle bundle = new Bundle();
						bundle.putString("event_name", (String) wy_suggest.get("event_name"));
						bundle.putString("suggest", (String) wy_suggest.get("suggest"));
						bundle.putString("time", (String) wy_suggest.get("time"));
						bundle.putString("TAG", (String) wy_suggest.get("TAG"));
						Intent intent = new Intent();
						intent.putExtras(bundle);
						intent.setClass(YH_Not_SuggestBackActivity.this, YH_Not_Details_SuggestBackActivity.class);
						startActivity(intent);
					}
				});
			}else {
				Toast.makeText(getApplicationContext(), "请求不到网络数据！", Toast.LENGTH_SHORT).show();
			}

		}
	};

	

	private Runnable runnable_ny = new Runnable() {

		@Override
		public void run() {
			String path = "http://" + serverIP + "/AppService.php";
			try {
				String reqdata = HttpReqService.postRequest(path, reqparams_ny, "GB2312");
				Log.d("debugTest", "reqdata -- " + reqdata);
				if (reqdata != null) {
					Message msg = mhandler_ny.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
					Bundle bundle = new Bundle();
					bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);
					msg.setData(bundle);
					mhandler_ny.sendMessage(msg);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private Handler mhandler_ny = new Handler() {

		public void handleMessage(android.os.Message msg) {
			String jsondata = msg.getData().getString(FinalConstant.GT_BACK_INFO);
			if (jsondata.equals("1")) {
				Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
			} else {
				try {
					JSONArray arr = new JSONArray(jsondata);
					JSONObject tmp_cmd = (JSONObject) arr.get(0);
					String str_cmd = tmp_cmd.getString("cmd");
					int len = 0;
					len = arr.length();
					if (len > 1) {
						if (str_cmd.equals(FinalConstant.YH_NY_NOT_SUGGESTSUBMIT_REBACK_SERVER)) {
							ny_suggest_info = new ArrayList<HashMap<String, Object>>();
							for (int i = 1; i <= (arr.length() - 1); i++) {
								JSONObject data = (JSONObject) arr.get(i);
								HashMap<String, Object> suggestdata = new HashMap<String, Object>();
								suggestdata.put("id", data.get("id"));
								suggestdata.put("TAG", data.getString("TAG"));
								suggestdata.put("event_name", data.getString("event_name"));
								suggestdata.put("time", data.getString("time"));
								suggestdata.put("suggest", data.getString("suggest"));
								suggestdata.put("state", data.getString("state"));
								ny_suggest_info.add(suggestdata);
							}
							info(ny_suggest_info);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		private void info(final ArrayList<HashMap<String, Object>> ny_suggest_info) {
			if (ny_suggest_info != null) {
				ListViewAdapter_NotSuggest adapter = new ListViewAdapter_NotSuggest(YH_Not_SuggestBackActivity.this, ny_suggest_info);
				lv_yh_suggest.setAdapter(adapter);
				lv_yh_suggest.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						TextView tv_event_name = (TextView) view.findViewById(R.id.tv_event_name);
						TextView tv_not_suggest = (TextView) view.findViewById(R.id.tv_not_suggest);
						TextView tv_time = (TextView) view.findViewById(R.id.tv_time);
						TextView TAG = (TextView) view.findViewById(R.id.TAG);
						HashMap<String, Object> ny_suggest = ny_suggest_info.get(position);
						
						Bundle bundle = new Bundle();
						bundle.putString("event_name", (String) ny_suggest.get("event_name"));
						bundle.putString("suggest", (String) ny_suggest.get("suggest"));
						bundle.putString("time", (String) ny_suggest.get("time"));
						bundle.putString("TAG", (String) ny_suggest.get("TAG"));
						Intent intent = new Intent();
						intent.putExtras(bundle);
						intent.setClass(YH_Not_SuggestBackActivity.this, YH_Not_Details_SuggestBackActivity.class);
						startActivity(intent);
					}
				});
			}else {
				Toast.makeText(getApplicationContext(), "请求不到网络数据！", Toast.LENGTH_SHORT).show();
			}

		}
	};

	

	private Runnable runnable_zj = new Runnable() {

		@Override
		public void run() {
			String path = "http://" + serverIP + "/AppService.php";
			try {
				String reqdata = HttpReqService.postRequest(path, reqparams_zj, "GB2312");
				Log.d("debugTest", "reqdata -- " + reqdata);
				if (reqdata != null) {
					Message msg = mhandler_zj.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
					Bundle bundle = new Bundle();
					bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);
					msg.setData(bundle);
					mhandler_zj.sendMessage(msg);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private Handler mhandler_zj = new Handler() {

		public void handleMessage(android.os.Message msg) {
			String jsondata = msg.getData().getString(FinalConstant.GT_BACK_INFO);
			if (jsondata.equals("1")) {
				Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
			} else {
				try {
					JSONArray arr = new JSONArray(jsondata);
					JSONObject tmp_cmd = (JSONObject) arr.get(0);
					String str_cmd = tmp_cmd.getString("cmd");
					int len = 0;
					len = arr.length();
					if (len > 1) {
						if (str_cmd.equals(FinalConstant.YH_ZJ_NOT_SUGGESTSUBMIT_REBACK_SERVER)) {
							zj_suggest_info = new ArrayList<HashMap<String, Object>>();
							for (int i = 1; i <= (arr.length() - 1); i++) {
								JSONObject data = (JSONObject) arr.get(i);
								HashMap<String, Object> suggestdata = new HashMap<String, Object>();
								suggestdata.put("id", data.get("id"));
								suggestdata.put("TAG", data.getString("TAG"));
								suggestdata.put("event_name", data.getString("event_name"));
								suggestdata.put("time", data.getString("time"));
								suggestdata.put("suggest", data.getString("suggest"));
								suggestdata.put("state", data.getString("state"));
								zj_suggest_info.add(suggestdata);
							}
							info(zj_suggest_info);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		private void info(final ArrayList<HashMap<String, Object>> zj_suggest_info) {
			if (zj_suggest_info != null) {
				ListViewAdapter_NotSuggest adapter = new ListViewAdapter_NotSuggest(YH_Not_SuggestBackActivity.this, zj_suggest_info);
				lv_yh_suggest.setAdapter(adapter);
				lv_yh_suggest.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						TextView tv_event_name = (TextView) view.findViewById(R.id.tv_event_name);
						TextView tv_not_suggest = (TextView) view.findViewById(R.id.tv_not_suggest);
						TextView tv_time = (TextView) view.findViewById(R.id.tv_time);
						TextView TAG = (TextView) view.findViewById(R.id.TAG);
						
						HashMap<String, Object> zj_suggest = zj_suggest_info.get(position);
						
						Bundle bundle = new Bundle();
						bundle.putString("event_name", (String) zj_suggest.get("event_name"));
						bundle.putString("suggest", (String) zj_suggest.get("suggest"));
						bundle.putString("time", (String) zj_suggest.get("time"));
						bundle.putString("TAG", (String) zj_suggest.get("TAG"));
						Intent intent = new Intent();
						intent.putExtras(bundle);
						intent.setClass(YH_Not_SuggestBackActivity.this, YH_Not_Details_SuggestBackActivity.class);
						startActivity(intent);
					}
				});
			}else {
				Toast.makeText(getApplicationContext(), "请求不到网络数据！", Toast.LENGTH_SHORT).show();
			}

		}
	};

}
