package com.example.kxapp;


import java.util.ArrayList;

import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.GroundOverlayOptions;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.InfoWindow.OnInfoWindowClickListener;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.map.BaiduMap.OnMarkerDragListener;
import com.baidu.mapapi.map.MarkerOptions.MarkerAnimateType;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.example.kxapp.R;
import com.example.widget.MapDialog;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
/**
@Name:百度地图GIS显示页面 *
@Description: * 
@author wuzhuoyu * 
@Version:V1.00 * 
@Create Date:2018-06-12 *
*/

/**
 * 演示覆盖物的用法
 */
public class BaiduMapGIS extends Activity {

    /**
     * MapView 是地图主控件
     */
    private MapView mMapView;
    private BaiduMap mBaiduMap;
    private Marker mMarkerA;
    private Marker mMarkerB;
    private Marker mMarkerC;
    private Marker mMarkerD;
    private Marker mMarkerE;
    private InfoWindow mInfoWindow;
    private SeekBar alphaSeekBar = null;
    //private CheckBox animationBox = null;

    // 初始化全�? bitmap 信息，不用时及时 recycle
    BitmapDescriptor bdA = BitmapDescriptorFactory
            .fromResource(R.drawable.icon_markss);
    BitmapDescriptor bdB = BitmapDescriptorFactory
            .fromResource(R.drawable.icon_markzj);
    BitmapDescriptor bdC = BitmapDescriptorFactory
            .fromResource(R.drawable.icon_marklj);
    BitmapDescriptor bdD = BitmapDescriptorFactory
            .fromResource(R.drawable.icon_markwy);
    BitmapDescriptor bdE = BitmapDescriptorFactory
            .fromResource(R.drawable.icon_markly);
    BitmapDescriptor bd = BitmapDescriptorFactory
            .fromResource(R.drawable.icon_gcoding);
    BitmapDescriptor bdGround = BitmapDescriptorFactory
            .fromResource(R.drawable.ground_overlay);
	private Button mDataJump;
	private Button mVideoJump;
	private String flag="";
	private TextView mBack;

	
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//      Intent intent=getIntent();
//    	flag=intent.getStringExtra("flag");
        flag=Flag.getFlag();
        setContentView(R.layout.activity_overlay_demo);
        mDataJump = (Button) findViewById(R.id.btn_datajump);
        findViewById(R.id.btn_datajump).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(BaiduMapGIS.this, EnvironmentActivity.class);
				intent.putExtra("flag", flag);
				Log.d("debugTest", "有缘人你看到了吧"+flag);
				startActivity(intent);
			}
		});
        mVideoJump = (Button) findViewById(R.id.btn_videojump);
        findViewById(R.id.btn_videojump).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent it = new Intent();
				it.setClass(BaiduMapGIS.this, IPLoginActivity.class);
				startActivity(it);
			}
		});
        mBack = (TextView) findViewById(R.id.back);
        findViewById(R.id.back).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
//        alphaSeekBar = (SeekBar) findViewById(R.id.alphaBar);
//        alphaSeekBar.setOnSeekBarChangeListener(new SeekBarListener());
//        animationBox = (CheckBox) findViewById(R.id.animation);
        mMapView = (MapView) findViewById(R.id.bmapview);
        mBaiduMap = mMapView.getMap();
        MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(14.0f);
        mBaiduMap.setMapStatus(msu);
        initOverlay();
        
        mBaiduMap.setOnMarkerClickListener(new OnMarkerClickListener() {
            public boolean onMarkerClick(final Marker marker) {
//            	MapDialog dialog =new MapDialog(getApplicationContext());
                Button button = new Button(getApplicationContext());
                button.setWidth(1000);
                //button.setBackgroundResource(R.drawable.bg_gk);
                OnInfoWindowClickListener listener = null;
                if (marker == mMarkerA ) {//四圣村
//           
                    button.setText("重庆开凡农业科技有限公司：主要经营沃柑、W ·默科特、春见等优质晚熟柑橘的种植和销售，产品获绿色食品认证。果园基地面积3000亩，是中国优质农产品示范基地，中国农业部、国标委优质晚熟柑橘标准园示范区，开州区沃柑种植示范基地。公司还经营：农业信息咨询；农业科技领域内的技术开发，技术服务，技术转让；瓜果蔬菜花卉苗木农作物的种植和销售；农家乐生态观光旅游；水产绿色养殖及销售等。开凡公司产业基地气候条件优良，地理位置卓越，环境清幽宜人，是渝东北难得的优质农产品种植基地。");
                    button.setTextColor(Color.WHITE);
//                    button.setWidth(300);
//                    button.setOnClickListener(new OnClickListener() {
//						
//						@Override
//						public void onClick(View v) {
//							Toast.makeText(getApplicationContext(), "那个是这", Toast.LENGTH_SHORT).show();							
//						}
//					});
                    listener = new OnInfoWindowClickListener() {
                        public void onInfoWindowClick() {
                            LatLng ll = marker.getPosition();
                            LatLng llNew = new LatLng(ll.latitude + 0.005,
                                    ll.longitude + 0.005);
                            marker.setPosition(llNew);
                            mBaiduMap.hideInfoWindow();
//                            Toast.makeText(getApplicationContext(), "这个是这�?", Toast.LENGTH_SHORT).show();
                        }
                    };
                    LatLng ll = marker.getPosition();
                    mInfoWindow = new InfoWindow(BitmapDescriptorFactory.fromView(button), ll, -47, listener);
                    mBaiduMap.showInfoWindow(mInfoWindow);
                } else if (marker == mMarkerB) {//赵家
                    button.setText("重庆开州区民丰农业科技有限公司以科学发展观为指导，深化改革，创新发展，在全体员工的共同努力下，取得了优异成绩，荣获多项荣誉称号，企业综合素质不断提升。特别是在主营业务（农业开发服务，果树种植及其产品销售；农业观光服务，乡村旅游服务，农业技术咨询服务（依法须经批准的项目）方面的发展成就有目共睹。" );
                    button.setTextColor(Color.WHITE);
                    button.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            //marker.setIcon(bd);
                            mBaiduMap.hideInfoWindow();
                        }
                    });
                    LatLng ll = marker.getPosition();
                    mInfoWindow = new InfoWindow(button, ll, -47);
                    mBaiduMap.showInfoWindow(mInfoWindow);
                } else if (marker == mMarkerC) {//福德村
                    button.setText("重庆市开州区临江镇福德村柑桔种植股份合作社于2016年注册成立，公司地址：重庆市开州区临江镇福德村5组，主要经营柑桔、苗圃种植、农产品加工服务、农产品物流配送服务；冷藏服务、乡村旅游、餐饮休闲、电子商务服务；雪茄烟、日杂、百货零售 企业类型：农民专业合作社。");
                    button.setTextColor(Color.WHITE);
                    button.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            //marker.remove();
                            mBaiduMap.hideInfoWindow();
                        }
                    });
                    LatLng ll = marker.getPosition();
                    mInfoWindow = new InfoWindow(button, ll, -47);
                    mBaiduMap.showInfoWindow(mInfoWindow);
                }else if (marker == mMarkerD) {//乌杨村
                    button.setText("重庆市开州区绿周果业有限公司于2005年注册成立，公司地址：重庆市开州区丰乐街道办事处乌阳五组，主要经营农业、林业开发服务；果园建设、改造；果树新品种引进、示范；苗木繁育、购销；名优果品；绿化树、造林苗生产、销售；生态旅游开发、植保技术专业化防控；对外贸易经营、进出口本企业生产的产品和原辅材料（凭备案证经营）；农副土特产品购销（不含中药材、粮食购销）、技术咨询、培训服务，畜禽养殖、农药、农业机械、种子、肥料购销 企业类型：有限责任公司。");
                    button.setTextColor(Color.WHITE);
                    button.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            //marker.remove();
                            mBaiduMap.hideInfoWindow();
                        }
                    });
                    LatLng ll = marker.getPosition();
                    mInfoWindow = new InfoWindow(button, ll, -47);
                    mBaiduMap.showInfoWindow(mInfoWindow);
                }else if (marker == mMarkerE) {//南雅
                    button.setText("重庆开州区南雅镇新全村明亮家庭农场，成立于2015-04-29，注册地址在重庆市开县南雅镇新全村8组，主要从事柑橘种植及销售，果树新品种引进、示范；苗木繁育、购销；名优果品；绿化树、造林苗生产、销售；生态旅游开发、植保技术专业化防控；对外贸易经营、进出口本企业生产的产品和原辅材料（凭备案证经营）；农副土特产品购销（不含中药材、粮食购销）、技术咨询、培训服务。");
                    button.setTextColor(Color.WHITE);
                    button.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            //marker.remove();
                            mBaiduMap.hideInfoWindow();
                        }
                    });
                    LatLng ll = marker.getPosition();
                    mInfoWindow = new InfoWindow(button, ll, -47);
                    mBaiduMap.showInfoWindow(mInfoWindow);
                }
                return true;
            }
        });
    }
   
    public void initOverlay() {
        // add marker overlay
    	LatLng llA = new LatLng(31.261879, 108.508525);      //四圣村
        LatLng llB = new LatLng(31.086898, 108.438544);	     //赵家
        LatLng llC = new LatLng(31.121495, 108.132744);		//福德村
        LatLng llD = new LatLng(31.196633, 108.458504);		//乌杨村
        LatLng llE = new LatLng(31.086689, 108.073063);		//南雅村

        MarkerOptions ooA = new MarkerOptions().position(llA).icon(bdA)
                .zIndex(9);
     
        mMarkerA = (Marker) (mBaiduMap.addOverlay(ooA));
        
        MarkerOptions ooB = new MarkerOptions().position(llB).icon(bdB)
                .zIndex(5);
      
        mMarkerB = (Marker) (mBaiduMap.addOverlay(ooB));
        
        MarkerOptions ooC = new MarkerOptions().position(llC).icon(bdC)
                .zIndex(9);
  
        mMarkerC = (Marker) (mBaiduMap.addOverlay(ooC));
        
        MarkerOptions ooD = new MarkerOptions().position(llD).icon(bdD)
                .zIndex(9);

        mMarkerD = (Marker) (mBaiduMap.addOverlay(ooD));

        MarkerOptions ooE = new MarkerOptions().position(llE).icon(bdE)
                .zIndex(9);
        mMarkerE = (Marker) (mBaiduMap.addOverlay(ooE));

        // add ground overlay
//        LatLng southwest = new LatLng(31.1681670600, 108.3945579100);   //31.1681670600,108.3945579100
//        LatLng northeast = new LatLng(31.1681680600, 108.3945578100);
//        LatLngBounds bounds = new LatLngBounds.Builder().include(northeast)
//                .include(southwest).build();
//
//        OverlayOptions ooGround = new GroundOverlayOptions()
//                .positionFromBounds(bounds).image(bdGround).transparency(0.8f);
//        mBaiduMap.addOverlay(ooGround);
//
//        MapStatusUpdate u = MapStatusUpdateFactory
//                .newLatLng(bounds.getCenter());
//        mBaiduMap.setMapStatus(u);
//
//        mBaiduMap.setOnMarkerDragListener(new OnMarkerDragListener() {
//            public void onMarkerDrag(Marker marker) {
//            }
//
//            public void onMarkerDragEnd(Marker marker) {
//                Toast.makeText(
//                        OverlayDemo.this,
//                        "拖拽结束，新位置�?" + marker.getPosition().latitude + ", "
//                                + marker.getPosition().longitude,
//                        Toast.LENGTH_LONG).show();
//            }
//
//            public void onMarkerDragStart(Marker marker) {
//            }
//        });

        LatLng cenpt = new LatLng(31.1681670600,108.3945579100); //设定中心点坐标
       
        MapStatus mMapStatus = new MapStatus.Builder()//定义地图状态
        .target(cenpt)
        .zoom(11)  //设置缩放比例（11代表10KM/cm、12代表5Km/cm、10代表20km/cm）
        .build();  //定义MapStatusUpdate对象，以便描述地图状态将要发生的变化
        MapStatusUpdate mMapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mMapStatus);
        mBaiduMap.setMapStatus(mMapStatusUpdate);//改变地图状态
        
    }

    /**
     * 清除所有Overlay
     *
     * @param view
     */
    public void clearOverlay(View view) {
        mBaiduMap.clear();
        mMarkerA = null;
        mMarkerB = null;
        mMarkerC = null;
        mMarkerD = null;
        mMarkerE = null;
    }

    /**
     * 重新添加Overlay
     *
     * @param view
     */
    public void resetOverlay(View view) {
        clearOverlay(null);
        initOverlay();
    }

    private class SeekBarListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
            // TODO Auto-generated method stub
            float alpha = ((float) seekBar.getProgress()) / 10;
            if (mMarkerA != null) {
                mMarkerA.setAlpha(alpha);
            }
            if (mMarkerB != null) {
                mMarkerB.setAlpha(alpha);
            }
            if (mMarkerC != null) {
                mMarkerC.setAlpha(alpha);
            }
            if (mMarkerD != null) {
                mMarkerD.setAlpha(alpha);
            }
            if (mMarkerE != null) {
                mMarkerE.setAlpha(alpha);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // TODO Auto-generated method stub
        }

    }

    @Override
    protected void onPause() {
        // MapView的生命周期与Activity同步，当activity挂起时需调用MapView.onPause()
        mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        // MapView的生命周期与Activity同步，当activity恢复时需调用MapView.onResume()
        mMapView.onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        // MapView的生命周期与Activity同步，当activity�?毁时�?调用MapView.destroy()
        mMapView.onDestroy();
        super.onDestroy();
        // 回收 bitmap 资源
        boolean nThread = false;
        bdA.recycle();
        bdB.recycle();
        bdC.recycle();
        bdD.recycle();
        bd.recycle();
        bdGround.recycle();
        
    }

}

