package com.example.kxapp;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.kxapp.R;
import com.example.service.FinalConstant;
import com.example.service.GridViewAdapter;
import com.example.service.HttpReqService;
import com.example.service.PreferencesService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.GridView;
import android.widget.Toast;

public class AllinfoActivity extends Activity {

	private GridView gv_suggestinfo;
	private PreferencesService preferences;
	private String ServerIP;
	private String cmd;
	private Map<String, Object> reqparams;
	private String flag;
	private ArrayList<HashMap<String, Object>> eventinfo;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent=getIntent();
		flag = intent.getStringExtra("flag");
		setContentView(R.layout.activity_allinfo);
		initView();
	}

	private void initView() {
		//初始化网格控件
		gv_suggestinfo = (GridView) findViewById(R.id.suggest_info);
		preferences=new PreferencesService(getApplicationContext());
		Map<String, String> params=preferences.getPreferences();
		String serverip=params.get("serviceIP");
		if(serverip.equals("")) {
			preferences.save("192.168.16.75:8080", "", "", "", "", "", "");
		}else {
			ServerIP=serverip;
		}
		if(flag.equals("1")) {
			cmd=FinalConstant.REQUEST_SUGGEST_LJ;
		}else if(flag.equals("2")) {
			cmd=FinalConstant.REQUEST_SUGGEST_SS;
		}else if(flag.equals("3")) {
			cmd=FinalConstant.REQUEST_SUGGEST_WY;
		}else if(flag.equals("4")) {
			cmd=FinalConstant.REQUEST_SUGGEST_LY;
		}else {
			cmd=FinalConstant.REQUEST_SUGGEST_ZJ;
		}
		reqparams.put("cmd", cmd);
		new Thread(runnable).start();
		if(eventinfo!=null) {
			//数据适配器
			GridViewAdapter adapter=new GridViewAdapter(getApplicationContext(), eventinfo);
			gv_suggestinfo.setAdapter(adapter);
		}else {
			Toast.makeText(getApplicationContext(), "请求不到服务器数据", Toast.LENGTH_SHORT).show();
		}
	}
	Runnable runnable=new Runnable() {
		
		@Override
		public void run() {
			try {
				String path="http://"+ServerIP+"/AppService.php";
				String jsondata=HttpReqService.postRequest(path, reqparams, "GB2312");
				if(jsondata!=null) {
					Message msg=mHandler.obtainMessage(FinalConstant.QUERY_BACK_DATA);
					Bundle bundle=new Bundle();
					bundle.putString(FinalConstant.BACK_INFO, jsondata);
					msg.setData(bundle);
					mHandler.sendMessage(msg);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
	Handler mHandler=new Handler() {
		public void handleMessage(android.os.Message msg) {
			if(msg.what==FinalConstant.QUERY_BACK_DATA) {
				String jsondata=msg.getData().getString(FinalConstant.BACK_INFO);
				if(jsondata.equals("1")) {
					Toast.makeText(AllinfoActivity.this, "服务器没有打开或异常", Toast.LENGTH_SHORT).show();
				}else {
					try {
						JSONArray array=new JSONArray(jsondata);
						JSONObject tmp_cmd=(JSONObject) array.get(0);
						String str_cmd=tmp_cmd.getString("cmd");
						int len =0;
						len=array.length();
						if(len>1) {
							if(str_cmd.equals(FinalConstant.REQUEST_SUGGEST_REBACK)) {
								//存放数据，将数据传进数据适配器进行显示
								eventinfo=new ArrayList<HashMap<String,Object>>();
								for(int i=1;i<=(array.length()-1);i++) {
									HashMap<String, Object> hm=new HashMap<String, Object>();
									JSONObject data=(JSONObject) array.get(i);
									hm.put("name", URLEncoder.encode(data.getString("event_name"), "UTF-8"));
									hm.put("info", URLEncoder.encode(data.getString("info"), "UTF-8"));
									hm.put("time", data.getString("time"));
									eventinfo.add(hm);
								}
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
			}
		}
	};
}
