package com.example.kxapp;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.kxapp.R;
import com.example.kxapp.R.layout;
import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PreferencesService;
import com.example.service.PriceService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

/**
 * @Name:价格上报页面 *
 * @Description: 只针对价格上报员*
 * @author wuzhuoyu *
 * @Version:V1.00 *
 * @Create Date:2018-07-10*
 */
public class PriceReportActivity extends Activity {
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		nThread = false;
	}

	private String flag;
	private String[] items = new String[] { "沃柑", "大雅柑", "不知火", "砂糖橘", "贡桔", "蜜桔", "其他" };
	private String[] item = new String[] { "2L", "L", "M", "S", "2S", "等外果" };
	private String[] itemm = new String[] { "零售价", "批发价" };
	private String orangeType;
	private String orangeMold;
	private String priceType;
	private TextView ReportData;
	private Spinner OrangeType;
	private Spinner OrangeMold;
	private Spinner PriceType;
	private TextView Back;
	private TextView UnitPrice;
	private EditText ReportPrice;
	private EditText ReportPeople;
	private EditText ReportAddress;
	private Button btn_PriceSave;
	//线程初始状态
	private   boolean nThread= true;
	private PreferencesService preservice;
	/** 服务器地址 */
	private String serverIP = "120.79.76.116:8000";
	private EditText mEditText;
	private String m_oIPAddr = "61.157.134.34";
	private String m_oPort = "8081";
	private String m_oUser = "admin";
	private String m_oPsd = "cdjx1234cdjx1234";

	private Bitmap mge;
	private String Tag;
	public String cmd;
	public HashMap<String, Object> reqparams;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 获取身份标识
		Intent intent = getIntent();
		flag = intent.getStringExtra("flag");
		setContentView(R.layout.activity_price_report);

		ReportData = (TextView) findViewById(R.id.tv_reportdate);
		OrangeType = (Spinner) findViewById(R.id.sp_orangetype);
		OrangeMold = (Spinner) findViewById(R.id.sp_orangemold);
		PriceType = (Spinner) findViewById(R.id.sp_pricetype);
		ReportPrice = (EditText) this.findViewById(R.id.report_price);
		UnitPrice = (TextView) findViewById(R.id.unitprice);
		ReportPeople = (EditText) this.findViewById(R.id.report_people);
		ReportAddress = (EditText) this.findViewById(R.id.report_address);
		btn_PriceSave = (Button) this.findViewById(R.id.priceSave);
		btn_PriceSave.setOnClickListener(new PriceOnClickListener());

		// 获取系统时间
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd  ");// HH:mm:ss
		// 获取当前时间
		Date date = new Date(System.currentTimeMillis());
		ReportData.setText(simpleDateFormat.format(date));

		// 选择柑橘品种
		OrangeType.setPrompt("请选择柑橘品种");
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		OrangeType.setAdapter(adapter);
		OrangeType.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				orangeType = parent.getSelectedItem().toString();
			}
		});

		// 选择柑橘规格
		OrangeMold.setPrompt("请选择柑橘规格");
		ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, item);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		OrangeMold.setAdapter(adapter1);
		OrangeMold.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				orangeMold = parent.getSelectedItem().toString();
			}
		});

		// 选择价格类型
		PriceType.setPrompt("请选择价格类型");
		ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, itemm);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		PriceType.setAdapter(adapter2);
		PriceType.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				priceType = parent.getSelectedItem().toString();
			}
		});

		Back = (TextView) findViewById(R.id.back);
		findViewById(R.id.back).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				nThread = false;
				finish();
			}
		});

		// 获取网络配置
		preservice = new PreferencesService(getApplicationContext());
		Map<String, String> params = preservice.getPreferences();
		String serviceIP = params.get("serviceIP");
		String vedioIP = params.get("vedioIP");
		String vedioPort = params.get("vedioPort");
		String vedioUser = params.get("vedioUser");
		String vedioPsw = params.get("vedioPsw");
		if (serviceIP.equals("")) {
			preservice.save("120.79.76.116", "192.168.16.245", "502", "61.157.134.34", "8082", "admin",
					"cdjx1234cdjx1234");
		} else {
			serverIP = serviceIP;
			m_oIPAddr = vedioIP;
			m_oPort = vedioPort;
			m_oUser = vedioUser;
			m_oPsd = vedioPsw;
		}
	}

	/**
	 * 种植信息提交
	 * 
	 * @author Administrator
	 *
	 */
	private class PriceOnClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
//			String str = "";
//			if (mge != null) {
//				//将图片转成字符串
//				ByteArrayOutputStream baos = new ByteArrayOutputStream();
//				 mge.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//				 byte []imagedata=baos.toByteArray();
//				 str=Base64.encodeToString(imagedata, Base64.DEFAULT);
				 
				if ((ReportData.length() != 0) & (ReportPrice.length() != 0) & (ReportPeople.length() != 0)
						& (ReportAddress.length() != 0) & (UnitPrice.length() != 0) & (orangeType != null)
						& (orangeMold != null) & (priceType != null)) {
					Log.d("DebugTest", "-----sssss---" + ReportPrice);
					try {
						cmd = FinalConstant.PRICESUBMIT_REQUEST_SERVER;
						reqparams = new HashMap<String, Object>(); // 组织参数
						reqparams.put("cmd", cmd);
//						reqparams.put("TAG", Tag);
						reqparams.put("ReportData", URLEncoder.encode(ReportData.getText().toString().trim(), "UTF-8"));// 上报日期
						reqparams.put("OrangeType", URLEncoder.encode(orangeType, "UTF-8"));// 柑橘品种
						reqparams.put("OrangeMold", URLEncoder.encode(orangeMold, "UTF-8"));// 柑橘规格
						reqparams.put("ReportPrice", URLEncoder.encode(ReportPrice.getText().toString(), "UTF-8"));// 柑橘单价
						reqparams.put("UnitPrice", URLEncoder.encode(UnitPrice.getText().toString().trim(), "UTF-8"));// 单价
						reqparams.put("PriceType", URLEncoder.encode(priceType, "UTF-8"));// 价格类型
						reqparams.put("ReportPeople", URLEncoder.encode(ReportPeople.getText().toString(), "UTF-8"));// 上报人员
						reqparams.put("ReportAddress", URLEncoder.encode(ReportAddress.getText().toString(), "UTF-8"));// 价格采集点
							
						Log.d("DebugTest", "-----jilai -------" + orangeMold);
					} catch (UnsupportedEncodingException e) {
						Log.d("DebugTest", "-----jilai -------");
						e.printStackTrace();
					}
					Toast.makeText(PriceReportActivity.this, "上传成功!", Toast.LENGTH_LONG).show();
					new Thread(query).start();
					
				} else {
					Toast.makeText(PriceReportActivity.this, R.string.error, Toast.LENGTH_LONG).show();
				}

			};
		};

		// 提交信息子线程
		private Runnable query = new Runnable() {
			@Override
			public void run() {
				// 获取参数
				PriceService serviceip;
				serviceip = new PriceService(getApplicationContext());
				Map<String, String> params = serviceip.getPreferences();
				String url = params.get("serviceip");
				String path = "";
				if (url.equals("")) {
					path = "http://" + serverIP + "/AppService.php";
				} else {
					path = "http://" + serverIP + "/AppService.php";
				}
				Log.d("debugTest", "path -- " + path);
				// String path ="http://120.76.166.185:8000/testapp.php";
				try {
					String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
					Log.d("debugTest", "reqdata -- " + reqdata);
					if (reqdata != null) {
						// 子线程用sedMessage()方法传弟)Message对象
						Message msg = mhandler.obtainMessage(FinalConstant.QUERY_BACK_DATA);
						Bundle bundle = new Bundle();// 创建一个句柄
						bundle.putString(FinalConstant.BACK_INFO, reqdata);// 将reqdata填充入句柄
						msg.setData(bundle);// 设置一个任意数据值的Bundle对象。
						mhandler.sendMessage(msg);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			};
		};
		// 服务器返回信息
		private Handler mhandler = new Handler() {
			@SuppressLint("HandlerLeak")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == FinalConstant.QUERY_BACK_DATA) {
					Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_LONG).show();
					// 提交成功清空控件
					ReportPrice.setText("");
					ReportPeople.setText("");
					ReportAddress.setText("");
					
					
					
					
//					String jsonData = msg.getData().getString(FinalConstant.BACK_INFO);
//					try {
//						if (jsonData.equals("1")) {
//							Toast.makeText(PriceReportActivity.this, "服务器没有开启或异常", Toast.LENGTH_LONG).show();
//						} else {
//							JSONArray arr = new JSONArray(jsonData); // 收到JSON数组对象解析
//							JSONObject tmp_cmd = (JSONObject) arr.get(0); // 获取json数组对象返回命令
//							String str_cmd = tmp_cmd.getString("cmd");
//							Log.d("DebugTest", "arr_data -- " + arr);
//							int len = 0;
//							len = arr.length();
//							Log.d("debugTest", "len -- " + len);
//							if (len > 1) {
//
//								if (str_cmd.equals(FinalConstant.PRICESUBMIT_REBACK_SERVER)) {
//
//									JSONObject result_cmd = (JSONObject) arr.get(1);
//									if (result_cmd.getString("RESULT").equals("SUCCESS")) {
//										Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_LONG).show();
//										// 提交成功清空控件
//										ReportPrice.setText("");
//										ReportPeople.setText("");
//										ReportAddress.setText("");
//
//									}
//								}
//							}
//						}
//					} catch (JSONException e) {
//						e.printStackTrace();
//					}
				}
			};
		};
	}

