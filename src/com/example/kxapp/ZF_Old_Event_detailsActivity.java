package com.example.kxapp;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PlantService;
import com.example.service.PreferencesService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ZF_Old_Event_detailsActivity extends Activity {
	private boolean nThread;
	private TextView tv_event_name;
	private TextView tv_type;
	private TextView tv_eventinfo;
	private TextView tv_unit;
	private TextView tv_time;
	private TextView tv_person;
	private ImageView im_event;
	private EditText et_suggest;
	private Bundle data;
	private byte[] reqimage;
	private PreferencesService preservice;
	private String serverIP;
	private AlertDialog dialog;
	private String cmd;
	private HashMap<String, Object> reqparams;
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		nThread = false;
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.zf__old__event_detailsactivity);
		initView();
	}
	private void initView() {
		tv_event_name = (TextView) findViewById(R.id.tv_event_name);// 事件名称
		tv_type = (TextView) findViewById(R.id.tv_type);//事件类型
		tv_eventinfo = (TextView) findViewById(R.id.event_description);//事件描述
		tv_unit = (TextView) findViewById(R.id.report_unit);//上报单位
		tv_time = (TextView) findViewById(R.id.report_time);//上报事件
		tv_person = (TextView) findViewById(R.id.report_person);//上报人
		im_event = (ImageView) findViewById(R.id.im_event);//事件图片
		
		data = this.getIntent().getExtras();
		tv_event_name.setText(data.getString("event_name"));//事件名称
		tv_type.setText(data.getString("event_type"));//事件类型
		tv_eventinfo.setText("    "+data.getString("event_description"));//事件描述
		tv_unit.setText(data.getString("TAG"));//基地信息
		tv_time.setText(data.getString("report_time"));//上传时间
		tv_person.setText(data.getString("report_person"));//上传名字
		
		reqimage=Base64.decode(data.getString("image"), Base64.DEFAULT);
		im_event.setImageBitmap(BitmapFactory.decodeByteArray(reqimage, 0,reqimage.length));
		preservice = new PreferencesService(getApplicationContext());
	    Map<String, String> params = preservice.getPreferences();
	    String serviceIP = params.get("serviceIP");
	    if(serviceIP.equals("")){
	    	preservice.save("192.168.16.75:8080", "", "", "", "", "", "");
	    }else{
	    	serverIP = serviceIP;
	    }
		findViewById(R.id.suggest_cl).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showSuggestDialog();	
			}
		});
		
		findViewById(R.id.back).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}	

	public void showSuggestDialog() {
		dialog = new AlertDialog.Builder(ZF_Old_Event_detailsActivity.this).create();
		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
		Window window=dialog.getWindow();
		window.setContentView(R.layout.suggest_dialog);
		//window.setGravity(Gravity.CENTER);
		WindowManager.LayoutParams params = window.getAttributes(); 
		window.setAttributes(params);//此句代码一定要放在show()后面，否则不起作用  
		dialog.setCanceledOnTouchOutside(true);
		TextView tv_baseandperson = (TextView) window.findViewById(R.id.tv_baseandperson);
		tv_baseandperson.setText(data.getString("TAG")+"  "+data.getString("report_person"));
		et_suggest = (EditText) window.findViewById(R.id.et_suggest);
		window.clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		window.findViewById(R.id.btn_submit).setOnClickListener(new OnClickListener() {
		

			

			@Override
			public void onClick(View arg0) {
				String suggestback=et_suggest.getText().toString().trim();
				Date day=new Date();    
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm"); 
				String time=df.format(day); 
				if(et_suggest.length()>0) {
					try {
						 cmd = FinalConstant.ZF_SUGGESTSUBMIT_REQUEST_SERVER;
			             reqparams = new HashMap<String, Object>();	//组织参数
			        	 reqparams.put("cmd", cmd);
			        	 reqparams.put("TAG", data.get("TAG"));
			        	 reqparams.put("event_name", data.get("event_name"));
			        	 reqparams.put("time", time);
			        	 reqparams.put("suggest", URLEncoder.encode(suggestback,"UTF-8"));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					//提交图片
		             new Thread(submit).start();	
				}else {
					Toast.makeText(ZF_Old_Event_detailsActivity.this, "请输入您的意见！", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	//子线程
	private Runnable submit = new Runnable() {
		@Override
		public void run() {
			//获取参数
			PlantService  serviceip;
			serviceip = new PlantService(getApplicationContext());
		    Map<String, String> params = serviceip.getPreferences();
		    String url = params.get("serviceip");
		    String path = "";
		    if(url.equals("")){
		    	 path ="http://"+serverIP+"/AppService.php";
		    }else{
		    	path ="http://"+serverIP+"/AppService.php";
		    }
			Log.d("debugTest","path -- "+path);
			//String path ="http://120.76.166.185:8000/testapp.php";
			try{
				  String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
				  Log.d("debugTest","reqdata -- "+reqdata);
				  if(reqdata!= null){
					  //子线程用sedMessage()方法传弟)Message对象
					  Message msg = mhandler.obtainMessage(FinalConstant.QUERY_BACK_DATA);
					  Bundle bundle = new Bundle();//创建一个句柄
			          bundle.putString(FinalConstant.BACK_INFO, reqdata);//将reqdata填充入句柄
			          msg.setData(bundle);//设置一个任意数据值的Bundle对象。
					  mhandler.sendMessage(msg);
			  }
			}catch(Exception e){
				e.printStackTrace();
			}
		};
	};
	private Handler mhandler = new Handler() {
		@SuppressLint("HandlerLeak")
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == FinalConstant.QUERY_BACK_DATA) {

				Toast.makeText(ZF_Old_Event_detailsActivity.this, "二次意见反馈提交成功", Toast.LENGTH_SHORT).show();
				dialog.cancel();//退出dialog
			}
//				String jsonData = msg.getData().getString(FinalConstant.BACK_INFO);
//				try {
//						if(jsonData.equals("1"))
//						{
//							Toast.makeText(ZF_Event_detailsActivity.this, "服务器没有开启或异常", Toast.LENGTH_LONG).show();
//						}else{
//							JSONArray arr = new JSONArray(jsonData);  //收到JSON数组对象解析
//						    JSONObject tmp_cmd = (JSONObject) arr.get(0);	//获取json数组对象返回命令
//							String str_cmd= tmp_cmd.getString("cmd");
//						    Log.d("debugTest","arr_data -- "+arr);
//						    int len = 0;
//						    len = arr.length();
//						    Log.d("debugTest","len -- "+len);
//						    if(len>1)
//						    {
//							    if(str_cmd.equals(FinalConstant.ZF_SUGGESTSUBMIT_REBACK_SERVER))
//							    {
//							    	JSONObject result_cmd = (JSONObject) arr.get(1);
//							    	if(result_cmd.getString("RESULT").equals("SUCCESS")){
//							    		Toast.makeText(ZF_Event_detailsActivity.this, "意见提交成功", Toast.LENGTH_SHORT).show();
//							    		dialog.cancel();//退出dialog
//							    	}
//							    }
//						    }
//						}
//				} catch (JSONException e) {
//						e.printStackTrace();
//					}
//			}
	    };
	};
}

