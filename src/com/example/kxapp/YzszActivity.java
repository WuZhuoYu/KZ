package com.example.kxapp;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.baidu.mapsdkplatform.comapi.map.r;
import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PreferencesService;
import com.example.service.PriceService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class YzszActivity extends Activity {
	private Button btn_YzszSave;
	private TextView back;

	// 页面布局
	private View yzsz_qxz;
	private View yzsz_sqjc;
	private View yzsz_zj;
	// 空气温度
	private EditText et_Air_Temp_Up;
	private EditText et_Air_Temp_Floor;
	// 空气湿度
	private EditText et_Air_Damp_Up;
	private EditText et_Air_Damp_Floor;
	// 露点
	private EditText et_Dew_Point_Up;
	private EditText et_Dew_Point_Floor;
	// 光照度
	private EditText et_Illuminance_Up;
	private EditText et_Illuminance_Floor;
	// 太阳辐射
	private EditText et_Solar_Radiation_Up;
	private EditText et_Solar_Radiation_Floor;
	// 表层土壤湿度
	private EditText et_Surface_Soil_Moisture_Up;
	private EditText et_Surface_Soil_Moisture_Floor;
	// 浅层土壤湿度
	private EditText et_Shallow_Soil_Moisture_Up;
	private EditText et_Shallow_Soil_Moisture_Floor;
	// 中层土壤湿度
	private EditText et_Intermediate_Soil_Moisture_Up;
	private EditText et_Intermediate_Soil_Moisture_Floor;
	// 深层土壤湿度
	private EditText et_Deep_Soil_Moisture_Up;
	private EditText et_Deep_Soil_Moisture_Floor;
	// 表层土壤温度
	private EditText et_Surface_Soil_Temp_Up;
	private EditText et_Surface_Soil_Temp_Floor;
	// 浅层土壤温度
	private EditText et_Shallow_Soil_Temp_Up;
	private EditText et_Shallow_Soil_Temp_Floor;
	// 中层土壤温度
	private EditText et_Intermediate_Soil_Temp_Up;
	private EditText et_Intermediate_Soil_Temp_Floor;
	// 深层土壤温度
	private EditText et_Deep_Soil_Temp_Up;
	private EditText et_Deep_Soil_Temp_Floor;
	// 监测点空气温度
	private EditText et_Sqjc_Air_Temp_Up;
	private EditText et_Sqjc_Air_Temp_Floor;
	// 监测点空气湿度
	private EditText et_Sqjc_Air_Damp_Up;
	private EditText et_Sqjc_Air_Damp_Floor;
	// 监测点大气压力
	private EditText et_Sqjc_Barometric_Pressure_Up;
	private EditText et_Sqjc_Barometric_Pressure_Floor;
	// 监测点土壤水分
	private EditText et_Sqjc_Soil_Moisture_Up;
	private EditText et_Sqjc_Soil_Moisture_Floor;
	// 监测点土壤温度
	private EditText et_Sqjc_Soil_Temp_Up;
	private EditText et_Sqjc_Soil_Temp_Floor;
	// 监测点土壤PH
	private EditText et_Sqjc_Soil_PH_Up;
	private EditText et_Sqjc_Soil_PH_Floor;
	// 监测点土壤氨氮
	private EditText et_Sqjc_Soil_NH3N_Up;
	private EditText et_Sqjc_Soil_NH3N_Floor;
	// 监测点土壤硝铵
	private EditText et_Sqjc_Soil_NH4NO3_Up;
	private EditText et_Sqjc_Soil_NH4NO3_Floor;
	// 监测点钾离子
	private EditText et_Sqjc_Soil_K_Up;
	private EditText et_Sqjc_Soil_K_Floor;

	// 网络配置
	private PreferencesService preservice;
	private String serverIP = "120.79.76.116:8000";
	private String flag = "";

	/** 子线程提交命令 */
	private String cmd = "";
	/** 线程状态 */
	private boolean nThread = true;
	/** 子线程组合参数 */
	private Map<String, Object> reqparams;
	// 临江气象站标识
	protected String qxz;
	// 四圣村标识
	protected String sqjc1;
	// 乌杨村标识
	protected String sqjc2;
	// 南雅新全村标识
	protected String sqjc3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.yzsz);
		// 获取权限标志
//		Intent intent = getIntent();
//		flag = intent.getStringExtra("flag");
		flag=Flag.getFlag();
		InitView();

		// 返回点击事件
		back = (TextView) this.findViewById(R.id.back);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		// 获取网络配置
		preservice = new PreferencesService(getApplicationContext());
		Map<String, String> params = preservice.getPreferences();
		String serviceIP = params.get("serviceIP");
		if (serviceIP.equals("")) {
			preservice.save("192.168.16.75", "", "", "", "", "", "");
		} else {
			serverIP = serviceIP;
		}
	};

	private void InitView() {

		// 调用控件
		// 气象站控件
		yzsz_qxz = this.findViewById(R.id.yzsz_qxz);// 临江气象站页面
		yzsz_sqjc = this.findViewById(R.id.yzsz_sqjc);// 墒情监测站页面
		yzsz_zj = this.findViewById(R.id.yzsz_zj);// 赵家页面（无传感器监测站）
		et_Air_Temp_Up = (EditText) findViewById(R.id.et_air_temp_up);// 空气温度上限
		et_Air_Temp_Floor = (EditText) findViewById(R.id.et_air_temp_floor);// 空气温度下限
		et_Air_Damp_Up = (EditText) findViewById(R.id.et_air_damp_up);// 空气湿度上限
		et_Air_Damp_Floor = (EditText) findViewById(R.id.et_air_damp_floor);// 空气湿度下限
		et_Dew_Point_Up = (EditText) findViewById(R.id.et_dew_point_up);// 露点上限
		et_Dew_Point_Floor = (EditText) findViewById(R.id.et_dew_point_floor);// 露点下限
		et_Illuminance_Up = (EditText) findViewById(R.id.et_illuminance_up);// 光照度上限
		et_Illuminance_Floor = (EditText) findViewById(R.id.et_illuminance_floor);// 光照度下限
		et_Solar_Radiation_Up = (EditText) findViewById(R.id.et_solar_radiation_up);// 太阳辐射上限
		et_Solar_Radiation_Floor = (EditText) findViewById(R.id.et_solar_radiation_floor);// 太阳辐射下限
		et_Surface_Soil_Moisture_Up = (EditText) findViewById(R.id.et_Surface_soil_moisture_up);// 表层土壤水分上限
		et_Surface_Soil_Moisture_Floor = (EditText) findViewById(R.id.et_Surface_soil_moisture_floor);// 表层土壤水分下限
		et_Shallow_Soil_Moisture_Up = (EditText) findViewById(R.id.et_Shallow_soil_moisture_up);// 浅层土壤水分上限
		et_Shallow_Soil_Moisture_Floor = (EditText) findViewById(R.id.et_Shallow_soil_moisture_floor);// 浅层土壤水分下限
		et_Intermediate_Soil_Moisture_Up = (EditText) findViewById(R.id.et_Intermediate_soil_moisture_up);// 中层土壤水分上限
		et_Intermediate_Soil_Moisture_Floor = (EditText) findViewById(R.id.et_Intermediate_soil_moisture_floor);// 中层土壤水分下限
		et_Deep_Soil_Moisture_Up = (EditText) findViewById(R.id.et_Deep_soil_moisture_up);// 深层土壤水分上限
		et_Deep_Soil_Moisture_Floor = (EditText) findViewById(R.id.et_Deep_soil_moisture_floor);// 深层土壤水分下限
		et_Surface_Soil_Temp_Up = (EditText) findViewById(R.id.et_Surface_soil_temp_up);// 表层土壤温度上限
		et_Surface_Soil_Temp_Floor = (EditText) findViewById(R.id.et_Surface_soil_temp_floor);// 表层土壤温度下限
		et_Shallow_Soil_Temp_Up = (EditText) findViewById(R.id.et_Shallow_soil_temp_up);// 浅层土壤温度上限
		et_Shallow_Soil_Temp_Floor = (EditText) findViewById(R.id.et_Shallow_soil_temp_floor);// 浅层土壤温度下限
		et_Intermediate_Soil_Temp_Up = (EditText) findViewById(R.id.et_Intermediate_soil_temp_up);// 中层土壤温度上限
		et_Intermediate_Soil_Temp_Floor = (EditText) findViewById(R.id.et_Intermediate_soil_temp_floor);// 中层土壤温度下限
		et_Deep_Soil_Temp_Up = (EditText) findViewById(R.id.et_Deep_soil_temp_up);// 深层土壤温度上限
		et_Deep_Soil_Temp_Floor = (EditText) findViewById(R.id.et_Deep_soil_temp_floor);// 深层土壤温度下限
		// 监测点控件
		et_Sqjc_Air_Temp_Up = (EditText) findViewById(R.id.et_sqjc_air_temp_up);// 墒情监测空气温度上限
		et_Sqjc_Air_Temp_Floor = (EditText) findViewById(R.id.et_sqjc_air_temp_floor);// 墒情监测空气温度下限
		et_Sqjc_Air_Damp_Up = (EditText) findViewById(R.id.et_sqjc_air_damp_up);// 墒情监测空气湿度上限
		et_Sqjc_Air_Damp_Floor = (EditText) findViewById(R.id.et_sqjc_air_damp_floor);// 墒情监测空气湿度下限
		et_Sqjc_Barometric_Pressure_Up = (EditText) findViewById(R.id.et_sqjc_barometric_pressure_up);// 墒情监测大气压力上限
		et_Sqjc_Barometric_Pressure_Floor = (EditText) findViewById(R.id.et_sqjc_barometric_pressure_floor);// 墒情监测大气压力下限
		et_Sqjc_Soil_Moisture_Up = (EditText) findViewById(R.id.et_sqjc_soil_moisture_up);// 墒情监测土壤水分上限
		et_Sqjc_Soil_Moisture_Floor = (EditText) findViewById(R.id.et_sqjc_soil_moisture_floor);// 墒情监测土壤水分下限
		et_Sqjc_Soil_Temp_Up = (EditText) findViewById(R.id.et_sqjc_soil_temp_up);// 墒情监测土壤温度上限
		et_Sqjc_Soil_Temp_Floor = (EditText) findViewById(R.id.et_sqjc_soil_temp_floor);// 墒情监测土壤温度下限
		et_Sqjc_Soil_PH_Up = (EditText) findViewById(R.id.et_sqjc_soil_PH_up);// 墒情监测土壤PH上限
		et_Sqjc_Soil_PH_Floor = (EditText) findViewById(R.id.et_sqjc_soil_PH_floor);// 墒情监测土壤PH下限
		et_Sqjc_Soil_NH3N_Up = (EditText) findViewById(R.id.et_sqjc_soil_NH3N_up);// 墒情监测土壤氨氮上限
		et_Sqjc_Soil_NH3N_Floor = (EditText) findViewById(R.id.et_sqjc_soil_NH3N_floor);// 墒情监测土壤氨氮下限
		et_Sqjc_Soil_NH4NO3_Up = (EditText) findViewById(R.id.et_sqjc_soil_NH4NO3_up);// 墒情监测土壤硝铵上限
		et_Sqjc_Soil_NH4NO3_Floor = (EditText) findViewById(R.id.et_sqjc_soil_NH4NO3_floor);// 墒情监测土壤硝铵下限
		et_Sqjc_Soil_K_Up = (EditText) findViewById(R.id.et_sqjc_soil_K_up);// 墒情监测土壤氨氮上限
		et_Sqjc_Soil_K_Floor = (EditText) findViewById(R.id.et_sqjc_soil_K_floor);// 墒情监测土壤氨氮下限

		btn_YzszSave = (Button) findViewById(R.id.YzszSave);

		// 用户端点击事件
		if (flag.equals("1")) {
			peng1Page();// 临江
		} else if (flag.equals("2")) {
			peng2Page();// 四圣村
		} else if (flag.equals("3")) {
			peng3Page();// 乌杨村
		} else if (flag.equals("4")) {
			peng4Page();// 南雅新全村
		} else if (flag.equals("5")) {
			peng5Page();// 赵家
		}
	}

	// 临江基地
	private void peng1Page() {

		yzsz_qxz.setVisibility(View.VISIBLE);// 显示气象站布局
		yzsz_sqjc.setVisibility(View.GONE);// 隐藏
		yzsz_zj.setVisibility(View.GONE);// 隐藏赵家页面

		btn_YzszSave.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				if ((et_Air_Temp_Up.length() != 0) & (et_Air_Temp_Floor.length() != 0) & (et_Air_Damp_Up.length() != 0)
						& (et_Air_Damp_Floor.length() != 0) & (et_Dew_Point_Up.length() != 0)
						& (et_Dew_Point_Floor.length() != 0) & (et_Illuminance_Up.length() != 0)
						& (et_Illuminance_Floor.length() != 0) & (et_Solar_Radiation_Up.length() != 0)
						& (et_Solar_Radiation_Floor.length() != 0) & (et_Surface_Soil_Moisture_Up.length() != 0)
						& (et_Surface_Soil_Moisture_Floor.length() != 0) & (et_Shallow_Soil_Moisture_Up.length() != 0)
						& (et_Shallow_Soil_Moisture_Floor.length() != 0)
						& (et_Intermediate_Soil_Moisture_Up.length() != 0)
						& (et_Intermediate_Soil_Moisture_Floor.length() != 0) & (et_Deep_Soil_Moisture_Up.length() != 0)
						& (et_Deep_Soil_Moisture_Floor.length() != 0) & (et_Surface_Soil_Temp_Up.length() != 0)
						& (et_Surface_Soil_Temp_Floor.length() != 0) & (et_Shallow_Soil_Temp_Up.length() != 0)
						& (et_Shallow_Soil_Temp_Floor.length() != 0) & (et_Intermediate_Soil_Temp_Up.length() != 0)
						& (et_Intermediate_Soil_Temp_Floor.length() != 0) & (et_Deep_Soil_Temp_Up.length() != 0)
						& (et_Deep_Soil_Temp_Floor.length() != 0)) {
					cmd = FinalConstant.YZSZ_QXZ_SUBMIT_REQUEST_SERVER;// 气象站提交命令
					reqparams = new HashMap<String, Object>(); // 组织参数

					reqparams.put("cmd", cmd);
					reqparams.put("qxz", qxz);

					reqparams.put("et_Air_Temp_Up", et_Air_Temp_Up); // 空气温度上限
					reqparams.put("et_Air_Temp_Floor", et_Air_Temp_Floor); // 空气温度下限
					reqparams.put("et_Air_Damp_Up", et_Air_Damp_Up);// 空气湿度上限
					reqparams.put("et_Air_Damp_Floor", et_Air_Damp_Floor);// 空气湿度下限
					reqparams.put("et_Dew_Point_Up", et_Dew_Point_Up);// 露点上限
					reqparams.put("et_Dew_Point_Floor", et_Dew_Point_Floor);// 露点下限
					reqparams.put("et_Illuminance_Up", et_Illuminance_Up);// 光照度上限
					reqparams.put("et_Illuminance_Floor", et_Illuminance_Floor);// 光照度下限
					reqparams.put("et_Solar_Radiation_Up", et_Solar_Radiation_Up);// 太阳辐射上限
					reqparams.put("et_Solar_Radiation_Floor", et_Solar_Radiation_Floor);// 太阳辐射下限
					reqparams.put("et_Surface_Soil_Moisture_Up", et_Surface_Soil_Moisture_Up);// 表层土壤水分上限
					reqparams.put("et_Surface_Soil_Moisture_Floor", et_Surface_Soil_Moisture_Floor);// 表层土壤水分下限
					reqparams.put("et_Shallow_Soil_Moisture_Up", et_Shallow_Soil_Moisture_Up);// 浅层土壤水分上限
					reqparams.put("et_Shallow_Soil_Moisture_Floor", et_Shallow_Soil_Moisture_Floor);// 浅层土壤水分下限
					reqparams.put("et_Intermediate_Soil_Moisture_Up", et_Intermediate_Soil_Moisture_Up);// 中层土壤水分上限
					reqparams.put("et_Intermediate_Soil_Moisture_Floor", et_Intermediate_Soil_Moisture_Floor);// 中层土壤水分下限
					reqparams.put("et_Deep_Soil_Moisture_Up", et_Deep_Soil_Moisture_Up);// 深层土壤水分上限
					reqparams.put("et_Deep_Soil_Moisture_Floor", et_Deep_Soil_Moisture_Floor);// 深层土壤水分下限
					reqparams.put("et_Surface_Soil_Temp_Up", et_Surface_Soil_Temp_Up);// 表层土壤温度上限
					reqparams.put("et_Surface_Soil_Temp_Floor", et_Surface_Soil_Temp_Floor);// 表层土壤温度下限
					reqparams.put("et_Shallow_Soil_Temp_Up", et_Shallow_Soil_Temp_Up);// 浅层土壤温度上限
					reqparams.put("et_Shallow_Soil_Temp_Floor", et_Shallow_Soil_Temp_Floor);// 浅层土壤温度下限
					reqparams.put("et_Intermediate_Soil_Temp_Up", et_Intermediate_Soil_Temp_Up);// 中层土壤温度上限
					reqparams.put("et_Intermediate_Soil_Temp_Floor", et_Intermediate_Soil_Temp_Floor);// 中层土壤温度下限
					reqparams.put("et_Deep_Soil_Temp_Up", et_Deep_Soil_Temp_Up);// 深层土壤温度上限
					reqparams.put("et_Deep_Soil_Temp_Floor", et_Deep_Soil_Temp_Floor);// 深层土壤温度下限

					new Thread(query).start(); // 向服务器提交阈值设置参数

				} else {
					Toast.makeText(YzszActivity.this, R.string.error, Toast.LENGTH_LONG).show();
				}
			}
		});

	}

	// 四圣基地
	private void peng2Page() {
		yzsz_qxz.setVisibility(View.GONE);// 隐藏气象站布局
		yzsz_sqjc.setVisibility(View.VISIBLE);// 显示
		yzsz_zj.setVisibility(View.GONE);// 隐藏赵家页面
		Log.d("hhhh", "---hhhhhhhhhhhhhhh---");

		btn_YzszSave.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				if ((et_Sqjc_Air_Temp_Up.length() != 0) & (et_Sqjc_Air_Temp_Floor.length() != 0)
						& (et_Sqjc_Air_Damp_Up.length() != 0) & (et_Sqjc_Air_Damp_Floor.length() != 0)
						& (et_Sqjc_Barometric_Pressure_Up.length() != 0)& (et_Sqjc_Barometric_Pressure_Floor.length() != 0) 
						& (et_Sqjc_Soil_Moisture_Up.length() != 0)& (et_Sqjc_Soil_Moisture_Floor.length() != 0) 
						& (et_Sqjc_Soil_Temp_Up.length() != 0)& (et_Sqjc_Soil_Temp_Floor.length() != 0)
						& (et_Sqjc_Soil_PH_Up.length() != 0)& (et_Sqjc_Soil_PH_Floor.length() != 0)
						& (et_Sqjc_Soil_NH3N_Up.length() != 0)& (et_Sqjc_Soil_NH3N_Floor.length() != 0) 
						& (et_Sqjc_Soil_NH4NO3_Up.length() != 0)& (et_Sqjc_Soil_NH4NO3_Floor.length() != 0) 
						& (et_Sqjc_Soil_K_Up.length() != 0)& (et_Sqjc_Soil_K_Floor.length() != 0)) {
					cmd = FinalConstant.YZSZ_SQJC_SUBMIT_REQUEST_SERVER;// 墒情监测站提交命令
					Log.d("hello", "-------");
					reqparams = new HashMap<String, Object>(); // 组织参数
					reqparams.put("cmd", cmd);
					reqparams.put("sqjc", sqjc1);

					reqparams.put("et_Sqjc_Air_Temp_Up", et_Sqjc_Air_Temp_Up); // 空气温度上限
					reqparams.put("et_Sqjc_Air_Temp_Floor", et_Sqjc_Air_Temp_Floor); // 空气温度下限
					reqparams.put("et_Sqjc_Air_Damp_Up", et_Sqjc_Air_Damp_Up); // 空气湿度上限
					reqparams.put("et_Sqjc_Air_Damp_Floor", et_Sqjc_Air_Damp_Floor); // 空气湿度下限
					reqparams.put("et_Sqjc_Barometric_Pressure_Up", et_Sqjc_Barometric_Pressure_Up); // 大气压力上限
					reqparams.put("et_Sqjc_Barometric_Pressure_Floor", et_Sqjc_Barometric_Pressure_Floor); // 大气压力下限
					reqparams.put("et_Sqjc_Soil_Moisture_Up", et_Sqjc_Soil_Moisture_Up); // 土壤水分上限
					reqparams.put("et_Sqjc_Soil_Moisture_Floor", et_Sqjc_Soil_Moisture_Floor); // 土壤水分下限
					reqparams.put("et_Sqjc_Soil_Temp_Up", et_Sqjc_Soil_Temp_Up); // 土壤温度上限
					reqparams.put("et_Sqjc_Soil_Temp_Floor", et_Sqjc_Soil_Temp_Floor); // 土壤温度下限
					reqparams.put("et_Sqjc_Soil_PH_Up", et_Sqjc_Soil_PH_Up); // 土壤PH上限
					reqparams.put("et_Sqjc_Soil_PH_Floor", et_Sqjc_Soil_PH_Floor); // 土壤PH下限
					reqparams.put("et_Sqjc_Soil_NH3N_Up", et_Sqjc_Soil_NH3N_Up); // 土壤氨氮上限
					reqparams.put("et_Sqjc_Soil_NH3N_Floor", et_Sqjc_Soil_NH3N_Floor); // 土壤氨氮下限
					reqparams.put("et_Sqjc_Soil_NH4NO3_Up", et_Sqjc_Soil_NH4NO3_Up); // 土壤硝铵上限
					reqparams.put("et_Sqjc_Soil_NH4NO3_Floor", et_Sqjc_Soil_NH4NO3_Floor); // 土壤硝铵下限
					reqparams.put("et_Sqjc_Soil_K_Up", et_Sqjc_Soil_K_Up); // 土壤钾离子上限
					reqparams.put("et_Sqjc_Soil_K_Floor", et_Sqjc_Soil_K_Floor); // 土壤钾离子下限

					new Thread(query).start(); // 向服务器提交阈值设置参数
				}

			}
		});

	}

	// 乌杨基地
	private void peng3Page() {
		yzsz_qxz.setVisibility(View.GONE);// 隐藏气象站布局
		yzsz_sqjc.setVisibility(View.VISIBLE);// 显示
		yzsz_zj.setVisibility(View.GONE);// 隐藏赵家页面

		btn_YzszSave.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				if ((et_Sqjc_Air_Temp_Up.length() != 0) & (et_Sqjc_Air_Temp_Floor.length() != 0)
						& (et_Sqjc_Air_Damp_Up.length() != 0) & (et_Sqjc_Air_Damp_Floor.length() != 0)
						& (et_Sqjc_Barometric_Pressure_Up.length() != 0)
						& (et_Sqjc_Barometric_Pressure_Floor.length() != 0) & (et_Sqjc_Soil_Moisture_Up.length() != 0)
						& (et_Sqjc_Soil_Moisture_Floor.length() != 0) & (et_Sqjc_Soil_Temp_Up.length() != 0)
						& (et_Sqjc_Soil_Temp_Floor.length() != 0) & (et_Sqjc_Soil_PH_Up.length() != 0)
						& (et_Sqjc_Soil_PH_Floor.length() != 0) & (et_Sqjc_Soil_NH3N_Up.length() != 0)
						& (et_Sqjc_Soil_NH3N_Floor.length() != 0) & (et_Sqjc_Soil_NH4NO3_Up.length() != 0)
						& (et_Sqjc_Soil_NH4NO3_Floor.length() != 0) & (et_Sqjc_Soil_K_Up.length() != 0)
						& (et_Sqjc_Soil_K_Floor.length() != 0)) {
					cmd = FinalConstant.YZSZ_SQJC_SUBMIT_REQUEST_SERVER;// 墒情监测站提交命令
					Log.d("hello", "-------");
					reqparams = new HashMap<String, Object>(); // 组织参数
					reqparams.put("cmd", cmd);
					reqparams.put("sqjc", sqjc2);

					reqparams.put("et_Sqjc_Air_Temp_Up", et_Sqjc_Air_Temp_Up); // 空气温度上限
					reqparams.put("et_Sqjc_Air_Temp_Floor", et_Sqjc_Air_Temp_Floor); // 空气温度下限
					reqparams.put("et_Sqjc_Air_Damp_Up", et_Sqjc_Air_Damp_Up); // 空气湿度上限
					reqparams.put("et_Sqjc_Air_Damp_Floor", et_Sqjc_Air_Damp_Floor); // 空气湿度下限
					reqparams.put("et_Sqjc_Barometric_Pressure_Up", et_Sqjc_Barometric_Pressure_Up); // 大气压力上限
					reqparams.put("et_Sqjc_Barometric_Pressure_Floor", et_Sqjc_Barometric_Pressure_Floor); // 大气压力下限
					reqparams.put("et_Sqjc_Soil_Moisture_Up", et_Sqjc_Soil_Moisture_Up); // 土壤水分上限
					reqparams.put("et_Sqjc_Soil_Moisture_Floor", et_Sqjc_Soil_Moisture_Floor); // 土壤水分下限
					reqparams.put("et_Sqjc_Soil_Temp_Up", et_Sqjc_Soil_Temp_Up); // 土壤温度上限
					reqparams.put("et_Sqjc_Soil_Temp_Floor", et_Sqjc_Soil_Temp_Floor); // 土壤温度下限
					reqparams.put("et_Sqjc_Soil_PH_Up", et_Sqjc_Soil_PH_Up); // 土壤PH上限
					reqparams.put("et_Sqjc_Soil_PH_Floor", et_Sqjc_Soil_PH_Floor); // 土壤PH下限
					reqparams.put("et_Sqjc_Soil_NH3N_Up", et_Sqjc_Soil_NH3N_Up); // 土壤氨氮上限
					reqparams.put("et_Sqjc_Soil_NH3N_Floor", et_Sqjc_Soil_NH3N_Floor); // 土壤氨氮下限
					reqparams.put("et_Sqjc_Soil_NH4NO3_Up", et_Sqjc_Soil_NH4NO3_Up); // 土壤硝铵上限
					reqparams.put("et_Sqjc_Soil_NH4NO3_Floor", et_Sqjc_Soil_NH4NO3_Floor); // 土壤硝铵下限
					reqparams.put("et_Sqjc_Soil_K_Up", et_Sqjc_Soil_K_Up); // 土壤钾离子上限
					reqparams.put("et_Sqjc_Soil_K_Floor", et_Sqjc_Soil_K_Floor); // 土壤钾离子下限

					new Thread(query).start(); // 向服务器提交阈值设置参数
				}

			}
		});
	}

	// 南雅基地
	private void peng4Page() {
		yzsz_qxz.setVisibility(View.GONE);// 隐藏气象站布局
		yzsz_sqjc.setVisibility(View.VISIBLE);// 显示
		yzsz_zj.setVisibility(View.GONE);// 隐藏赵家页面

		btn_YzszSave.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				if ((et_Sqjc_Air_Temp_Up.length() != 0) & (et_Sqjc_Air_Temp_Floor.length() != 0)
						& (et_Sqjc_Air_Damp_Up.length() != 0) & (et_Sqjc_Air_Damp_Floor.length() != 0)
						& (et_Sqjc_Barometric_Pressure_Up.length() != 0)
						& (et_Sqjc_Barometric_Pressure_Floor.length() != 0) & (et_Sqjc_Soil_Moisture_Up.length() != 0)
						& (et_Sqjc_Soil_Moisture_Floor.length() != 0) & (et_Sqjc_Soil_Temp_Up.length() != 0)
						& (et_Sqjc_Soil_Temp_Floor.length() != 0) & (et_Sqjc_Soil_PH_Up.length() != 0)
						& (et_Sqjc_Soil_PH_Floor.length() != 0) & (et_Sqjc_Soil_NH3N_Up.length() != 0)
						& (et_Sqjc_Soil_NH3N_Floor.length() != 0) & (et_Sqjc_Soil_NH4NO3_Up.length() != 0)
						& (et_Sqjc_Soil_NH4NO3_Floor.length() != 0) & (et_Sqjc_Soil_K_Up.length() != 0)
						& (et_Sqjc_Soil_K_Floor.length() != 0)) {
					
					cmd = FinalConstant.YZSZ_SQJC_SUBMIT_REQUEST_SERVER;// 墒情监测站提交命令

					reqparams = new HashMap<String, Object>(); // 组织参数
					reqparams.put("cmd", cmd);
					reqparams.put("sqjc", sqjc3);

					reqparams.put("et_Sqjc_Air_Temp_Up", et_Sqjc_Air_Temp_Up); // 空气温度上限
					reqparams.put("et_Sqjc_Air_Temp_Floor", et_Sqjc_Air_Temp_Floor); // 空气温度下限
					reqparams.put("et_Sqjc_Air_Damp_Up", et_Sqjc_Air_Damp_Up); // 空气湿度上限
					reqparams.put("et_Sqjc_Air_Damp_Floor", et_Sqjc_Air_Damp_Floor); // 空气湿度下限
					reqparams.put("et_Sqjc_Barometric_Pressure_Up", et_Sqjc_Barometric_Pressure_Up); // 大气压力上限
					reqparams.put("et_Sqjc_Barometric_Pressure_Floor", et_Sqjc_Barometric_Pressure_Floor); // 大气压力下限
					reqparams.put("et_Sqjc_Soil_Moisture_Up", et_Sqjc_Soil_Moisture_Up); // 土壤水分上限
					reqparams.put("et_Sqjc_Soil_Moisture_Floor", et_Sqjc_Soil_Moisture_Floor); // 土壤水分下限
					reqparams.put("et_Sqjc_Soil_Temp_Up", et_Sqjc_Soil_Temp_Up); // 土壤温度上限
					reqparams.put("et_Sqjc_Soil_Temp_Floor", et_Sqjc_Soil_Temp_Floor); // 土壤温度下限
					reqparams.put("et_Sqjc_Soil_PH_Up", et_Sqjc_Soil_PH_Up); // 土壤PH上限
					reqparams.put("et_Sqjc_Soil_PH_Floor", et_Sqjc_Soil_PH_Floor); // 土壤PH下限
					reqparams.put("et_Sqjc_Soil_NH3N_Up", et_Sqjc_Soil_NH3N_Up); // 土壤氨氮上限
					reqparams.put("et_Sqjc_Soil_NH3N_Floor", et_Sqjc_Soil_NH3N_Floor); // 土壤氨氮下限
					reqparams.put("et_Sqjc_Soil_NH4NO3_Up", et_Sqjc_Soil_NH4NO3_Up); // 土壤硝铵上限
					reqparams.put("et_Sqjc_Soil_NH4NO3_Floor", et_Sqjc_Soil_NH4NO3_Floor); // 土壤硝铵下限
					reqparams.put("et_Sqjc_Soil_K_Up", et_Sqjc_Soil_K_Up); // 土壤钾离子上限
					reqparams.put("et_Sqjc_Soil_K_Floor", et_Sqjc_Soil_K_Floor); // 土壤钾离子下限
					
					new Thread(query).start(); // 向服务器提交阈值设置参数
				}

			}
		});
	}

	// 赵家基地
	private void peng5Page() {
		yzsz_qxz.setVisibility(View.GONE);// 隐藏气象站布局
		yzsz_sqjc.setVisibility(View.VISIBLE);// 隐藏墒情监测站
		yzsz_zj.setVisibility(View.VISIBLE);// 显示赵家页面

	}

	// 提交信息子线程
	private Runnable query = new Runnable() {
		@Override
		public void run() {
			// 获取参数
			PriceService serviceip;
			serviceip = new PriceService(getApplicationContext());
			Map<String, String> params = serviceip.getPreferences();
			String url = params.get("serviceip");
			String path = "";
			if (url.equals("")) {
				path = "http://" + serverIP + "/AppService.php";
			} else {
				path = "http://" + serverIP + "/AppService.php";
			}
			Log.d("debugTest", "path -- " + path);
			// String path ="http://120.76.166.185:8000/testapp.php";
			try {
				String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
				Log.d("debugTest", "reqdata -- " + reqdata);
				if (reqdata != null) {
					// 子线程用sedMessage()方法传弟)Message对象
					Message msg = mhandler.obtainMessage(FinalConstant.QUERY_BACK_DATA);
					Bundle bundle = new Bundle();// 创建一个句柄
					bundle.putString(FinalConstant.BACK_INFO, reqdata);// 将reqdata填充入句柄
					msg.setData(bundle);// 设置一个任意数据值的Bundle对象。
					mhandler.sendMessage(msg);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		};
	};

	// 服务器返回信息
	private Handler mhandler = new Handler() {
		@SuppressLint("HandlerLeak")
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == FinalConstant.QUERY_BACK_DATA) {
				String jsonData = msg.getData().getString(FinalConstant.BACK_INFO);
				try {
					if (jsonData.equals("1")) {
						Toast.makeText(YzszActivity.this, "服务器没有开启或异常", Toast.LENGTH_LONG).show();
					} else {
						JSONArray arr = new JSONArray(jsonData); // 收到JSON数组对象解析
						JSONObject tmp_cmd = (JSONObject) arr.get(0); // 获取json数组对象返回命令
						String str_cmd = tmp_cmd.getString("cmd");
						Log.d("DebugTest", "arr_data -- " + arr);
						int len = 0;
						len = arr.length();
						Log.d("debugTest", "len -- " + len);
						if (len > 1) {

							if (str_cmd.equals(FinalConstant.YZSZ_QXZ_SUBMIT_REBACK_SERVER)) {

								JSONObject result_cmd = (JSONObject) arr.get(1);
								if (result_cmd.getString("RESULT").equals("SUCCESS")) {
									Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_LONG).show();
								}
							} else if (str_cmd.equals(FinalConstant.YZSZ_SQJC_SUBMIT_REBACK_SERVER)) {
								JSONObject result_cmd = (JSONObject) arr.get(1);
								if (result_cmd.getString("RESULT").equals("SUCCESS")) {
									Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_LONG).show();
								}
							}
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		};
	};
}
