package com.example.kxapp;

import java.util.Map;

import com.example.service.PreferencesService;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class YH_Old_Details_SuggestBackActivity extends Activity {

	private TextView tv_event_name;
	private TextView time;
	private TextView suggest;
	private TextView TAG;
	private Bundle data;
	private PreferencesService preservice;
	private String serverIP = "120.79.76.116:8000";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.yh_old_details_suggest_backactivity);
		
		
		findViewById(R.id.back).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		initView();
	}

	private void initView() {
		tv_event_name = (TextView) findViewById(R.id.tv_event_name);// 事件名称
		time = (TextView) findViewById(R.id.time);//反馈时间
		suggest = (TextView) findViewById(R.id.suggest);//反馈意见
		TAG = (TextView) findViewById(R.id.TAG);//反馈基地
		
		data = this.getIntent().getExtras();
		tv_event_name.setText(data.getString("event_name"));//事件名称
		time.setText(data.getString("time"));//意见反馈时间
		suggest.setText("    "+data.getString("suggest"));//意见反馈内容
		TAG.setText(data.getString("TAG"));//反馈基地
		
		
		
		
		//获取网络配置
		preservice = new PreferencesService(getApplicationContext());
	    Map<String, String> params = preservice.getPreferences();
	    String serviceIP = params.get("serviceIP");
	    if(serviceIP.equals("")){
	    	preservice.save("192.168.16.75:8080", "", "", "", "", "", "");
	    }else{
	    	serverIP  = serviceIP;
	    }
	}

}
