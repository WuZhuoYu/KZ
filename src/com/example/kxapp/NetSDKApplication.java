package com.example.kxapp;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.baidu.mapapi.SDKInitializer;
import com.company.NetSDK.NET_DEVICEINFO_Ex;
import com.example.service.NetSDKLib;
import com.example.service.ToolKits;
import java.lang.ref.WeakReference;

/**
 * Created by 29779 on 2017/4/14.
 */
public class NetSDKApplication extends Application{
    private long mLoginHandle;
    private NET_DEVICEINFO_Ex mDeviceInfo;
    private static NetSDKApplication instance ;
    
    public static NetSDKApplication getInstance(){
        return instance;
       
    }
    @Override
    public void onCreate() {
    	Log.d("DebugTest", "---Application设备信息1---"+mLoginHandle+"-----"+mDeviceInfo);
        super.onCreate();
        NetSDKLib.getInstance().init();
        SDKInitializer.initialize(getApplicationContext());
        instance = this;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
//            registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
//                @Override
//                public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
//                	Log.d("DebugTest", "---Application设备信息2---"+mLoginHandle+"-----"+mDeviceInfo);
//                }
//
//                @Override
//                public void onActivityStarted(Activity activity) {
//                	Log.d("DebugTest", "---Application设备信息3---"+mLoginHandle+"-----"+mDeviceInfo);
//                }
//
//                @Override
//                public void onActivityResumed(Activity activity) {
//                        DHActivityManager.getManager().setCurrentActivity(activity);
//                        Log.d("DebugTest", "---Application设备信息4---"+mLoginHandle+"-----"+mDeviceInfo);
//                }
//
//                @Override
//                public void onActivityPaused(Activity activity) {
//                	Log.d("DebugTest", "---Application设备信息5---"+mLoginHandle+"-----"+mDeviceInfo);
//                }
//
//                @Override
//                public void onActivityStopped(Activity activity) {
//                	Log.d("DebugTest", "---Application设备信息6---"+mLoginHandle+"-----"+mDeviceInfo);
//                }
//
//                @Override
//                public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
//                	Log.d("DebugTest", "---Application设备信息7---"+mLoginHandle+"-----"+mDeviceInfo);
//                }
//
//                @Override
//                public void onActivityDestroyed(Activity activity) {
//                	Log.d("DebugTest", "---Application设备信息8---"+mLoginHandle+"-----"+mDeviceInfo);
//                }
//            });
//        }
        setLoginHandle(0);
        setDeviceInfo(null);
        Log.i("log_data", "applictation");
        Log.d("DebugTest", "---Application设备信息9---"+mLoginHandle+"-----"+mDeviceInfo);
    }

    public long getLoginHandle() {
    	Log.d("debugtest", "-------"+mLoginHandle);
        return mLoginHandle;
        
    }

    public void setLoginHandle(long loginHandle) {
        this.mLoginHandle = loginHandle;
        Log.d("DebugTest", "---Application设备信息10---"+mLoginHandle+"-----"+mDeviceInfo);
        
    }

    public NET_DEVICEINFO_Ex getDeviceInfo() {
        return mDeviceInfo;
        
    }

    public void setDeviceInfo(NET_DEVICEINFO_Ex mDeviceInfo) {
        this.mDeviceInfo = mDeviceInfo;
        Log.d("DebugTest", "---Application设备信息11---"+mLoginHandle+"-----"+mDeviceInfo);
    }

    public static class DHActivityManager{
        private static DHActivityManager manager = new DHActivityManager();
        private WeakReference<Activity> current;
        private DHActivityManager(){
        }
        public static DHActivityManager getManager(){
            return manager;
        }

        public Activity getTopActivity(){
            if (current!=null)
                return current.get();
            return null;
        }
        public void setCurrentActivity(Activity obj){
            current = new WeakReference<Activity>(obj);
        }
    }

}
