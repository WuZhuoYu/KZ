package com.example.kxapp;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PreferencesService;
import com.example.kxapp.R;

/**
@Name:环境监测页面 *
@Description: * 
@author wuzhuoyu * 
@Version:V1.00 * 
@Create Date:2018-07-27 *
*/
public class EnvironmentActivity extends Activity {
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		nThread = false;
	}
	/**监测点选择*/
    private TextView        tv_content;
 	private ImageView       img_right;
 	
    /**返回*/
    private TextView back;
    private TextView peng_title;
    private String mYear ; // 获取当前年份  
    private String mMonth;// 获取当前月份  
    private String mDay;// 获取当前月份的日期号码
    
    /**临江气象站数据布局*/
    private View include_changeOne;
    
    /**无线墒情数据布局*/
    private View include_changeTwo;
    
    /**气象站布局*/
    private View include_changeThree;   
    
    /**气象站布局*/
    private View include_changeFour;
    
    /**测试点1表层土壤水分*/
    private TextView data1;
    
    /**测试点1浅层土壤水分*/
    private TextView data2;
    
    /**测试点1深层土壤水分*/
    private TextView data3;
    
    /**测试点2表层土壤水分*/
    private TextView data4;
    
    /**测试点2浅层土壤水分*/
    private TextView data5;
    
    /**测试点2深层土壤水分*/
    private TextView data6;
    
    /**测试点3表层土壤水分*/
    private TextView data7;
   
    /**测试点3浅层土壤水分*/
    private TextView data8;
    
    /**空气温度*/
    private TextView data9;
    
    /**空气湿度*/
    private TextView data10;
    
    /**测试点1表层土壤水分*/
    private TextView data11;
    
    /**测试点1浅层土壤水分*/
    private TextView data12;
    
    /**测试点1深层土壤水分*/
    private TextView data13;
    
    /**测试点2表层土壤水分*/
    private TextView data14;
    
    /**测试点2浅层土壤水分*/
    private TextView data15;
    
    /**测试点2深层土壤水分*/
    private TextView data16;
    
    /**测试点3表层土壤水分*/
    private TextView data17;
      
    
    /**气象站表层土壤水分*/
    private TextView yt_data1;
    
    /**气象站浅层土壤水分*/
    private TextView yt_data2;
    
    /**气象站中层土壤水分*/
    private TextView yt_data3;
    
    /**气象站深层土壤水分*/
    private TextView yt_data4;
    /**
     * <p>气象站空气温度</p>
     */
    private TextView yt_data5;
    
    /**气象站空气湿度*/
    private TextView yt_data6;
    
    /**气象站大气压力*/
    private TextView yt_data7;
    
    /**气象站土壤温度*/
    private TextView yt_data8;
    
    /**气象站风速*/
    private TextView yt_data9;
    
    
    /**子线程提交命令*/
    private  String  cmd="";
    
    /**子线程组合参数*/
    private  Map<String, Object>  reqparams;
 	
	
	private final String 	TAG						= "MainActivity";
	
    Handler handler = new Handler();  
    
    /**切换大棚时加载页面缓冲*/
    private TextView loadTV = null;
	private View linkPage = null;
	private ImageView linkimage = null;
	
	/**线程状态*/
	private   boolean nThread= true;

	private PreferencesService  preservice;
	
	/**服务器地址*/	
	private String serverIP = "192.168.16.75:8080";
	private String flag="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_environment);
//		Intent intent=getIntent();
//		flag=intent.getStringExtra("flag");
//		Log.d("debugTest", "hhhhhhhhhhhhhhhhhhh"+flag);
		flag=Flag.getFlag();
		
		back  = (TextView) this.findViewById(R.id.back);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				nThread = false;
				finish();
			}
		});
		
		
	
		
		InitDataView();	//初始化视频组件
 		
        
		//获取网络配置
		preservice = new PreferencesService(getApplicationContext());
	    Map<String, String> params = preservice.getPreferences();
	    String serviceIP = params.get("serviceIP");
	    String vedioIP = params.get("vedioIP");
	    String vedioPort = params.get("vedioPort");
	    String vedioUser = params.get("vedioUser");
	    String vedioPsw = params.get("vedioPsw");
	    if(serviceIP.equals("")){
	    	preservice.save("192.168.16.75:8080", "192.168.16.245", "502", "61.157.134.34", "8082", "admin", "cdjx1234cdjx1234");
	    }else{
	    	serverIP = serviceIP;
	    }
	    
	};
	/**
	 * 初始化传感器组件
	 */
	private void InitDataView(){
		include_changeOne = this.findViewById(R.id.include_changeOne);
		include_changeTwo = this.findViewById(R.id.include_changeTwo);
		include_changeThree = this.findViewById(R.id.include_changeThree);
		include_changeFour = this.findViewById(R.id.include_changeFour);
 		data1 = (TextView) findViewById(R.id.data1);
 		data2 = (TextView) findViewById(R.id.data2);
 		data3 = (TextView) findViewById(R.id.data3);
 		data4 = (TextView) findViewById(R.id.data4);
 		data5 = (TextView) findViewById(R.id.data5);
 		data6 = (TextView) findViewById(R.id.data6);
 		data7 = (TextView) findViewById(R.id.data7);
 		data8 = (TextView) findViewById(R.id.data8);
 		data9 = (TextView) findViewById(R.id.data9);
 		data10 = (TextView) findViewById(R.id.data10);
 		data11 = (TextView) findViewById(R.id.data11);
 		data12 = (TextView) findViewById(R.id.data12);
 		data13 = (TextView) findViewById(R.id.data13);
 		data14 = (TextView) findViewById(R.id.data14);
 		data15 = (TextView) findViewById(R.id.data15);
 		data16 = (TextView) findViewById(R.id.data16);
 		data17 = (TextView) findViewById(R.id.data17);
 		yt_data1 = (TextView) findViewById(R.id.yt_data1);
 		yt_data2 = (TextView) findViewById(R.id.yt_data2);
 		yt_data3 = (TextView) findViewById(R.id.yt_data3);
 		yt_data4 = (TextView) findViewById(R.id.yt_data4);
 		yt_data5 = (TextView) findViewById(R.id.yt_data5);
 		yt_data6 = (TextView) findViewById(R.id.yt_data6);
 		yt_data7 = (TextView) findViewById(R.id.yt_data7);
 		yt_data8 = (TextView) findViewById(R.id.yt_data8);
 		yt_data9 = (TextView) findViewById(R.id.yt_data9);
 		
 		getcurrentTime();	//获取系统时间
 		peng_title = (TextView) findViewById(R.id.peng_title);
 		peng_title.setText("今日("+mYear+"-"+mMonth+"-"+mDay+")_实时数据");
 		tv_content = (TextView) findViewById(R.id.e_content);
 		img_right = (ImageView) findViewById(R.id.e_right);
 		 loadTV = (TextView) this.findViewById(R.id.loadingTV);
         linkPage = (View) this.findViewById(R.id.linkPage);
 	    linkimage = (ImageView) findViewById(R.id.loadingVideo);
 		if("0".equals(flag)||"6".equals(flag)) { 	 		
 	 		//政府端的点击事件
 			cmd = FinalConstant.QXZFIND_REQUEST_SERVER;
 	        reqparams = new HashMap<String, Object>();	//组织参数
 	    	reqparams.put("cmd", cmd);
 	        new Thread(query_int).start();
 		    img_right.setOnClickListener(new View.OnClickListener() {
 				@Override
 				public void onClick(View v) {
 					//下拉列表
 					AlertDialog.Builder builder = new AlertDialog.Builder(EnvironmentActivity.this);
 		            builder.setTitle("选择类型");
 		            String str_wd = "临江镇#四圣村#乌杨村#南雅镇#赵家街道";
 		            //指定下拉列表的显示数据
 		            final String[] cities = str_wd.split("#");
 		            //设置一个下拉的列表选择项
 		            builder.setItems(cities, new DialogInterface.OnClickListener()
 		            {
 		                    @Override
 		                    public void onClick(DialogInterface dialog, int which)
 		                    {
 		                    	tv_content.setText(cities[which]);
 		                    	if(which==0){
 		                    		peng1Page();//临江基地
 		                    		Log.d("DebugTest", "----页面切换-----");
 		                    	}
 		                       	if(which==1){
 		                    		peng2Page();//四圣基地
 		                    	}
 		                       	if(which==2){
 		                    		peng3Page();//乌杨基地
 		                    	}
 		                       	if(which==3){
 		                    		peng4Page();//南雅基地
 		                    	} 		                       
 		                        if(which==4){
		                    		peng5Page();//赵家基地
		                    	} 	
 		                    }
 		             });
 		             builder.show();
 				}
 			});	
 		}else if("1".equals(flag)) {
 			img_right.setVisibility(View.GONE);
 			tv_content.setText("临江镇福德村");
 			peng1Page();
 		}else if("2".equals(flag)) {
 			img_right.setVisibility(View.GONE);
 			tv_content.setText("白鹤街道四圣村");
 			peng2Page();
 		}else if("3".equals(flag)) {
 			img_right.setVisibility(View.GONE);
 			tv_content.setText("丰乐街道乌杨村");
 			peng3Page();
 		}else if("4".equals(flag)) {
 			img_right.setVisibility(View.GONE);
 			tv_content.setText("南雅镇新全村");
 			peng4Page();
 		}else if ("5".equals(flag)) {
 			img_right.setVisibility(View.GONE);
 			tv_content.setText("赵家街道姚家村");
			Toast.makeText(getApplicationContext(), "该基地暂无传感器数据监测站",Toast.LENGTH_LONG ).show();
			peng5Page();
		}		      	   
	};
	
	/*
     * 获取系统当前时间
     */
	@SuppressLint("SimpleDateFormat")
	private void getcurrentTime() {
	    final Calendar c = Calendar.getInstance();  
	    c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));  
	    mYear = String.valueOf(c.get(Calendar.YEAR)); // 获取当前年份  
	    mMonth = String.valueOf(c.get(Calendar.MONTH) + 1);// 获取当前月份  
	    mDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));// 获取当前月份的日期号码
	};		
 	/**
 	 * 临江气象站
 	 */
 	private void peng1Page(){
 		cmd = FinalConstant.QXZFIND_REQUEST_SERVER;
 		nThread = true;	//query_int线程开启
 		
 		
 		include_changeOne.setVisibility(View.VISIBLE);//显示临江气象站
    	include_changeTwo.setVisibility(View.GONE);//隐藏
    	include_changeThree.setVisibility(View.GONE);
    	include_changeFour.setVisibility(View.GONE);
    	
        reqparams = new HashMap<String, Object>();	//组织参数
    	reqparams.put("cmd", cmd);
        new Thread(query_int).start();	//从服务器获取监测点1传感器参数       
 	};
 	/**
 	 * 四圣村基地无线墒情监测站
 	 */
 	private void peng2Page(){
 		cmd = FinalConstant.SQZ1FIND_REQUEST_SERVER;
 		nThread = true;	//query_int线程开启
 		
		include_changeOne.setVisibility(View.GONE);
    	include_changeTwo.setVisibility(View.VISIBLE);
    	include_changeThree.setVisibility(View.GONE);
     	include_changeFour.setVisibility(View.GONE);
        reqparams = new HashMap<String, Object>();	//组织参数
    	reqparams.put("cmd", cmd);
    	new Thread(query_int).start();	//从服务器获取监测点2传感器参数
    	
 	};
 	/**
 	 * 乌杨村基地无线墒情监测站
 	 */
 	private void peng3Page(){
 		cmd = FinalConstant.SQZ2FIND_REQUEST_SERVER;
 		nThread = true;	//query_int线程开启
 		
		include_changeOne.setVisibility(View.GONE);
    	include_changeTwo.setVisibility(View.VISIBLE);
    	include_changeThree.setVisibility(View.GONE);
    	include_changeFour.setVisibility(View.GONE);
    
        reqparams = new HashMap<String, Object>();	//组织参数
    	reqparams.put("cmd", cmd);
    	new Thread(query_int).start();	
    	
 	};
 	/**
 	 * 南雅基地无线墒情监测站
 	 */
 	private void peng4Page(){
 		nThread = true;	//query_int线程开启
 		cmd = FinalConstant.SQZ3FIND_REQUEST_SERVER;
 		include_changeOne.setVisibility(View.GONE);	
    	include_changeTwo.setVisibility(View.VISIBLE);
    	include_changeThree.setVisibility(View.GONE);
    	include_changeFour.setVisibility(View.GONE);
    	
        reqparams = new HashMap<String, Object>();	//组织参数
    	reqparams.put("cmd", cmd);
    	new Thread(query_int).start();	
    	Log.d("debugTest", "cmd----"+cmd);
    	
 	};
 	/**
 	 * 赵家街道基地（没有监测站，只做显示）
 	 */
 	private void peng5Page() {
//nThread = true;	//query_int线程开启
// 		
 		include_changeOne.setVisibility(View.GONE);	
    	include_changeTwo.setVisibility(View.GONE);
    	include_changeThree.setVisibility(View.GONE);
    	include_changeFour.setVisibility(View.VISIBLE);
//    	cmd = FinalConstant.SQZ3FIND_REQUEST_SERVER;
//        reqparams = new HashMap<String, Object>();	//组织参数
//    	reqparams.put("cmd", cmd);
//    	new Thread(query_int).start();	
//    	Log.d("debugTest", "cmd----"+cmd);
	}
 	//子线程  每个10秒从服务器获取传感器数据
    private Runnable query_int = new Runnable() {
 			@Override
 			public void run() {
	 				while (nThread){	
			 			try{
			 				    String path ="http://"+serverIP+"/AppService.php";
		 					
			 					String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
			 					Log.d("debugTest","reqdata -- "+reqdata);
			 					if(reqdata!= null){
			 							    //子线程用sedMessage()方法传弟)Message对象
			 								Message msg = mhandler_get.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
			 								Bundle bundle = new Bundle();//创建一个句柄
			 							    bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);//将reqdata填充入句柄
			 							    msg.setData(bundle);//设置一个任意数据值的Bundle对象。
			 							    mhandler_get.sendMessage(msg);
			 					 }
			 					Thread.sleep(1000);//线程暂停10秒，单位毫秒  启动线程后，线程每10s发送一次消息
			 			}catch(Exception e){
			 					e.printStackTrace();
			 			}
	 				}
		 			
 			};
 	};
 	@SuppressLint("HandlerLeak")
 	private Handler mhandler_get = new Handler() {
 			@SuppressLint("HandlerLeak")
 			@Override
 			public void handleMessage(Message msg) {
 				if (msg.what == FinalConstant.GT_QUERY_BACK_DATA) {
 					String jsonData = msg.getData().getString(FinalConstant.GT_BACK_INFO);
 					try {
	 						if(jsonData.equals("1"))
							{
//	 							linkPage.setVisibility(View.GONE);
//	 			  				loadingVideo.clearAnimation();//清除动画	
								Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
								nThread = false;
							}else{
//								linkPage.setVisibility(View.GONE);
//				  				loadingVideo.clearAnimation();//清除动画	
								JSONArray arr = new JSONArray(jsonData);  //收到JSON数组对象解析
								Log.d("arr","arr -- "+arr);
								
							    JSONObject tmp_cmd = (JSONObject) arr.get(0);	//获取json数组对象返回命令
								String str_cmd= tmp_cmd.getString("cmd"); 
							    int len = 0;
							    len = arr.length();
							    if(len>1)
							    {
								   
								    
								    //环境综合监测站显示处理
								    if(str_cmd.equals(FinalConstant.QXZFIND_REBACK_SERVER))
								    {
								    	ShowPeng(arr);
								    }
								  //墒情监测站显示处理
								    if(str_cmd.equals(FinalConstant.SQZ1FIND_REBACK_SERVER)||str_cmd.equals(FinalConstant.SQZ2FIND_REBACK_SERVER)||str_cmd.equals(FinalConstant.SQZ3FIND_REBACK_SERVER))
								    {
								    	ShowQxz(arr);
								    }
								}
							}
 					    
 				    } catch (JSONException e) {
 						e.printStackTrace();
 					}				
 			   }
 			
 		    };

 		    /**
 		     * 气象站数据显示
 		     * @param arr
 		     */
 			private void ShowPeng(JSONArray arr) {
 				TextView[] data={data1,data2,data3,data4,data5,data6,data7,data8,data9,data10,data11,data12,data13,data14,data15,data16,data17};
 					try {
 						    if(arr.get(1).equals(false)){
 						    	for(int i=0;i<data.length;i++)
 						    	{
 						    		data[i].setText("----");
 						    	}
 						    }
 						    if(!arr.get(1).equals(false))
 						    {
 						    	//获取json数组对象有效数据
 						    	JSONArray arr_data = (JSONArray) arr.get(1);
 						    	Log.d("json数组", "json数组"+arr_data);
 								JSONObject temp = (JSONObject) arr_data.get(0);
 								data[0].setText(temp.getString("yn_air_temp")+" ℃");
 								data[1].setText(temp.getString("yn_air_humi")+" %RH");
 								data[2].setText(temp.getString("yn_dew_point")+" ℃");
 								data[3].setText(temp.getString("yn_illuminance")+" Lux");
 								data[4].setText(temp.getString("yn_radiation")+" W/m²");
 								data[5].setText(temp.getString("yn_rainfall")+" mm");
 								data[6].setText(temp.getString("yn_evaporation")+" mm");
 								data[7].setText(temp.getString("yn_water_1content")+" %RH");
 								data[8].setText(temp.getString("yn_water_2content")+" %RH");
 								data[9].setText(temp.getString("yn_water_3content")+" %RH");
 								data[10].setText(temp.getString("yn_water_4content")+" %RH");
 								data[11].setText(temp.getString("yn_solid_1temp")+" ℃");
 								data[12].setText(temp.getString("yn_solid_2temp")+" ℃");
 								data[13].setText(temp.getString("yn_solid_3temp")+" ℃");
 								data[14].setText(temp.getString("yn_solid_4temp")+" ℃");
 								data[15].setText(temp.getString("yn_wind_speed")+" m/s");
 								data[16].setText(temp.getString("yn_wind_direction")+"");
 						    }
 			    } catch (JSONException e) {
 					e.printStackTrace();
 				}				
 			};
 			
 			/**
 		     * 墒情站数据显示
 		     * @param arr
 		     */
 			private void ShowQxz(JSONArray arr) {
 						TextView[] qxz_data={yt_data1,yt_data2,yt_data3,yt_data4,yt_data5,yt_data6,yt_data7,yt_data8,yt_data9};
 						try {
 							    if(arr.get(1).equals(false)){
 							    	for(int i=0;i<qxz_data.length;i++)
 							    	{
 							    		qxz_data[i].setText("----");
 							    	}
 							    }
 							    if(!arr.get(1).equals(false))
 							    {
 							    	//获取json数组对象有效数据
 							    	JSONArray arr_data = (JSONArray) arr.get(1);
 									JSONObject temp = (JSONObject) arr_data.get(0);
 									qxz_data[0].setText(temp.getString("yn_air_temp")+" ℃");
 									qxz_data[1].setText(temp.getString("yn_air_humi")+" %RH");
 									qxz_data[2].setText(temp.getString("yn_air_pressure")+" hpa");
 									qxz_data[3].setText(temp.getString("yn_water_content")+" %RH");
 									qxz_data[4].setText(temp.getString("yn_soil_temp")+" ℃");
 									qxz_data[5].setText(temp.getString("yn_soil_ph")+"");
 									qxz_data[6].setText(temp.getString("yn_soil_nh3n")+" ppm");
 									qxz_data[7].setText(temp.getString("yn_soil_nh4no3")+" ppm");
 									qxz_data[8].setText(temp.getString("yn_soil_k")+" ppm");
 									
 							    }
 				    } catch (JSONException e) {
 						e.printStackTrace();
 					}				
 			};
 			
    };


}
