package com.example.kxapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.DigitsKeyListener;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.company.NetSDK.CB_fRealDataCallBackEx;
import com.company.NetSDK.CB_fSnapRev;
import com.company.NetSDK.CFG_DSPENCODECAP_INFO;
import com.company.NetSDK.CFG_ENCODE_INFO;
import com.company.NetSDK.CFG_VIDEOENC_OPT;
import com.company.NetSDK.CFG_VIDEO_COMPRESSION;
import com.company.NetSDK.FinalVar;
import com.company.NetSDK.INetSDK;
import com.company.NetSDK.NET_IN_ENCODE_CFG_CAPS;
import com.company.NetSDK.NET_OUT_ENCODE_CFG_CAPS;
import com.company.NetSDK.NET_STREAM_CFG_CAPS;
import com.company.NetSDK.SDKDEV_DSP_ENCODECAP;
import com.company.NetSDK.SDKDEV_DSP_ENCODECAP_EX;
import com.company.NetSDK.SDKDEV_SYSTEM_ATTR_CFG;
import com.company.NetSDK.SDK_EXTPTZ_ControlType;
import com.company.NetSDK.SDK_PTZ_ControlType;
import com.company.NetSDK.SDK_RESOLUTION_INFO;
import com.company.NetSDK.SDK_RealPlayType;
import com.company.NetSDK.SNAP_PARAMS;
import com.company.PlaySDK.IPlaySDK;
import com.example.kxapp.R;
//import com.example.module.LivePreviewModule;
//import com.example.module.LivePreviewModule.EncodeTask;
//import com.example.module.LivePreviewModule.TestfSnapRev;
import com.example.service.NetSDKLib;
import com.example.service.ToolKits;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
@Name:播放页面 *
@Description:* 
@author wuzhuoyu * 
@Version:V1.00* 
@Create Date:2018-07-26 *
*/

public class LivePreviewActivity extends Activity implements
        SurfaceHolder.Callback,
        AdapterView.OnItemSelectedListener,
        View.OnClickListener{
    private final String TAG = LivePreviewActivity.class.getSimpleName();
    Spinner mSelectStream;
    Spinner mSelectChannel ;
    Spinner mEncodeMode;
    Spinner mEncodeResolve;
    Spinner mEncodeFps;
    Spinner mEncodeBitRate;
    SurfaceView mRealView;
    EditText mEditText;
    View mPtzControlLayoutView;
    LivePreviewModule mLiveModule;
    AlertDialog.Builder builder;

    private boolean isRecord = false;
    private int count = 0;
  ///touch time.
    ///触摸时间.
    long mTouchStartTime = 0;
    long mTouchMoveTime = 0;

    ///single touch.
    ///单点触摸.
    float mSingleTouchStart_x = 0;
    float mSingleTouchStart_y = 0;
    float mSingleTouchEnd_x = 0;
    float mSingleTouchEnd_y = 0;

    ///double touch.
    ///两点触摸.
    float mDoubleTouchStart_x1 = 0;
    float mDoubleTouchStart_y1 = 0;
    float mDoubleTouchStart_x2 = 0;
    float mDoubleTouchStart_y2 = 0;
    float mDoubleTouchEnd_x1 = 0;
    float mDoubleTouchEnd_y1 = 0;
    float mDoubleTouchEnd_x2 = 0;
    
    float mDoubleTouchEnd_y2 = 0;
    Button  mEncodeBtn = null;
	private String port;
	private String iP;
	private String user;
	private String password;
	private String mDeviceInfo;
	private String mLoginHandle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_preview);
        
        Intent intent = getIntent();
        iP = intent.getStringExtra("IP");
        port = intent.getStringExtra("Port");
        user = intent.getStringExtra("User");
        password = intent.getStringExtra("Psw");
        mDeviceInfo = intent.getStringExtra("DeviceINFO");
        mLoginHandle = intent.getStringExtra("LoginHandle");
        Log.d("DebugTest", "---传值取到的信息-----"+mLoginHandle+mDeviceInfo+iP);
        
        mLiveModule = new LivePreviewModule(this);
//        setTitle(R.string.activity_function_list_live_preview);

        builder = new AlertDialog.Builder(this);
//        builder.setMessage(R.string.ptz_control_fragment_info);
        builder.setPositiveButton(R.string.success, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setupView();
        
    }

    private void setupView(){
        mSelectStream = (Spinner)findViewById(R.id.select_stream_type);
        mSelectChannel = (Spinner)findViewById(R.id.select_channel);
        mRealView = (SurfaceView)findViewById(R.id.real_view);
        mRealView.getHolder().addCallback(this);
        initializeSpinner(mSelectChannel,(ArrayList)mLiveModule.getChannelList()).setSelection(0);
        initializeSpinner(mSelectStream,(ArrayList)mLiveModule.getStreamTypeList(mSelectChannel.getSelectedItemPosition())).setSelection(1);
        ((Button)findViewById(R.id.preview_ptz_control)).setOnClickListener(this);
        mPtzControlLayoutView = (View)findViewById(R.id.ptz_control);
        mEditText = (EditText)mPtzControlLayoutView.findViewById(R.id.edittext_preset);
        ///Only limit to use number		
        ///只允许输入数字
        KeyListener keyListener = new DigitsKeyListener(false, false);
        mEditText.setKeyListener(keyListener);

        ((Button) mPtzControlLayoutView.findViewById(R.id.preview_focus_add)).setOnClickListener(this);
        ((Button) mPtzControlLayoutView.findViewById(R.id.preview_focus_dec)).setOnClickListener(this);
        ((Button) mPtzControlLayoutView.findViewById(R.id.preview_aperture_add)).setOnClickListener(this);
        ((Button) mPtzControlLayoutView.findViewById(R.id.preview_aperture_dec)).setOnClickListener(this);
        ((Button) mPtzControlLayoutView.findViewById(R.id.preview_setpreset)).setOnClickListener(this);
        ((Button) mPtzControlLayoutView.findViewById(R.id.preview_clearpreset)).setOnClickListener(this);
        ((Button) mPtzControlLayoutView.findViewById(R.id.preview_gotopreset)).setOnClickListener(this);

        ((Button)findViewById(R.id.preview_remote_snapPic)).setOnClickListener(this);
        mEncodeBtn = ((Button)findViewById(R.id.preview_encode));
        mEncodeBtn.setOnClickListener(this);
        ((Button)findViewById(R.id.preview_record)).setOnClickListener(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mLiveModule.initSurfaceView(mRealView);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    @Override
    protected void onDestroy(){
        mLiveModule.stopRealPlay();
        mLiveModule = null;
        mRealView = null;
        super.onDestroy();
    }
	
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int parentID = parent.getId();
        boolean isMain = isMainStream();

        ///Close the listener event when not triggered.
        ///未触发时关闭侦听器事件。
        if((count == 0)&&(position == 0)) {
            count ++;
            return;
        }

        switch (parentID){
            case R.id.select_channel:
                onChannelChanged(position);
                break;
            case R.id.select_stream_type:
                onStreamTypeChanged(position);
                break;
            case R.id.compress_fromat_spinner:
                onUpdateMode(((TextView)view).getText().toString(),isMain);
                break;
            case R.id.resolve_spinner:
                onUpdateResolve(((TextView)view).getText().toString(),isMain);
                break;
            case R.id.frame_rate_spinner:
                onUpdateFps(position,isMain);
                break;
            case R.id.bit_rate_spinner:
                onUpdateBitRate(((TextView)view).getText().toString(),isMain);
                break;
            default:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void onChannelChanged(int pos){
        if (mLiveModule == null)
            return;
        mLiveModule.stopRealPlay();
        mLiveModule.startPlay(pos,mSelectStream.getSelectedItemPosition(),mRealView);
    }

    private void onStreamTypeChanged(int position){
        if (mLiveModule == null)
            return;
        mLiveModule.stopRealPlay();
        mLiveModule.startPlay(mSelectChannel.getSelectedItemPosition(),position,mRealView);
    }
//云台控制
    @Override
    public void onClick(View v) {
        String text = mEditText.getText().toString();
        switch (v.getId()){
            case R.id.preview_ptz_control:
                if(mPtzControlLayoutView.isShown()) {
                    mPtzControlLayoutView.setVisibility(View.GONE);
                } else  {
                    mPtzControlLayoutView.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.preview_remote_snapPic:
                if(mPtzControlLayoutView.isShown()) {
                    mPtzControlLayoutView.setVisibility(View.VISIBLE);
                }
                mLiveModule.snap(mSelectChannel.getSelectedItemPosition());
                break;
            case R.id.preview_encode:
                if(mPtzControlLayoutView.isShown()) {
                    mPtzControlLayoutView.setVisibility(View.GONE);
                }
                mEncodeBtn.setEnabled(false);
                onEncode();
                break;
            case R.id.preview_record:
                if(mPtzControlLayoutView.isShown()) {
                    mPtzControlLayoutView.setVisibility(View.GONE);
                }
                isRecord = !isRecord;
//               onRecord(v, isRecord);
                break;
            case R.id.preview_focus_add:
                mLiveModule.ptzControlEx(mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_FOCUS_ADD_CONTROL,(byte)8);
                break;
            case R.id.preview_focus_dec:
                mLiveModule.ptzControlEx(mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_FOCUS_DEC_CONTROL,(byte)8);
                break;
            case R.id.preview_aperture_add:
                mLiveModule.ptzControlEx(mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_APERTURE_ADD_CONTROL, (byte)8);
                break;
            case R.id.preview_aperture_dec:
                mLiveModule.ptzControlEx(mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_APERTURE_DEC_CONTROL, (byte)8);
                break;
            case R.id.preview_setpreset:
                if(!text.equals("")) {
                    mLiveModule.ptzControlEx(mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_POINT_SET_CONTROL,
                            (byte)Integer.parseInt(text)) ;
                }else {
//                    ToolKits.showMessage(LivePreviewActivity.this, getString(R.string.input_number));
                }
                break;
            case R.id.preview_clearpreset:
                if(!text.equals("")) {
                    mLiveModule.ptzControlEx(mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_POINT_DEL_CONTROL,
                            (byte)Integer.parseInt(text));
                }else {
//                    ToolKits.showMessage(LivePreviewActivity.this, getString(R.string.input_number));
                }
                break;
            case R.id.preview_gotopreset:
                if(!text.equals("")) {
                    mLiveModule.ptzControlEx(mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_POINT_MOVE_CONTROL,
                            (byte)Integer.parseInt(text));
                } else {
//                    ToolKits.showMessage(LivePreviewActivity.this, getString(R.string.input_number));
                }
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int mAction = event.getAction();
        int mPointorCount = event.getPointerCount();

        switch(mAction) {
            case MotionEvent.ACTION_DOWN :
                if(mPtzControlLayoutView.isShown()) {
                    mPtzControlLayoutView.setVisibility(View.GONE);
                }

                ///If the input method has already been shown on the window, it is hidden.
                ///如果输入方法已经显示在窗口中，那么它将被隐藏。
                InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(mRealView.getWindowToken(), 0);

                mTouchStartTime = System.currentTimeMillis();
                mSingleTouchStart_x = event.getX();
                mSingleTouchStart_y = event.getY();
                break;
            case MotionEvent.ACTION_MOVE :
                mTouchMoveTime = System.currentTimeMillis() - mTouchStartTime;
                int mHistorySize = event.getHistorySize();
                if(mHistorySize == 0) {
                    return true;
                }

                if((mPointorCount == 1) && (mTouchMoveTime > 300)){
                    mSingleTouchEnd_x = event.getX();
                    mSingleTouchEnd_y = event.getY();

                    float mSingleTouchValue_x = mSingleTouchEnd_x - mSingleTouchStart_x;
                    float mSingleTouchValue_y = mSingleTouchEnd_y - mSingleTouchStart_y;

                    float mDeviation = Math.abs(mSingleTouchValue_y/mSingleTouchValue_x);
                    //这里做的是手势控制球机方向
                    if((mSingleTouchValue_x > 0) && (mDeviation < 0.87)) {
                        return mLiveModule.ptzControl(event, mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_RIGHT_CONTROL, (byte)0, (byte)8);
                    } else if((mSingleTouchValue_x < 0) && (mDeviation < 0.87)) {
                        return mLiveModule.ptzControl(event, mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_LEFT_CONTROL, (byte)0, (byte)8);
                    } else if((mSingleTouchValue_y > 0) && (mDeviation > 11.43)) {
                        return mLiveModule.ptzControl(event, mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_DOWN_CONTROL, (byte)0, (byte)8);
                    } else if((mSingleTouchValue_y < 0) && (mDeviation > 11.43)) {
                        return mLiveModule.ptzControl(event, mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_UP_CONTROL, (byte)0, (byte)8);
                    } else if((mSingleTouchValue_x < 0) && (mSingleTouchValue_y < 0) && (mDeviation <= 11.43) && (mDeviation >= 0.87)) {
                        return mLiveModule.ptzControl(event, mSelectChannel.getSelectedItemPosition(), SDK_EXTPTZ_ControlType.SDK_EXTPTZ_LEFTTOP, (byte)8, (byte)8);
                    } else if((mSingleTouchValue_x < 0) && (mSingleTouchValue_y > 0) && (mDeviation <= 11.43) && (mDeviation >= 0.87)) {
                        return mLiveModule.ptzControl(event, mSelectChannel.getSelectedItemPosition(), SDK_EXTPTZ_ControlType.SDK_EXTPTZ_LEFTDOWN, (byte)8, (byte)8);
                    } else if((mSingleTouchValue_x > 0) && (mSingleTouchValue_y < 0) && (mDeviation <= 11.43) && (mDeviation >= 0.87)) {
                        return mLiveModule.ptzControl(event, mSelectChannel.getSelectedItemPosition(), SDK_EXTPTZ_ControlType.SDK_EXTPTZ_RIGHTTOP, (byte)8, (byte)8);
                    } else if((mSingleTouchValue_x > 0) && (mSingleTouchValue_y > 0) && (mDeviation <= 11.43) && (mDeviation >= 0.87)) {
                        return mLiveModule.ptzControl(event, mSelectChannel.getSelectedItemPosition(), SDK_EXTPTZ_ControlType.SDK_EXTPTZ_RIGHTDOWN, (byte)8, (byte)8);
                    }

                } else if((mPointorCount == 2) && (mTouchMoveTime > 300)){
                    mDoubleTouchStart_x1 = event.getHistoricalX(0, mHistorySize - 1);
                    mDoubleTouchStart_y1 = event.getHistoricalY(0, mHistorySize - 1);
                    mDoubleTouchStart_x2 = event.getHistoricalX(1, mHistorySize - 1);
                    mDoubleTouchStart_y2 = event.getHistoricalY(1, mHistorySize - 1);

                    mDoubleTouchEnd_x1 = event.getX(0);
                    mDoubleTouchEnd_y1 = event.getY(0);
                    mDoubleTouchEnd_x2 = event.getX(1);
                    mDoubleTouchEnd_y2 = event.getY(1);

                    float mStartDistance_x = mDoubleTouchStart_x2 - mDoubleTouchStart_x1;
                    float mStartDistance_y = mDoubleTouchStart_y2 - mDoubleTouchStart_y1;
                    float mEndDistance_x = mDoubleTouchEnd_x2 - mDoubleTouchEnd_x1;
                    float mEndDistance_y = mDoubleTouchEnd_y2 - mDoubleTouchEnd_y1;

                    float mStartTouchDistance = (float)Math.sqrt(mStartDistance_x * mStartDistance_x + mStartDistance_y * mStartDistance_y);
                    float mEndTouchDistance = (float)Math.sqrt(mEndDistance_x * mEndDistance_x + mEndDistance_y * mEndDistance_y);

                    if(mEndTouchDistance > mStartTouchDistance) {
                        return mLiveModule.ptzControl(event, mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_ZOOM_ADD_CONTROL, (byte)0, (byte)8);
                    } else if(mEndTouchDistance < mStartTouchDistance) {
                        return mLiveModule.ptzControl(event, mSelectChannel.getSelectedItemPosition(), SDK_PTZ_ControlType.SDK_PTZ_ZOOM_DEC_CONTROL, (byte)0, (byte)8);
                    } else {
                        return false;
                    }
                }

                break;
            case MotionEvent.ACTION_UP :
                break;
            default :
                break;
        }
        return false;
    }

    private void onEncode(){
        final Dialog dialog = new Dialog(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        dialog.setContentView(inflater.inflate(R.layout.encode_config_dialog,null));
        mEncodeMode = ((Spinner)dialog.findViewById(R.id.compress_fromat_spinner));
        mEncodeResolve =((Spinner)dialog.findViewById(R.id.resolve_spinner)) ;
        mEncodeFps = ((Spinner)dialog.findViewById(R.id.frame_rate_spinner));
        mEncodeBitRate = ((Spinner)dialog.findViewById(R.id.bit_rate_spinner));
//        mLiveModule.setSpinnerDataCallBack(new LivePreviewModule.SpinnerDataCallback() {
//            @Override
//            public void onSetSpinner(Bundle data, DialogProgress dhdialog) {
//                if (data == null)
//                    return;
//                initializeSpinner(mEncodeMode,data.getStringArrayList(mLiveModule.MODE)).
//                        setSelection(data.getInt(mLiveModule.MODE_POS),true);
//                initializeSpinner(mEncodeResolve,data.getStringArrayList(mLiveModule.RESOLUTION))
//                        .setSelection(data.getInt(mLiveModule.RESOLUTION_POS),true);
//                initializeSpinner(mEncodeFps,data.getStringArrayList(mLiveModule.FPS)).
//                        setSelection(data.getInt(mLiveModule.FPS_POS),true);
//                initializeSpinner(mEncodeBitRate,data.getStringArrayList(mLiveModule.BITRATE)).
//                        setSelection(data.getInt(mLiveModule.BITRATE_POS),true);
//                dialog.show();
//                if (dhdialog != null && dhdialog.isShowing())
//                    dhdialog.dismiss();
//            }
//        });

        // 得到编码设备
        mLiveModule.getEncodeData(mSelectChannel.getSelectedItemPosition(), isMainStream());
        ((Button)dialog.findViewById(R.id.encode_setting_config_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(mLiveModule.setEncodeConfig(mSelectChannel.getSelectedItemPosition())){
//                    ToolKits.showMessage(LivePreviewActivity.this,getString(R.string.encode_set_success));
                }else {
//                    ToolKits.showMessage(LivePreviewActivity.this,getString(R.string.encode_set_failed));
                }
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mEncodeBtn.setEnabled(true);
            }
        });
    }

    private void onUpdateMode(String text,boolean isMainStream){
        mLiveModule.updateMode(mSelectChannel.getSelectedItemPosition(),text,isMainStream);
        ((ArrayAdapter)mEncodeResolve.getAdapter()).notifyDataSetChanged();
        ((ArrayAdapter)mEncodeFps.getAdapter()).notifyDataSetChanged();
        ((ArrayAdapter)mEncodeBitRate.getAdapter()).notifyDataSetChanged();
    }
    private void onUpdateResolve(String text,boolean isMainStream){
        mLiveModule.updateResolve(mSelectChannel.getSelectedItemPosition(),text,isMainStream);
        ((ArrayAdapter)mEncodeFps.getAdapter()).notifyDataSetChanged();
        ((ArrayAdapter)mEncodeBitRate.getAdapter()).notifyDataSetChanged();
    }
    private void onUpdateFps(int pos,boolean isMainStream){
        mLiveModule.updateFps(mSelectChannel.getSelectedItemPosition(),pos,isMainStream);
        ((ArrayAdapter)mEncodeBitRate.getAdapter()).notifyDataSetChanged();
    }
    private void onUpdateBitRate(String value,boolean isMainStream){
        mLiveModule.updateBitRate(value,isMainStream);
    }

    private Spinner initializeSpinner(final Spinner spinner, ArrayList array){
        spinner.setSelection(0,true);
        spinner.setOnItemSelectedListener(this);
        spinner.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,array));
        return spinner;
    }

//    private void onRecord(View v, boolean recordFlag){
//        if( mLiveModule.record(recordFlag)){
//            if(recordFlag){
//                ((Button)v).setText(R.string.stop_record);
//            }else {
//                ((Button)v).setText(R.string.start_record);
//            }
//        }
//    }

    private boolean isMainStream(){
       return mSelectStream.getSelectedItemPosition() == 0 ? true : false;
    }

}

 class LivePreviewModule {
	
	
    private static final String TAG = LivePreviewModule.class.getSimpleName();
    private final int STREAM_BUF_SIZE = 1024*1024*2;
    private final int RAW_AUDIO_VIDEO_MIX_DATA = 0; ///原始音视频混合数据;  ///Raw audio and video mixing data.
    long mRealHandle = 0;
    Context mContext;
    Resources res;
    int mPlayPort = 0;
    int mCurVolume = -1;
    NetSDKApplication sdkApp;
    boolean isRecording = false;
    Map<Integer,Integer> streamTypeMap = new HashMap<Integer,Integer>();

    /// for preview date callback
    private CB_fRealDataCallBackEx mRealDataCallBackEx;

    CFG_ENCODE_INFO mEncInfo = new CFG_ENCODE_INFO();
    NET_OUT_ENCODE_CFG_CAPS mEncodeCfgCaps = new NET_OUT_ENCODE_CFG_CAPS();
    boolean bF6 = false;
    private final short[][][] s_resolution = new short[][][]{
        // pal
        {{704,	576},
            {352,	576},
            {704,	288},
            {352,	288},
            {176,	144},
            {640,	480},
            {320,	240},
            {480,	480},
            {160,	128},
            {800,	592},
            {1024,	768},
            {1280,	800},
            {1280,	1024},
            {1600,	1024},
            {1600,	1200},
            {1900,	1200},
            {240,	192},
            {1280,	720},
            {1920,	1080},
            {1280,	960},
            {1872,	1408},
            {3744,	1408},
            {2048,	1536},
            {2432,	2050},
            {1216,	1024},
            {1408,	1024},
            {3296,	2472},
            {2560,	1920},
            {960,	576},
            {60,   720},
            {640,   360},
            {320,   180},
            {160,   90}},

        // ntsc
        {{704,	480},
            {352,	480},
            {704,	240},
            {352,	240},
            {176,	120},
            {640,	480},
            {320,	240},
            {480,	480},
            {160,	128},
            {800,	592},
            {1024,	768},
            {1280,	800},
            {1280,	1024},
            {1600,	1024},
            {1600,	1200},
            {1900,	1200},
            {240,	192},
            {1280,	720},
            {1920,	1080},
            {1280,	960},
            {1872,	1408},
            {3744,	1408},
            {2048,	1536},
            {2432,	2050},
            {1216,	1024},
            {1408,	1024},
            {296,	2472},
            {2560,	1920},
            {960,	480},
            {60,   720},
            {640,   360},
            {320,   180},
            {160,   90}}
    };

    public LivePreviewModule(Context context){
    	
    	Log.d("DebugTest", "----LivePreviewMODULE页面1---");
        this.mContext = context;
        res = mContext.getResources();
        mPlayPort = IPlaySDK.PLAYGetFreePort();
        sdkApp = ((NetSDKApplication)((Activity)mContext).getApplication());
        Log.d("DebugTest", "----LivePreviewMODULE页面2---");
        initMap();
        Log.d("DebugTest", "----LivePreviewMODULE页面3---");
        initLinkedList();
        Log.d("DebugTest", "----LivePreviewMODULE页面4---");
    }
    ///码流类型的hash
    private void initMap(){
        streamTypeMap.put(0,SDK_RealPlayType.SDK_RType_Realplay_0);
        streamTypeMap.put(1,SDK_RealPlayType.SDK_RType_Realplay_1);
        Log.d("DebugTest", "----LivePreviewMODULE页面InitMap---");
    }
    ///视频预览前设置
    public boolean prePlay(int channel,int streamType,SurfaceView sv){//获取通道号、码流类型、
    	Log.d("DebugTest", "视频预览是否取到：--"+channel);
        mRealHandle = INetSDK.RealPlayEx(sdkApp.getLoginHandle(),channel,streamType);
        if (mRealHandle == 0){
        	Log.d("DebugTest", "----LivePreviewMODULE页面预览---");
            return false;
        }
        boolean isOpened = IPlaySDK.PLAYOpenStream(mPlayPort,null,0,STREAM_BUF_SIZE) == 0 ? false:true;
        if(!isOpened) {
            Log.d(TAG,"OpenStream Failed");
            return false;
        }
        boolean isPlayin = IPlaySDK.PLAYPlay(mPlayPort,sv) == 0 ? false : true;
        if (!isPlayin) {
            Log.d(TAG,"PLAYPlay Failed");
            IPlaySDK.PLAYCloseStream(mPlayPort);
            return false;
        }
        boolean isSuccess = IPlaySDK.PLAYPlaySoundShare(mPlayPort) == 0 ? false : true;
        if (!isSuccess) {
            Log.d(TAG,"SoundShare Failed");
            IPlaySDK.PLAYStop(mPlayPort);
            IPlaySDK.PLAYCloseStream(mPlayPort);
            return false;
        }
        if (-1 == mCurVolume) {
            mCurVolume = IPlaySDK.PLAYGetVolume(mPlayPort);
        } else {
            IPlaySDK.PLAYSetVolume(mPlayPort, mCurVolume);
        }
        return true;
    }
    public boolean getHandle(){
        if (this.mRealHandle == 0)
            return false;
        else
            return true;
    }
    ///开始预览视频
    public void startPlay(int channel,int streamType,final SurfaceView view){
        Log.d(TAG,"StreamTpye: "+streamTypeMap.get(streamType));
        if (!prePlay(channel, streamTypeMap.get(streamType), view)) {
//            ToolKits.showMessage(mContext, res.getString(R.string.live_preview_failed));
            Log.d(TAG,"prePlay returned false..");
            return;
        }
        if (mRealHandle!=0){
            mRealDataCallBackEx = new CB_fRealDataCallBackEx() {
                @Override
                public void invoke(long rHandle, int dataType, byte[] buffer, int bufSize, int param) {
                    Log.v(TAG,"dataType:"+dataType+"; bufSize:"+bufSize+"; param:"+param);
                    if (RAW_AUDIO_VIDEO_MIX_DATA == dataType){
                        Log.i(TAG,"dataType == 0");
                        IPlaySDK.PLAYInputData(mPlayPort,buffer,buffer.length);
                    }
                }
            };
            INetSDK.SetRealDataCallBackEx(mRealHandle, mRealDataCallBackEx, 1);
        }
    }

    ///停止预览视频
    public void stopRealPlay(){
        try{
            IPlaySDK.PLAYStop(mPlayPort);
            IPlaySDK.PLAYStopSoundShare(mPlayPort);
            IPlaySDK.PLAYCloseStream(mPlayPort);
            INetSDK.StopRealPlayEx(mRealHandle);
            if (isRecording)
                INetSDK.StopSaveRealData(mRealHandle);
        }catch (Exception e){
            e.printStackTrace();
        }
        mRealHandle = 0;
        isRecording = false;
    }
    ///初始化视频窗口
    public void initSurfaceView(final SurfaceView sv){
        if (sv == null)
            return;
        IPlaySDK.InitSurface(mPlayPort,sv);
    }
    ///
    public int getTypeMask(int channel){
        int streamMask = 0;
        SDKDEV_DSP_ENCODECAP_EX stEncodeCapOld = new SDKDEV_DSP_ENCODECAP_EX();
        CFG_DSPENCODECAP_INFO stEncodeCapNew = new CFG_DSPENCODECAP_INFO();
        if (INetSDK.QueryDevState(sdkApp.getLoginHandle(), FinalVar.SDK_DEVSTATE_DSP_EX, stEncodeCapOld, NetSDKLib.TIMEOUT_10S)) {
            streamMask = stEncodeCapOld.dwStreamCap;
        } else if (ToolKits.GetDevConfig(FinalVar.CFG_CMD_HDVR_DSP, stEncodeCapNew, sdkApp.getLoginHandle(), channel, ENCODE_BUFFER_SIZE*7)) {
            streamMask = stEncodeCapNew.dwStreamCap;
        }
        return streamMask;
    }
    ///获取通道数量
    public int getChannel(){
        if (sdkApp == null)
            return 0;
        return sdkApp.getDeviceInfo().nChanNum;
    }
    ///获取要显示的通道号
    public List getChannelList(){
        ArrayList<String> channelList = new ArrayList<String>();
        for (int i=0;i<getChannel();i++){
            channelList.add(res.getString(R.string.channel)+(i+1));
        }
        return channelList;
    }
    ///获取要显示的码流类型
    public List getStreamTypeList(int channel){
        ArrayList<String> list = new ArrayList<String>();
        int stream = getTypeMask(channel);
        String[] streamNames = res.getStringArray(R.array.stream_type_array);
        
        for (int i=0;i<2;i++){
            if ((stream & (0x01 << 1))!=0)
                list.add(streamNames[i]);
        }
        return list;
    }

    ///PTZ Direction、Zoom
    ///云台方向、变倍
    public boolean ptzControl(MotionEvent event , int nChn , int nControl , byte param1 , byte param2) {
        boolean bPtzControl = INetSDK.SDKPTZControl(sdkApp.getLoginHandle(), nChn , nControl ,
                param1 ,param2 ,(byte) 0 , false);
        if(bPtzControl) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            INetSDK.SDKPTZControl(sdkApp.getLoginHandle(), nChn , nControl ,
                    param1 ,param2 ,(byte) 0 , true);
        } else {
            ToolKits.writeErrorLog("PTZControl Failed...");
            return false;
        }
        return true;
    }

    ///Focus、Aperture、Preset
    ///变焦、光圈、预置点
    public boolean ptzControlEx( int nChn , int nControl , byte param1) {
        boolean bPtzControl = INetSDK.SDKPTZControl(sdkApp.getLoginHandle(), nChn , nControl ,
                (byte)0, param1,(byte)0 , false);
        if(bPtzControl) {
            INetSDK.SDKPTZControl(sdkApp.getLoginHandle(), nChn , nControl ,
                    (byte)0, param1,(byte)0 , true);
            ToolKits.showMessage(mContext , res.getString(R.string.info_success));
        } else {
            ToolKits.showMessage(mContext , res.getString(R.string.info_failed));
            return false;
        }
        return true;
    }
	
	///Remote Snap
    ///远程抓图
    public void snap(int channel) {
        ///Set snap callback
        ///设置抓图回调
        TestfSnapRev stCb = new TestfSnapRev();
        INetSDK.SetSnapRevCallBack(stCb);

        ///Send snap request
        ///发送抓图请求
        SNAP_PARAMS stSnapParam = new SNAP_PARAMS();
        stSnapParam.Channel = channel;
        stSnapParam.Quality = 3;
        stSnapParam.ImageSize = 1;
        stSnapParam.mode = 0;
        stSnapParam.InterSnap = 5;
        stSnapParam.CmdSerial = 100;
        if (INetSDK.SnapPictureEx(sdkApp.getLoginHandle(), stSnapParam)) {
            ToolKits.showMessage(mContext, res.getString(R.string.info_success));
        } else {
            ToolKits.showMessage(mContext, res.getString(R.string.info_failed));
            return;
        }
    }

    ///Snap callback
    ///抓图回调
    public class TestfSnapRev implements CB_fSnapRev {
        @Override
        public void invoke(long lLoginID, byte pBuf[], int RevLen, int EncodeType, int CmdSerial)
        {
            String strFileName = "";
            if (10 == EncodeType) {
                strFileName = createInnerAppFile("jpg");
            } else if (0 == EncodeType) {
                strFileName = createInnerAppFile("mpeg4");
            }

            ToolKits.writeErrorLog("FileName:"+strFileName);
            if (strFileName.equals(""))
                return;

            FileOutputStream fileStream = null;

            try {
                fileStream = new FileOutputStream(strFileName, true);
                ToolKits.writeErrorLog("fileStream");
                fileStream.write(pBuf, 0, RevLen);
                fileStream.flush();
            } catch(FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if(null != fileStream) {
                        fileStream.close();
                    }
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	private int mVideoStandard;  //0:PAL  ,1:NTSC
    int mModeMask = 0;
    int mResolveMask = 0;
    LinkedList<String> strMode = new LinkedList<String>() ;
    LinkedList<String> strModeProfile = new LinkedList<String>();
    LinkedList<String> strBitrate = new LinkedList<String>();
    final ArrayList<String> mode_data_list = new ArrayList<String>();
    final ArrayList<String> resolve_data_list = new ArrayList<String>();
    final ArrayList<String> fps_data_list = new ArrayList<String>();
    final ArrayList<String> bitrate_data_list = new ArrayList<String>();

    private void initLinkedList(){
        String[] mode = res.getStringArray(R.array.encode_mode);
        int modeCount = mode.length;
        String[] profile = res.getStringArray(R.array.encode_mode_profile);
        int profileCount = profile.length;
        String[] bitrate = res.getStringArray(R.array.encode_bit_rate_item);
        int bitCount = bitrate.length;
        Log.d("DebugTest", "----LivePreviewMODULE页面initLinkedList---");
        int max = modeCount;
        if (max<profileCount) {
            max = profileCount;
        }
        else if (max<bitCount) {
            max = bitCount;
        }

        for (int i = 0; i < max; i++){
            if (i<modeCount)
                strMode.add(mode[i]);
            if (i<profileCount)
                strModeProfile.add(profile[i]);
            if (i<bitCount)
                strBitrate.add(bitrate[i]);
        }
    }

    ///获取编码配置信息
    private boolean initEncodeData(final int channel, boolean isMainStream){
        SDKDEV_SYSTEM_ATTR_CFG[] sysAttrs = new SDKDEV_SYSTEM_ATTR_CFG[1];
        sysAttrs[0] = new SDKDEV_SYSTEM_ATTR_CFG();
        Log.d("DebugTest", "----LivePreviewMODULE页面initEncodeData---");
        // Get video standard : 0 - PAL, 1 - NTSC
        if (INetSDK.GetDevConfig(sdkApp.getLoginHandle(), FinalVar.SDK_DEV_DEVICECFG, channel, sysAttrs, null, NetSDKLib.TIMEOUT_5S)){
            mVideoStandard = sysAttrs[0].byVideoStandard;
        }else {
            ToolKits.writeErrorLog("GetDevConfig for SDK_DEV_DEVICECFG falied ");
            return false;
        }

        // Get encode
        if (!ToolKits.GetDevConfig(FinalVar.CFG_CMD_ENCODE, mEncInfo, sdkApp.getLoginHandle(), channel, ENCODE_BUFFER_SIZE)){
            ToolKits.writeErrorLog("GetDevConfig for CFG_CMD_ENCODE falied in initEncodeData..");
            return false;
        }

        bF6 = isSupportF6(channel, mEncInfo, mEncodeCfgCaps, getStream(isMainStream));
        if (!bF6){
            SDKDEV_DSP_ENCODECAP  stEncodeCapOld = new SDKDEV_DSP_ENCODECAP();
            CFG_DSPENCODECAP_INFO stEncodeCapNew = new CFG_DSPENCODECAP_INFO();
            if ((ToolKits.GetDevConfig(FinalVar.CFG_CMD_HDVR_DSP, stEncodeCapNew, sdkApp.getLoginHandle(), channel,ENCODE_BUFFER_SIZE))
                    &&(0!=stEncodeCapNew.dwEncodeModeMask)
                    &&(0!=stEncodeCapNew.dwImageSizeMask)){
                mModeMask = stEncodeCapNew.dwEncodeModeMask;
                mResolveMask = stEncodeCapNew.dwImageSizeMask;
            }else if ((INetSDK.QueryDevState(sdkApp.getLoginHandle(),FinalVar.SDK_DEVSTATE_DSP,stEncodeCapOld,NetSDKLib.TIMEOUT_10S))
                    &&(0!=stEncodeCapOld.dwEncodeModeMask)
                    &&(0!=stEncodeCapOld.dwImageSizeMask)){
                mModeMask = stEncodeCapOld.dwEncodeModeMask;
                mResolveMask = stEncodeCapOld.dwImageSizeMask;
            }
        }else {
            mModeMask = isMainStream ? mEncodeCfgCaps.stuMainFormatCaps[0].dwEncodeModeMask
                    : mEncodeCfgCaps.stuExtraFormatCaps[0].dwEncodeModeMask;
            Log.d(TAG, "Support F6 Configuration. mModeMask " + mModeMask);
        }
        return true;
    }

    public void getEncodeData(int channel, boolean mainStream){
        final EncodeTask task = new EncodeTask();
        task.execute(new Integer(channel), new Boolean(mainStream));
    }

    // Get encode cfg caps
    private boolean isSupportF6(final int channel,final CFG_ENCODE_INFO encode_info,final NET_OUT_ENCODE_CFG_CAPS encode_cfg_caps,int streamTpye){
        bF6 = true;
        char[] cJson = new char[ENCODE_BUFFER_SIZE];
        if (!INetSDK.PacketData(FinalVar.CFG_CMD_ENCODE, encode_info, cJson, ENCODE_BUFFER_SIZE)) {
            bF6 = false;
            return false;
        }

        NET_IN_ENCODE_CFG_CAPS in = new NET_IN_ENCODE_CFG_CAPS();
        in.nChannelId = channel;
        in.nStreamType = streamTpye;
        in.pchEncodeJson = getBytes(cJson);

        if (!INetSDK.GetDevCaps(sdkApp.getLoginHandle(),FinalVar.NET_ENCODE_CFG_CAPS, in, encode_cfg_caps, NetSDKLib.TIMEOUT_10S)){
            bF6 = false;
            return false;
        }
        return true;
    }

    ///char数组转为byte数组
    private byte[] getBytes(char[] chars){
        Charset charset = Charset.forName("UTF-8");
        CharBuffer cb = CharBuffer.allocate(chars.length);
        cb.put(chars);
        cb.flip();
        ByteBuffer bb = charset.encode(cb);
        return bb.array();
    }

    private int buildModeData(Boolean mainstream){
        mode_data_list.clear();
        NET_STREAM_CFG_CAPS[] streamCFG;
        streamCFG = mainstream ? mEncodeCfgCaps.stuMainFormatCaps : mEncodeCfgCaps.stuExtraFormatCaps;

        for (int i = 0; i < strMode.size(); i++){
            if ((mModeMask & (1<<i)) != 0 ){
                if (strMode.get(i).equals("H.264")) {
                    for (int j = 0; j < streamCFG[0].nH264ProfileRankNum; j++) {
                        Log.d(TAG,"H.264 mode:" + strModeProfile.get(streamCFG[0].bH264ProfileRank[j]-1));
                        mode_data_list.add(strModeProfile.get(streamCFG[0].bH264ProfileRank[j] - 1));
                    }
                } else {
                    Log.d(TAG, "mode: "+strMode.get(i));
                    mode_data_list.add(strMode.get(i));
                }
            }
        }

        int selectedPos = 0;
        CFG_VIDEOENC_OPT videoencOpt[] = mainstream ? mEncInfo.stuMainStream : mEncInfo.stuExtraStream;

        for (int i = 0; i < mode_data_list.size(); i++) {
            if (mode_data_list.get(i).equals(strMode.get(videoencOpt[0].stuVideoFormat.emCompression))) {
                selectedPos = i;
                break;
            }
            if (CFG_VIDEO_COMPRESSION.VIDEO_FORMAT_H264 == videoencOpt[0].stuVideoFormat.emCompression) {
                if (videoencOpt[0].stuVideoFormat.abProfile
                        && mode_data_list.get(i).equals(strModeProfile.get(videoencOpt[0].stuVideoFormat.emProfile - 1))) {
                    selectedPos = i;
                    break;
                }
            }
        }
        return selectedPos;
    }
//    private SpinnerDataCallback mCallback = null;
//    public void setSpinnerDataCallBack(SpinnerDataCallback callBack){
//        this.mCallback = callBack;
//    }
//    private SpinnerDataCallback getCallback(){
//        return this.mCallback;
//    }

    private int buildResolveData(boolean ismainStream){
        resolve_data_list.clear();
        CFG_VIDEOENC_OPT[]  streamTpye;
        NET_STREAM_CFG_CAPS[] streamCFG;
        if (ismainStream) {
            streamTpye = mEncInfo.stuMainStream;
            streamCFG = mEncodeCfgCaps.stuMainFormatCaps;
        }else {
            streamTpye = mEncInfo.stuExtraStream;
            streamCFG = mEncodeCfgCaps.stuExtraFormatCaps;
        }
        if(bF6){
            int modeInd  = streamTpye[0].stuVideoFormat.emCompression;
            if (modeInd<=CFG_VIDEO_COMPRESSION.VIDEO_FORMAT_H265){
                if (streamCFG[0].abIndivResolution){
                    for (int i=0;i<streamCFG[0].nIndivResolutionNums[modeInd];i++){
                        resolve_data_list.add(streamCFG[0].stuIndivResolutionTypes[modeInd][i].snWidth
                                +"*"+streamCFG[0].stuIndivResolutionTypes[modeInd][i].snHight);
                    }
                }else {
                    for (int i=0;i<streamCFG[0].nResolutionTypeNum;i++){
                        resolve_data_list.add(streamCFG[0].stuResolutionTypes[i].snWidth
                                +"*"+streamCFG[0].stuResolutionTypes[i].snHight);
                    }
                }
            }
        }else{
            String[] strResolution = res.getStringArray(R.array.encode_resolution_item);
            for (int i=0;i<strResolution.length && i<32;i++){
                if ((mResolveMask & (1<<i))!=0){
                    resolve_data_list.add(strResolution[i]+"("+
                            s_resolution[mVideoStandard][i][0]+
                            "*"+s_resolution[mVideoStandard][i][1]+")");
                }
            }
        }
        SDK_RESOLUTION_INFO infor = new SDK_RESOLUTION_INFO();
        infor.snWidth = (short)streamTpye[0].stuVideoFormat.nWidth;
        infor.snHight = (short)streamTpye[0].stuVideoFormat.nHeight;
        resolvePos= resolutionToIndex(infor,resolve_data_list);
        return resolvePos;
    }


    int resolvePos;
    private int resolutionToIndex(SDK_RESOLUTION_INFO info,ArrayList<String> list){
        for (int i=0;i<list.size();i++){
            String[] value = null;
            String temp = list.get(i);
            Log.i(TAG, "temp:"+temp);
            if (bF6){
                value = temp.split("\\*");
            }else {
                String[] a =temp.split("\\(");
                if (a.length>1) {
                    String[] b = a[1].split("\\)");
                    if (b.length>0) {
                        value = b[0].split("\\*");
                    }
                }
            //    value = temp.split("\\(")[1].split("\\)")[0].split("\\*");
            }
            if (value !=null && value.length > 1) {
                if (((int) info.snWidth == Integer.parseInt(value[0]))
                        && ((int) info.snHight == Integer.parseInt(value[1]))) {
                    return i;
                }
            }
        }
        return -1;
    }

    private SDK_RESOLUTION_INFO indexToResolution(String text){
        String[] value;
        if (bF6){
            value = text.split("\\*");
        }else {
            value = text.split("\\(")[1].split("\\)")[0].split("\\*");
        }
        SDK_RESOLUTION_INFO infor = new SDK_RESOLUTION_INFO();
        infor.snWidth = (short)Integer.parseInt(value[0]);
        infor.snHight = (short)Integer.parseInt(value[1]);
        return infor;
    }

    private int buildFpsData(boolean isMainStream){
        fps_data_list.clear();
        int maxFps = 0;
        CFG_VIDEOENC_OPT[]  streamTpye;
        NET_STREAM_CFG_CAPS[] streamCFG;
        if (isMainStream) {
            streamTpye = mEncInfo.stuMainStream;
            streamCFG = mEncodeCfgCaps.stuMainFormatCaps;
        }else {
            streamTpye = mEncInfo.stuExtraStream;
            streamCFG = mEncodeCfgCaps.stuExtraFormatCaps;
        }
        if (streamCFG[0].nFPSMax!=0){
            maxFps = streamCFG[0].nFPSMax;
        }else {
            SDK_RESOLUTION_INFO infor = new SDK_RESOLUTION_INFO();
            infor.snWidth = (short)streamTpye[0].stuVideoFormat.nWidth;
            infor.snHight = (short)streamTpye[0].stuVideoFormat.nHeight;
            int index = resolutionToIndex(infor,resolve_data_list);
            if (index != -1) {
                maxFps = streamCFG[0].nResolutionFPSMax[index];
            }
        }

        if (0==maxFps){
            for (int i=0;i<25;i++){
                fps_data_list.add(""+(i+1));
            }
        }else {
            for (int i=0;i<maxFps;i++){
                fps_data_list.add(""+(i+1));
            }
        }
        int selectedPos = 0;
        if ((int)streamTpye[0].stuVideoFormat.nFrameRate<=fps_data_list.size()){
            selectedPos = ((int)streamTpye[0].stuVideoFormat.nFrameRate)-1;
        }else {
            if (0!=fps_data_list.size()){
                selectedPos = fps_data_list.size()-1;
                streamTpye[0].stuVideoFormat.nFrameRate = fps_data_list.size();
            }
        }
        fpsSize = fps_data_list.size();
        return selectedPos;
    }
    int fpsSize;

    private int buildBitRateData(boolean isMainStream){
        Integer min = new Integer(0);
        Integer max = new Integer(0);
        CFG_VIDEOENC_OPT[]  streamTpye;
        NET_STREAM_CFG_CAPS[] streamCFG;
        if (isMainStream) {
            streamTpye = mEncInfo.stuMainStream;
            streamCFG = mEncodeCfgCaps.stuMainFormatCaps;
        }else {
            streamTpye = mEncInfo.stuExtraStream;
            streamCFG = mEncodeCfgCaps.stuExtraFormatCaps;
        }
        bitrate_data_list.clear();
        if ((0 == streamCFG[0].nMinBitRateOptions)
                && (0==streamCFG[0].nMaxBitRateOptions)){
            int fps;
            if ((int)streamTpye[0].stuVideoFormat.nFrameRate<=fpsSize){
                fps = (int)streamTpye[0].stuVideoFormat.nFrameRate;
            }else {
                fps = fpsSize;
            }

            int[] nTmp = new int[]{0,0};
            getBitRateScope(fps,2*fps,streamTpye[0].stuVideoFormat.nWidth,
                    streamTpye[0].stuVideoFormat.nHeight,
                    streamTpye[0].stuVideoFormat.emCompression,nTmp);
            min = nTmp[0];
            max = nTmp[1];
        }else {
            min = streamCFG[0].nMinBitRateOptions;
            max = streamCFG[0].nMaxBitRateOptions;
        }

        for (int i=0;i<strBitrate.size();i++){
            if((Integer.parseInt(strBitrate.get(i))>=min.intValue())
                    && (Integer.parseInt(strBitrate.get(i))<=max.intValue())){
                bitrate_data_list.add(strBitrate.get(i));
            }
        }
        for (int i=0;i<bitrate_data_list.size();i++){
            if (bitrate_data_list.get(i).equals(String.valueOf(streamTpye[0].stuVideoFormat.nBitRate))){
                return i;
            }
        }
        if (0!=bitrate_data_list.size()){
            streamTpye[0].stuVideoFormat.nBitRate = Integer.parseInt(bitrate_data_list.get(bitrate_data_list.size()-1));
        }
        return  bitrate_data_list.size()-1;
    }

    private void getBitRateScope(int fps,int iframes,int width,int height,int encode,int[] nTmp){
        int gop = (iframes>149)?50:iframes;
        double scalar = width*height/(352.0*288)/gop;
        double minRaw = 0;
        if (encode == 5){
            minRaw = (gop+IFRAME_PFRAME_QUOTIENT-1)*fps*7*3*scalar;
        }else {
            minRaw = (gop+IFRAME_PFRAME_QUOTIENT-1)*fps*MIN_CIF_PFRAME_SIZE*scalar;
        }
        nTmp[0] = roundToFactor((int)minRaw,(1<<(int)log2(minRaw))/4);
        double maxRaw = (gop+IFRAME_PFRAME_QUOTIENT-1)*fps*MAX_CIF_PFRAME_SIZE*scalar;
        nTmp[1] = roundToFactor((int)maxRaw,(1<<(int)log2(maxRaw))/4);
    }
    private int roundToFactor(int n,int f){
        if (f==0)
            return n;
        return f*round(n/(float)f);
    }
    private double log2(double val){
        return Math.log(val)/Math.log((double)2);
    }
    private int round(float val){
        return (int)(val+0.5);
    }
    final int MIN_CIF_PFRAME_SIZE = 7;
    final int MAX_CIF_PFRAME_SIZE = 40;
    final int IFRAME_PFRAME_QUOTIENT = 3;
    final int ENCODE_BUFFER_SIZE = 1024*10;

    public void updateMode(int channel,final String mode, boolean isMainStream){
        Log.d(TAG,"in Param mode:"+mode);
        CFG_VIDEOENC_OPT[]  streamTpye;
        if (isMainStream) {
            streamTpye = mEncInfo.stuMainStream;
        }else {
            streamTpye = mEncInfo.stuExtraStream;
        }
        if (strMode.contains(mode)){
            Log.d(TAG,"mode:"+mode);
            streamTpye[0].stuVideoFormat.emCompression = strMode.indexOf(mode);
            Log.d(TAG,"emCompression:"+strMode.indexOf(mode));

        }
        if (strModeProfile.contains(mode)){
            Log.d(TAG," profile  mode:"+mode);
            streamTpye[0].stuVideoFormat.emCompression = CFG_VIDEO_COMPRESSION.VIDEO_FORMAT_H264;
            streamTpye[0].stuVideoFormat.abProfile = true;
            streamTpye[0].stuVideoFormat.emProfile = strModeProfile.indexOf(mode)+1;
            Log.d(TAG,"emProfile:"+strModeProfile.indexOf(mode)+1);
        }
    }

    public void updateResolve(int chennel, String value, boolean isMainStream){
        CFG_VIDEOENC_OPT[]  streamTpye;
        if (isMainStream) {
            streamTpye = mEncInfo.stuMainStream;
        }else {
            streamTpye = mEncInfo.stuExtraStream;
        }
        SDK_RESOLUTION_INFO infor = indexToResolution(value);
        streamTpye[0].stuVideoFormat.nWidth = infor.snWidth;
        streamTpye[0].stuVideoFormat.nHeight = infor.snHight;
    }

    public void updateFps(int channel,int pos, boolean isMainStream){
        CFG_VIDEOENC_OPT[]  streamTpye;
        if (isMainStream) {
            streamTpye = mEncInfo.stuMainStream;
        }else {
            streamTpye = mEncInfo.stuExtraStream;
        }
        streamTpye[0].stuVideoFormat.nFrameRate = pos+1;
    }
    public void updateBitRate(String bit , boolean isMainStream){
        CFG_VIDEOENC_OPT[]  streamTpye;
        if (isMainStream) {
            streamTpye = mEncInfo.stuMainStream;
        }else {
            streamTpye = mEncInfo.stuExtraStream;
        }
        streamTpye[0].stuVideoFormat.nBitRate = Integer.parseInt(bit);
    }
    private int getStream(boolean isMain){
        if (isMain)
            return SDK_RealPlayType.SDK_RType_Realplay;
        else
            return SDK_RealPlayType.SDK_RType_Realplay_1;
    }

    public boolean record(boolean recordFlag){
        if (mRealHandle == 0)
            return false;
        ToolKits.writeLog("ExternalFilesDir:"+mContext.getExternalFilesDir(null).getAbsolutePath());
        isRecording = recordFlag;
        if(isRecording){
            String recordFile = createInnerAppFile("dav");
            if (!INetSDK.SaveRealData(mRealHandle, recordFile)){
                ToolKits.writeErrorLog("record file:"+recordFile);
                return false;
            }
        }else {
            INetSDK.StopSaveRealData(mRealHandle);
        }
        return true;
    }

    public boolean setEncodeConfig(int channel){
        return  (ToolKits.SetDevConfig(FinalVar.CFG_CMD_ENCODE,mEncInfo,sdkApp.getLoginHandle(),channel,ENCODE_BUFFER_SIZE));
    }

    private synchronized String createInnerAppFile(String suffix){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = format.format(new Date());
        String file = mContext.getExternalFilesDir(null).getAbsolutePath()+"/"+ time.replace(":","_")+
                "."+suffix;
        return file;
    }

//    public interface SpinnerDataCallback{
//        void onSetSpinner(Bundle data,DialogProgress dialog);
//    }

//    private DialogProgress dialog = null;
    public final String MODE = "mode";
    public final String FPS = "fps";
    public final String BITRATE = "bitrate";
    public final String RESOLUTION = "resolve";
    public final String MODE_POS = "mode_pos";
    public final String FPS_POS = "fps_pos";
    public final String BITRATE_POS = "bitrate_pos";
    public final String RESOLUTION_POS = "resolve_pos";

    /**
     * EncodeTask
     */
    private class EncodeTask extends AsyncTask<Object,Object,Integer[]>{

//        @Override
//        protected void onPreExecute(){
//            super.onPreExecute();
//            if (dialog == null){
//                dialog = new DialogProgress(mContext);
//            }
//            dialog.setMessage(res.getString(R.string.waiting));
//            dialog.setSpinnerType(DialogProgress.FADED_ROUND_SPINNER);
//            dialog.setCancelable(false);
//            dialog.show();
//        }
        @Override
        protected Integer[] doInBackground(Object... params) {
            Integer channel = (Integer)params[0];
            Boolean isMainStream = (Boolean)params[1];

            boolean r = initEncodeData(channel, isMainStream);

            int selectedModePos = buildModeData(isMainStream);
            int selectedFpsPos = buildFpsData(isMainStream);
            int selectedBRPos = buildBitRateData(isMainStream);
            int selectedResPos = buildResolveData(isMainStream);

            Integer[] result = new Integer[5];
            result[0] = selectedModePos;
            result[1] = selectedFpsPos;
            result[2] = selectedBRPos;
            result[3] = selectedResPos;
            result[4] = r ? 1:0;
            return result;
        }
        @Override
        protected void onPostExecute(Integer[] result){
            super.onPostExecute(result);
            if (result[4] == 0){
                Toast.makeText(mContext,mContext.getString(R.string.get_encode_failed),Toast.LENGTH_SHORT).show();
            }
            Bundle bundle = new Bundle();
            bundle.putInt(MODE_POS,result[0]);
            bundle.putInt(FPS_POS,result[1]);
            bundle.putInt(BITRATE_POS, result[2]);
            bundle.putInt(RESOLUTION_POS, result[3]);
            bundle.putStringArrayList(MODE,mode_data_list);
            bundle.putStringArrayList(FPS,fps_data_list);
            bundle.putStringArrayList(BITRATE,bitrate_data_list);
//            bundle.putStringArrayList(RESOLUTION,resolve_data_list);
//            getCallback().onSetSpinner(bundle,dialog);
        }
    }
}

