package com.example.kxapp;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PreferencesService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @Name:阈值报警页面*
 * @Description: 先提取到各个基地的传感器数据（由于气象站和墒情站传感器数据不同，所以气象站是单独开了线程取数据，由于数据传输标志不同的问题所以三个墒情站也是单独开的线程（以后代码可优化））放在HashMap里面。
 *               再取到基地的阈值数据（同样气象站要单独开线程获取，（这里的墒情站都是用的同样的阈值））放在HashMap里面。再将所取的数据按监测点号装到ArrayList里面（使用ArrayList方便后期可添加设备），进行数据的比较
 *               同样也做了一个区分用户的flag，根据不同的flag值，进入到自定义的不同的page方法来组拼参数，开启线程
 *               比较函数由于数据的不同，做了两个比较函数，墒情站的函数做了复用*
 * @author wuzhuoyu *
 * @Version:V1.00 *
 * @Create Date:2013-10-14 *
 */

public class WarnActivity extends Activity {

	private String flag = "";
	private TextView back;
	private boolean nThread = true;
	private TextView tv_YzszWarn1;
	/** 服务器地址 */
	private String IP = "120.79.76.116:8000";
	private PreferencesService preservice;

	/** 数据子线程提交命令 */
	private String cmd = "";
	
	/* 气象站站参数命令部分 */
	
	/** 气象站传感器数据子线程组合参数 */
	private HashMap<String, Object> qxz_data;
	/** 气象站阈值数据子线程提交命令 */
	private String scmd_qxz;
	/** 气象站阈值数据子线程组合参数 */
	private HashMap<String, Object> qxz_scmd;
	// 保存墒情监测站阈值进行数据比较
	private ArrayList<HashMap<String, Object>> thresholdValue_qxz;

	/* 墒情监测站参数命令部分 */
	private HashMap<String, Object> sqjc_data;
	/** 墒情监测站阈值数据子线程提交命令 */
	private String scmd_sqjc;
	/** 墒情监测站阈值数据子线程组合参数 */
	private HashMap<String, Object> sqjc_scmd;
	// 保存墒情监测站阈值进行数据比较
	private ArrayList<HashMap<String, Object>> thresholdValue_sqjc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.warnshow);
//		Intent intent = getIntent();
//		flag = intent.getStringExtra("flag");//用户flag
		flag=Flag.getFlag();
		back = (TextView) findViewById(R.id.tv_back);//返回点击事件
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				nThread = false;
				finish();
			}
		});
		InitView();//页面初始化

		// 获取网络配置
		preservice = new PreferencesService(getApplicationContext());
		Map<String, String> params = preservice.getPreferences();
		String serviceIP = params.get("serviceIP");

		if (serviceIP.equals("")) {
			preservice.save("120.79.76.116:8000", "222.180.45.174", "8083", "admin", "zs123456", "", "");
		} else {
			IP = serviceIP;

		}

	}
//初始化
	private void InitView() {
		tv_YzszWarn1 = (TextView) findViewById(R.id.tv_YzszWarn1);
//判断用户的flag值
		if (flag.equals("1")) {
			page_one();//福德村
		} else if (flag.equals("2")) {
			page_two();//四圣村
		} else if (flag.equals("3")) {
			page_three();//乌杨村
		} else if (flag.equals("4")) {
			page_four();//南雅
		} else if (flag.equals("")) {
			page_five();//赵家
		}
	}
	

//临江镇福德村
	private void page_one() {
		// 组拼气象站传感器数据

		cmd = FinalConstant.QXZFIND_REQUEST_SERVER;
		nThread = true;

		qxz_data = new HashMap<String, Object>();// 组织参数
		qxz_data.put("cmd", cmd);
		new Thread(query_qxz).start();

		// 组拼请求气象站阈值参数
		scmd_qxz = FinalConstant.YZSZ_QXZ_WARN_REQUEST_SERVER;
		qxz_scmd = new HashMap<String, Object>();
		qxz_scmd.put("cmd", scmd_qxz);
		new Thread(querythreshold_qxz).start();
	}
	
//白鹤街道四圣村
	private void page_two() {
		// 组拼四圣传感器数据
		cmd = FinalConstant.SQZ1FIND_REQUEST_SERVER;
		nThread = true;

		sqjc_data = new HashMap<String, Object>();// 组织参数
		sqjc_data.put("cmd", cmd);
		new Thread(query_sqjc1).start();

		// 组拼请求四圣村阈值参数
		scmd_sqjc = FinalConstant.YZSZ_SQJC_WARN_REQUEST_SERVER;
		sqjc_scmd = new HashMap<String, Object>();
		sqjc_scmd.put("cmd", scmd_sqjc);
		new Thread(querythreshold_sqjc).start();
	}
	
//丰乐街道乌杨村
	private void page_three() {
		// 组拼乌杨传感器数据
		// nThread = true;
		cmd = FinalConstant.SQZ2FIND_REQUEST_SERVER;

		sqjc_data = new HashMap<String, Object>();// 组织参数
		sqjc_data.put("cmd", cmd);
		new Thread(query_sqjc2).start();

		// 组拼请求乌杨村阈值参数
		scmd_sqjc = FinalConstant.YZSZ_SQJC_WARN_REQUEST_SERVER;
		sqjc_scmd = new HashMap<String, Object>();
		sqjc_scmd.put("cmd", scmd_sqjc);
		new Thread(querythreshold_sqjc).start();
	}
	
//南雅镇新全村
	private void page_four() {
		// 组拼南雅传感器数据
		// nThread = true;
		cmd = FinalConstant.SQZ3FIND_REQUEST_SERVER;

		sqjc_data = new HashMap<String, Object>();// 组织参数
		sqjc_data.put("cmd", cmd);
		new Thread(query_sqjc3).start();

		// 组拼请求南雅阈值参数
		scmd_sqjc = FinalConstant.YZSZ_SQJC_WARN_REQUEST_SERVER;
		sqjc_scmd = new HashMap<String, Object>();
		sqjc_scmd.put("cmd", scmd_sqjc);
		new Thread(querythreshold_sqjc).start();
	}
	
//赵家点击事件
private void page_five() {
	
	Toast.makeText(getApplicationContext(), "该基地暂无监测站数据", Toast.LENGTH_LONG).show();

}	

	// 开启子线程取气象站的传感器数据
	private Runnable query_qxz = new Runnable() {

		@Override
		public void run() {
			while (nThread) {
				try {
					String path = "http://" + IP + "/AppService.php";

					String reqdata = HttpReqService.postRequest(path, qxz_data, "GB2312");
					// Log.d("debugTest", "qxzreqdata -- " + reqdata);
					if (reqdata != null) {
						// 子线程用sedMessage()方法传递Message对象
						Message msg = mhandler_qxz.obtainMessage(FinalConstant.GT_QUERY_BACK_QXZ_DATA);
						Bundle bundle = new Bundle();// 创建一个句柄
						bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);// 将reqdata填充入句柄
						msg.setData(bundle);// 设置一个任意数据值的Bundle对象。
						mhandler_qxz.sendMessage(msg);
						// Log.d("hello", "hello气象站传感器数据 -- " + reqdata);
					}
					Thread.sleep(10000);// 线程暂停10秒，单位毫秒，启动线程后，线程每10秒发送一次消息
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
	// 请求四圣村墒情监测点传感器参数子线程
	private Runnable query_sqjc1 = new Runnable() {

		@Override
		public void run() {
			while (nThread) {
				try {
					String path = "http://" + IP + "/AppService.php";
					String data = HttpReqService.postRequest(path, sqjc_data, "GB2312");
					if (data != null) {
						Message msg = mhandler_sqjc1.obtainMessage(FinalConstant.GT_QUERY_BACK_SQJC1_DATA);
						Bundle bundle = new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler_sqjc1.sendMessage(msg);
					}
					Thread.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};

	// 请求乌杨村墒情监测点传感器参数子线程
	Runnable query_sqjc2 = new Runnable() {

		@Override
		public void run() {
			while (nThread) {
				try {
					String path = "http://" + IP + "/AppService.php";
					String data = HttpReqService.postRequest(path, sqjc_data, "GB2312");
					Log.d("debugTest", "sqjc2_data -- " + data);
					if (data != null) {
						Message msg = mhandler_sqjc2.obtainMessage(FinalConstant.GT_QUERY_BACK_SQJC2_DATA);
						Bundle bundle = new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler_sqjc2.sendMessage(msg);
					}
					Thread.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};

	// 请求南雅村墒情监测点传感器参数子线程
	Runnable query_sqjc3 = new Runnable() {

		@Override
		public void run() {
			while (nThread) {
				try {
					String path = "http://" + IP + "/AppService.php";
					String data = HttpReqService.postRequest(path, sqjc_data, "GB2312");
					Log.d("debugTest", "sqjc3_data -- " + data);
					if (data != null) {
						Message msg = mhandler_sqjc3.obtainMessage(FinalConstant.GT_QUERY_BACK_SQJC3_DATA);
						Bundle bundle = new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler_sqjc3.sendMessage(msg);
					}
					Thread.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};

	// 开启线程请求气象站阈值设置数据
	Runnable querythreshold_qxz = new Runnable() {

		@Override
		public void run() {
			try {
				String path = "http://" + IP + "/AppService.php";
				String data = HttpReqService.postRequest(path, qxz_scmd, "GB2312");
				Log.d("hello", "hello气象站阈值--------" + data);
				if (data.equals("1")) {
					Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
				} else {
					JSONArray array = new JSONArray(data);
					JSONObject cmd_tag = (JSONObject) array.get(0);
					String str_cmd = cmd_tag.getString("cmd");
					int len = 0;
					len = array.length();
					if (len > 1) {
						if (str_cmd.equals(FinalConstant.YZSZ_QXZ_WARN_REBACK_SERVER)) {
							if (!array.get(1).equals(false)) {
								thresholdValue_qxz = new ArrayList<HashMap<String, Object>>();
								Log.i("info", "取阈值里面" + thresholdValue_qxz.toString());
								for (int i = 1; i < 2; i++) {
									JSONObject temp = (JSONObject) array.get(i);
									HashMap<String, Object> map = new HashMap<String, Object>();
									map.put("qxzNum", 
											Integer.parseInt(temp.getString("qxzNum")));
									map.put("et_Air_Temp_Up",
											Integer.parseInt(temp.getString("et_Air_Temp_Up"))); // 空气温度上限
									map.put("et_Air_Temp_Floor",
											Integer.parseInt(temp.getString("et_Air_Temp_Floor"))); // 空气温度下限
									map.put("et_Air_Damp_Up", 
											Integer.parseInt(temp.getString("et_Air_Damp_Up"))); // 空气湿度上限
									map.put("et_Air_Damp_Floor",
											Integer.parseInt(temp.getString("et_Air_Damp_Floor")));// 空气湿度下限
									map.put("et_Dew_Point_Up",
											Integer.parseInt(temp.getString("et_Dew_Point_Up")));// 露点上限
									map.put("et_Dew_Point_Floor",
											Integer.parseInt(temp.getString("et_Dew_Point_Floor"))); // 露点下限
									map.put("et_Illuminance_Up",
											Integer.parseInt(temp.getString("et_Illuminance_Up"))); // 光照度上限
									map.put("et_Illuminance_Floor",
											Integer.parseInt(temp.getString("et_Illuminance_Floor"))); // 光照度下限
									map.put("et_Solar_Radiation_Up",
											Integer.parseInt(temp.getString("et_Solar_Radiation_Up"))); // 太阳辐射上限
									map.put("et_Solar_Radiation_Floor",
											Integer.parseInt(temp.getString("et_Solar_Radiation_Floor"))); // 太阳辐射下限
									map.put("et_Surface_Soil_Moisture_Up",
											Integer.parseInt(temp.getString("et_Surface_Soil_Moisture_Up"))); // 表层土壤水分上限
									map.put("et_Surface_Soil_Moisture_Floor",
											Integer.parseInt(temp.getString("et_Surface_Soil_Moisture_Floor"))); // 表层土壤水分下限
									map.put("et_Shallow_Soil_Moisture_Up",
											Integer.parseInt(temp.getString("et_Shallow_Soil_Moisture_Up"))); // 浅层土壤水分上限
									map.put("et_Shallow_Soil_Moisture_Floor",
											Integer.parseInt(temp.getString("et_Shallow_Soil_Moisture_Floor"))); // 浅层土壤水分下限
									map.put("et_Intermediate_Soil_Moisture_Up",
											Integer.parseInt(temp.getString("et_Intermediate_Soil_Moisture_Up"))); // 中层土壤水分上限
									map.put("et_Intermediate_Soil_Moisture_Floor",
											Integer.parseInt(temp.getString("et_Intermediate_Soil_Moisture_Floor"))); // 中层土壤水分下限
									map.put("et_Deep_Soil_Moisture_Up",
											Integer.parseInt(temp.getString("et_Deep_Soil_Moisture_Up"))); // 深层土壤水分上限
									map.put("et_Deep_Soil_Moisture_Floor",
											Integer.parseInt(temp.getString("et_Deep_Soil_Moisture_Floor"))); // 深层土壤水分下限
									map.put("et_Surface_Soil_Temp_Up",
											Integer.parseInt(temp.getString("et_Surface_Soil_Temp_Up"))); // 表层土壤温度上限
									map.put("et_Surface_Soil_Temp_Floor",
											Integer.parseInt(temp.getString("et_Surface_Soil_Temp_Floor"))); // 表层土壤温度下限
									map.put("et_Shallow_Soil_Temp_Up",
											Integer.parseInt(temp.getString("et_Shallow_Soil_Temp_Up"))); // 浅层土壤温度上限
									map.put("et_Shallow_Soil_Temp_Floor",
											Integer.parseInt(temp.getString("et_Shallow_Soil_Temp_Floor"))); // 浅层土壤温度下限
									map.put("et_Intermediate_Soil_Temp_Up",
											Integer.parseInt(temp.getString("et_Intermediate_Soil_Temp_Up"))); // 中层土壤温度上限
									map.put("et_Intermediate_Soil_Temp_Floor",
											Integer.parseInt(temp.getString("et_Intermediate_Soil_Temp_Floor"))); // 中层土壤温度下限
									map.put("et_Deep_Soil_Temp_Up",
											Integer.parseInt(temp.getString("et_Deep_Soil_Temp_Up"))); // 深层土壤温度上限
									map.put("et_Deep_Soil_Temp_Floor",
											Integer.parseInt(temp.getString("et_Deep_Soil_Temp_Floor"))); // 深层土壤温度下限

									thresholdValue_qxz.add(map);
									Log.d("hello", "【Runnable】就是这里阈值 -- " + thresholdValue_qxz.toString());
									Log.i("info", map.toString());

								}

							}
						}
					}
				}
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	};

	// 开启子线程请求墒情监测站阈值设置数据
	Runnable querythreshold_sqjc = new Runnable() {

		@Override
		public void run() {
			try {
				String path = "http://" + IP + "/AppService.php";
				String data = HttpReqService.postRequest(path, sqjc_scmd, "GB2312");
				if (data.equals("1")) {
					Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
				} else {
					JSONArray array = new JSONArray(data);
					JSONObject cmd_tag = (JSONObject) array.get(0);
					String str_cmd = cmd_tag.getString("cmd");
					int len = 0;
					len = array.length();
					if (len > 1) {
						if (str_cmd.equals(FinalConstant.YZSZ_SQJC_WARN_REBACK_SERVER)) {
							if (!array.get(1).equals(false)) {
								thresholdValue_sqjc = new ArrayList<HashMap<String, Object>>();
								for (int i = 1; i < 2; i++) {
									JSONObject temp = (JSONObject) array.get(i);
									HashMap<String, Object> map = new HashMap<String, Object>();
									map.put("sqjcNum", 
											Integer.parseInt(temp.getString("sqjcNum")));
									map.put("et_Sqjc_Air_Temp_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Air_Temp_Up")));
									map.put("et_Sqjc_Air_Temp_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Air_Temp_Floor")));
									map.put("et_Sqjc_Air_Damp_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Air_Damp_Up")));
									map.put("et_Sqjc_Air_Damp_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Air_Damp_Floor")));
									map.put("et_Sqjc_Barometric_Pressure_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Barometric_Pressure_Up")));
									map.put("et_Sqjc_Barometric_Pressure_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Barometric_Pressure_Floor")));
									map.put("et_Sqjc_Soil_Moisture_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_Moisture_Up")));
									map.put("et_Sqjc_Soil_Moisture_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_Moisture_Floor")));
									map.put("et_Sqjc_Soil_Temp_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_Temp_Up")));
									map.put("et_Sqjc_Soil_Temp_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_Temp_Floor")));
									map.put("et_Sqjc_Soil_PH_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_PH_Up")));
									map.put("et_Sqjc_Soil_PH_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_PH_Floor")));
									map.put("et_Sqjc_Soil_NH3N_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_NH3N_Up")));
									map.put("et_Sqjc_Soil_NH3N_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_NH3N_Floor")));
									map.put("et_Sqjc_Soil_NH4NO3_Up",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_NH4NO3_Up")));
									map.put("et_Sqjc_Soil_NH4NO3_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_NH4NO3_Floor")));
									map.put("et_Sqjc_Soil_K_Up", 
											Integer.parseInt(temp.getString("et_Sqjc_Soil_K_Up")));
									map.put("et_Sqjc_Soil_K_Floor",
											Integer.parseInt(temp.getString("et_Sqjc_Soil_K_Floor")));
									thresholdValue_sqjc.add(map);
									Log.i("info", map.toString());

								}
							}
						}
					}
				}
				Thread.sleep(10000);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	};
	// 气象站消息返回机制
	private Handler mhandler_qxz = new Handler() {

		public void handleMessage(Message msg) {
			switch (msg.what) {
			case FinalConstant.GT_QUERY_BACK_QXZ_DATA:
				String qxz_data = msg.getData().getString(FinalConstant.BACK_INFO);

				if (qxz_data.equals("1")) {
					Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
				} else {
					try {
						JSONArray array = new JSONArray(qxz_data);
						JSONObject object = (JSONObject) array.get(0);
						String cmd = object.getString("cmd");
						int len = 0;
						len = array.length();
						if (len > 1) {
							if (cmd.equals(FinalConstant.QXZFIND_REBACK_SERVER)) {
								String str = judge_qxz_params(array, FinalConstant.GT_QUERY_BACK_QXZ_DATA);
								String str2 = "高于";
								String str3 = "低于";
								// String str4="正常";
								str = str.replaceAll(str2, "<font color='red'>" + str2 + "</font>");
								str = str.replaceAll(str3, "<font color='blue'>" + str3 + "</font>");
								// str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
								tv_YzszWarn1.setText(Html.fromHtml("临江镇福德村：" + str));
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}

		// 气象站比较函数
		private String judge_qxz_params(JSONArray array, int qxzNum) throws NumberFormatException, JSONException {
			int et_Air_Temp_Up, et_Air_Temp_Floor;// 空气温度
			int et_Air_Damp_Up, et_Air_Damp_Floor;// 空气湿度
			int et_Dew_Point_Up, et_Dew_Point_Floor;// 露点
			int et_Illuminance_Up, et_Illuminance_Floor;// 光照度
			int et_Solar_Radiation_Up, et_Solar_Radiation_Floor;// 太阳辐射
			int et_Surface_Soil_Moisture_Up, et_Surface_Soil_Moisture_Floor;// 表层土壤水分
			int et_Shallow_Soil_Moisture_Up, et_Shallow_Soil_Moisture_Floor;// 浅层土壤水分
			int et_Intermediate_Soil_Moisture_Up, et_Intermediate_Soil_Moisture_Floor;// 中层土壤水分
			int et_Deep_Soil_Moisture_Up, et_Deep_Soil_Moisture_Floor; // 深层土壤水分
			int et_Surface_Soil_Temp_Up, et_Surface_Soil_Temp_Floor; // 表层土壤温度
			int et_Shallow_Soil_Temp_Up, et_Shallow_Soil_Temp_Floor; // 浅层土壤温度
			int et_Intermediate_Soil_Temp_Up, et_Intermediate_Soil_Temp_Floor; // 中层土壤温度
			int et_Deep_Soil_Temp_Up, et_Deep_Soil_Temp_Floor;// 深层土壤温度

			// 输出警报信息
			String et_Air_Temp_msg = ""; // 空气温度
			String et_Air_Damp_msg = ""; // 空气湿度
			String et_Dew_Point_msg = ""; // 露点
			String et_Illuminance_msg = ""; // 光照度
			String et_Solar_Radiation_msg = ""; // 太阳辐射
			String et_Surface_Soil_Moisture_msg = ""; // 表层土壤水分
			String et_Shallow_Soil_Moisture_msg = ""; // 浅层土壤水分
			String et_Intermediate_Soil_Moisture_msg = ""; // 中层土壤水分
			String et_Deep_Soil_Moisture_msg = ""; // 深层土壤水分
			String et_Surface_Soil_Temp_msg = ""; // 表层土壤温度
			String et_Shallow_Soil_Temp_msg = ""; // 浅层土壤温度
			String et_Intermediate_Soil_Temp_msg = ""; // 中层土壤温度
			String et_Deep_Soil_Temp_msg = ""; // 深层土壤温度

			// 取传感器数据
			JSONArray arr_data = (JSONArray) array.get(1);
			JSONObject info = (JSONObject) arr_data.get(0);
			
			//打印每一条数据确定都取到了数据
			Log.i("info", info.toString());
			double m_Air_Temp = Double.parseDouble(info.getString("yn_air_temp"));
			Log.i("info", String.valueOf(m_Air_Temp));
			
			double m_Air_Damp = Double.parseDouble(info.getString("yn_air_humi"));
			Log.i("info", String.valueOf(m_Air_Damp));
			
			double m_Dew_Point = Double.parseDouble(info.getString("yn_dew_point"));
			Log.i("info", String.valueOf(m_Dew_Point));
			
			double m_Illuminance = Double.parseDouble(info.getString("yn_illuminance"));
			Log.i("info", String.valueOf(m_Illuminance));
			
			double m_Solar_Radiation = Double.parseDouble(info.getString("yn_radiation"));
			Log.i("info", String.valueOf(m_Solar_Radiation));
			
			double m_Surface_Soil_Moisture = Double.parseDouble(info.getString("yn_water_1content"));
			Log.i("info", String.valueOf(m_Surface_Soil_Moisture));
			
			double m_Shallow_Soil_Moisture = Double.parseDouble(info.getString("yn_water_2content"));
			Log.i("info", String.valueOf(m_Shallow_Soil_Moisture));
			
			double m_Intermediate_Soil_Moisture = Double.parseDouble(info.getString("yn_water_3content"));
			Log.i("info", String.valueOf(m_Intermediate_Soil_Moisture));
			
			double m_Deep_Soil_Moisture = Double.parseDouble(info.getString("yn_water_4content"));
			Log.i("info", String.valueOf(m_Deep_Soil_Moisture));
			
			double m_Surface_Soil_Temp = Double.parseDouble(info.getString("yn_solid_1temp"));
			Log.i("info", String.valueOf(m_Surface_Soil_Temp));
			
			double m_Shallow_Soil_Temp = Double.parseDouble(info.getString("yn_solid_2temp"));
			Log.i("info", String.valueOf(m_Shallow_Soil_Temp));
			
			double m_Intermediate_Soil_Temp = Double.parseDouble(info.getString("yn_solid_3temp"));
			Log.i("info", String.valueOf(m_Intermediate_Soil_Temp));
			
			double m_Deep_Soil_Temp = Double.parseDouble(info.getString("yn_solid_4temp"));
			Log.i("info", String.valueOf(m_Deep_Soil_Temp));

			// 取阈值
			// Log.i("info", "比较函数里面"+thresholdValue_qxz.toString());

			HashMap<String, Object> sensordata = thresholdValue_qxz.get(qxzNum - 1);
			Log.d("hello", "【judge_qxz_params】就是这里阈值 -- " + thresholdValue_qxz.toString());

			int qm = (Integer) sensordata.get("qxzNum");
			if (qm == qxzNum) {
				et_Air_Temp_Up = (Integer) sensordata.get("et_Air_Temp_Up");// 空气温度上限

				et_Air_Temp_Floor = (Integer) sensordata.get("et_Air_Temp_Floor");// 空气温度下限

				et_Air_Damp_Up = (Integer) sensordata.get("et_Air_Damp_Up");// 空气湿度上限

				et_Air_Damp_Floor = (Integer) sensordata.get("et_Air_Damp_Floor");// 空气湿度下限

				et_Dew_Point_Up = (Integer) sensordata.get("et_Dew_Point_Up");// 露点上限

				et_Dew_Point_Floor = (Integer) sensordata.get("et_Dew_Point_Floor");// 露点下限

				et_Illuminance_Up = (Integer) sensordata.get("et_Illuminance_Up");// 光照度上限

				et_Illuminance_Floor = (Integer) sensordata.get("et_Illuminance_Floor");// 光照度下限

				et_Solar_Radiation_Up = (Integer) sensordata.get("et_Solar_Radiation_Up");// 太阳辐射上限

				et_Solar_Radiation_Floor = (Integer) sensordata.get("et_Solar_Radiation_Floor");// 太阳辐射下限

				et_Surface_Soil_Moisture_Up = (Integer) sensordata.get("et_Surface_Soil_Moisture_Up");// 表层土壤水分上限

				et_Surface_Soil_Moisture_Floor = (Integer) sensordata.get("et_Surface_Soil_Moisture_Floor");// 表层土壤水分下限

				et_Shallow_Soil_Moisture_Up = (Integer) sensordata.get("et_Shallow_Soil_Moisture_Up");// 浅层土壤水分上限

				et_Shallow_Soil_Moisture_Floor = (Integer) sensordata.get("et_Shallow_Soil_Moisture_Floor");// 浅层土壤水分下限

				et_Intermediate_Soil_Moisture_Up = (Integer) sensordata.get("et_Intermediate_Soil_Moisture_Up");// 中层土壤水分上限

				et_Intermediate_Soil_Moisture_Floor = (Integer) sensordata.get("et_Intermediate_Soil_Moisture_Floor");// 中层土壤水分下限

				et_Deep_Soil_Moisture_Up = (Integer) sensordata.get("et_Deep_Soil_Moisture_Up");// 深层土壤水分上限

				et_Deep_Soil_Moisture_Floor = (Integer) sensordata.get("et_Deep_Soil_Moisture_Floor");// 深层土壤水分上限

				et_Surface_Soil_Temp_Up = (Integer) sensordata.get("et_Surface_Soil_Temp_Up");// 表层土壤温度上限

				et_Surface_Soil_Temp_Floor = (Integer) sensordata.get("et_Surface_Soil_Temp_Floor");// 表层土壤温度下限

				et_Shallow_Soil_Temp_Up = (Integer) sensordata.get("et_Shallow_Soil_Temp_Up");// 浅层土壤温度上限

				et_Shallow_Soil_Temp_Floor = (Integer) sensordata.get("et_Shallow_Soil_Temp_Floor");// 浅层土壤温度下限

				et_Intermediate_Soil_Temp_Up = (Integer) sensordata.get("et_Intermediate_Soil_Temp_Up");// 中层土壤温度上限

				et_Intermediate_Soil_Temp_Floor = (Integer) sensordata.get("et_Intermediate_Soil_Temp_Floor");// 中层土壤温度下限

				et_Deep_Soil_Temp_Up = (Integer) sensordata.get("et_Deep_Soil_Temp_Up");// 深层土壤温度上限

				et_Deep_Soil_Temp_Floor = (Integer) sensordata.get("et_Deep_Soil_Temp_Floor");// 深层土壤温度下限

				String time = info.getString("yn_time");

				// 比较空气温度
				if (m_Air_Temp >= et_Air_Temp_Floor && m_Air_Temp <= et_Air_Temp_Up) {
					et_Air_Temp_msg = "空气温度正常";
				} else if (m_Air_Temp > et_Air_Temp_Up) {
					et_Air_Temp_msg = "空气温度高于阈值";
				} else {
					et_Air_Temp_msg = "空气温度低于阈值";
				}

				// 比较空气湿度
				if (m_Air_Damp >= et_Air_Damp_Floor && m_Air_Damp <= et_Air_Damp_Up) {
					et_Air_Damp_msg = "空气湿度正常";
				} else if (m_Air_Damp > et_Air_Damp_Up) {
					et_Air_Damp_msg = "空气湿度高于阈值";
				} else {
					et_Air_Damp_msg = "空气湿度低于阈值";
				}

				// 比较露点
				if (m_Dew_Point >= et_Dew_Point_Floor && m_Dew_Point <= et_Dew_Point_Up) {
					et_Dew_Point_msg = "露点正常";
				} else if (m_Dew_Point > et_Dew_Point_Up) {
					et_Dew_Point_msg = "露点高于阈值";
				} else {
					et_Dew_Point_msg = "露点低于阈值";
				}

				// 比较光照度
				if (m_Illuminance >= et_Illuminance_Floor && m_Illuminance <= et_Illuminance_Up) {
					et_Illuminance_msg = "光照度正常";
				} else if (m_Illuminance > et_Illuminance_Up) {
					et_Illuminance_msg = "光照度高于阈值";
				} else {
					et_Illuminance_msg = "光照度低于阈值";
				}

				// 比较太阳辐射
				if (m_Solar_Radiation >= et_Solar_Radiation_Floor && m_Solar_Radiation <= et_Solar_Radiation_Up) {
					et_Solar_Radiation_msg = "太阳辐射正常";
				} else if (m_Solar_Radiation > et_Solar_Radiation_Up) {
					et_Solar_Radiation_msg = "太阳辐射高于阈值";
				} else {
					et_Solar_Radiation_msg = "太阳辐射低于阈值";
				}

				// 比较表层土壤水分
				if (m_Surface_Soil_Moisture >= et_Surface_Soil_Moisture_Floor
						&& m_Surface_Soil_Moisture <= et_Surface_Soil_Moisture_Up) {
					et_Surface_Soil_Moisture_msg = "表层土壤水分正常";
				} else if (m_Surface_Soil_Moisture > et_Surface_Soil_Moisture_Up) {
					et_Surface_Soil_Moisture_msg = "表层土壤水分高于阈值";
				} else {
					et_Surface_Soil_Moisture_msg = "表层土壤水分低于阈值";
				}

				// 比较浅层土壤水分
				if (m_Shallow_Soil_Moisture >= et_Shallow_Soil_Moisture_Floor
						&& m_Shallow_Soil_Moisture <= et_Shallow_Soil_Moisture_Up) {
					et_Shallow_Soil_Moisture_msg = "浅层土壤水分正常";
				} else if (m_Shallow_Soil_Moisture > et_Shallow_Soil_Moisture_Up) {
					et_Shallow_Soil_Moisture_msg = "浅层土壤水分高于阈值";
				} else {
					et_Shallow_Soil_Moisture_msg = "浅层土壤水分低于阈值";
				}

				// 比较中层土壤水分
				if (m_Intermediate_Soil_Moisture >= et_Intermediate_Soil_Moisture_Floor
						&& m_Intermediate_Soil_Moisture <= et_Intermediate_Soil_Moisture_Up) {
					et_Intermediate_Soil_Moisture_msg = "中层土壤水分正常";
				} else if (m_Intermediate_Soil_Moisture > et_Intermediate_Soil_Moisture_Up) {
					et_Intermediate_Soil_Moisture_msg = "中层土壤水分高于阈值";
				} else {
					et_Intermediate_Soil_Moisture_msg = "中层土壤水分低于阈值";
				}

				// 比较深层土壤水分
				if (m_Deep_Soil_Moisture >= et_Deep_Soil_Moisture_Floor
						&& m_Deep_Soil_Moisture <= et_Deep_Soil_Moisture_Up) {
					et_Deep_Soil_Moisture_msg = "深层土壤水分正常";
				} else if (m_Deep_Soil_Moisture > et_Deep_Soil_Moisture_Up) {
					et_Deep_Soil_Moisture_msg = "深层土壤水分高于阈值";
				} else {
					et_Deep_Soil_Moisture_msg = "深层土壤水分低于阈值";
				}

				// 比较表层土壤温度
				if (m_Surface_Soil_Temp >= et_Surface_Soil_Temp_Floor
						&& m_Surface_Soil_Temp <= et_Surface_Soil_Temp_Up) {
					et_Surface_Soil_Temp_msg = "表层土壤温度正常";
				} else if (m_Surface_Soil_Temp > et_Surface_Soil_Temp_Up) {
					et_Surface_Soil_Temp_msg = "表层土壤温度高于阈值";
				} else {
					et_Surface_Soil_Temp_msg = "表层土壤温度低于阈值";
				}

				// 比较浅层土壤温度
				if (m_Shallow_Soil_Temp >= et_Shallow_Soil_Temp_Floor
						&& m_Shallow_Soil_Temp <= et_Shallow_Soil_Temp_Up) {
					et_Shallow_Soil_Temp_msg = "浅层土壤温度正常";
				} else if (m_Shallow_Soil_Temp > et_Shallow_Soil_Temp_Up) {
					et_Shallow_Soil_Temp_msg = "浅层土壤温度高于阈值";
				} else {
					et_Shallow_Soil_Temp_msg = "浅层土壤温度低于阈值";
				}

				// 比较中层土壤温度
				if (m_Intermediate_Soil_Temp >= et_Intermediate_Soil_Temp_Floor
						&& m_Intermediate_Soil_Temp <= et_Intermediate_Soil_Temp_Up) {
					et_Intermediate_Soil_Temp_msg = "中层土壤温度正常";
				} else if (m_Intermediate_Soil_Temp > et_Intermediate_Soil_Temp_Up) {
					et_Intermediate_Soil_Temp_msg = "中层土壤温度高于阈值";
				} else {
					et_Intermediate_Soil_Temp_msg = "中层土壤温度低于阈值";
				}

				// 比较深层土壤温度
				if (m_Deep_Soil_Temp >= et_Deep_Soil_Temp_Floor && m_Deep_Soil_Temp <= et_Deep_Soil_Temp_Up) {
					et_Deep_Soil_Temp_msg = "深层土壤温度正常";
				} else if (m_Deep_Soil_Temp > et_Deep_Soil_Temp_Up) {
					et_Deep_Soil_Temp_msg = "深层土壤温度高于阈值";
				} else {
					et_Deep_Soil_Temp_msg = "深层土壤温度低于阈值";
				}

				Log.i("info", et_Air_Temp_msg + "#" + et_Air_Damp_msg + "#" + et_Dew_Point_msg + "#"
						+ et_Illuminance_msg + "#" + et_Solar_Radiation_msg + "#" + et_Surface_Soil_Moisture_msg + "#"
						+ et_Shallow_Soil_Moisture_msg + "#" + et_Intermediate_Soil_Moisture_msg + "#"
						+ et_Deep_Soil_Moisture_msg + "#" + et_Surface_Soil_Temp_msg + "#" + et_Shallow_Soil_Temp_msg
						+ "#" + et_Intermediate_Soil_Temp_msg + "#" + et_Deep_Soil_Temp_msg);

				return "<br>"+""
						+ "<br>" +""
				        + "<br>" + et_Air_Temp_msg 
				        + "<br>" +""
						+ "<br>" + et_Air_Damp_msg 
						+ "<br>" +""
						+ "<br>" + et_Dew_Point_msg
						+ "<br>" +""
						+ "<br>" + et_Illuminance_msg 
						+ "<br>" +""
						+ "<br>" + et_Solar_Radiation_msg
						+ "<br>" +""
						+ "<br>" + et_Surface_Soil_Moisture_msg 
						+ "<br>" +""
						+ "<br>" + et_Shallow_Soil_Moisture_msg 
						+ "<br>" +""
						+ "<br>" + et_Intermediate_Soil_Moisture_msg 
						+ "<br>" +""
						+ "<br>" + et_Deep_Soil_Moisture_msg 
						+ "<br>" +""
						+ "<br>" + et_Surface_Soil_Temp_msg
						+ "<br>" +""
						+ "<br>" + et_Shallow_Soil_Temp_msg
						+ "<br>" +""
						+ "<br>" + et_Intermediate_Soil_Temp_msg 
						+ "<br>" +""
						+ "<br>" + et_Deep_Soil_Temp_msg 
						+ "<br>" +""
						+ "<br>" + time + "</br>";

			}
			return null;
		}

	};
	// 四圣村消息返回机制
	private Handler mhandler_sqjc1 = new Handler() {

		public void handleMessage(Message msg) {
			switch (msg.what) {
			case FinalConstant.GT_QUERY_BACK_SQJC1_DATA:
				String qxz_data = msg.getData().getString(FinalConstant.BACK_INFO);

				if (qxz_data.equals("1")) {
					Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
				} else {
					try {
						JSONArray array = new JSONArray(qxz_data);
						JSONObject object = (JSONObject) array.get(0);
						String cmd = object.getString("cmd");
						int len = 0;
						len = array.length();
						if (len > 1) {
							if (cmd.equals(FinalConstant.SQZ1FIND_REBACK_SERVER)) {
								String str = judge_sqjc_params(array, FinalConstant.GT_QUERY_BACK_SQJC1_DATA);
								String str2 = "高于";
								String str3 = "低于";
								// String str4="正常";
								str = str.replaceAll(str2, "<font color='red'>" + str2 + "</font>");
								str = str.replaceAll(str3, "<font color='blue'>" + str3 + "</font>");
								// str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
								tv_YzszWarn1.setText(Html.fromHtml("白鹤街道四圣村：" + str));
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	};
	// 乌杨村消息返回机制
	private Handler mhandler_sqjc2 = new Handler() {

		public void handleMessage(Message msg) {
			switch (msg.what) {
			case FinalConstant.GT_QUERY_BACK_SQJC2_DATA:
				String qxz_data = msg.getData().getString(FinalConstant.BACK_INFO);

				if (qxz_data.equals("1")) {
					Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
				} else {
					try {
						JSONArray array = new JSONArray(qxz_data);
						JSONObject object = (JSONObject) array.get(0);
						String cmd = object.getString("cmd");
						int len = 0;
						len = array.length();
						if (len > 1) {
							if (cmd.equals(FinalConstant.SQZ2FIND_REBACK_SERVER)) {
								String str = judge_sqjc_params(array, FinalConstant.GT_QUERY_BACK_SQJC2_DATA);
								String str2 = "高于";
								String str3 = "低于";
								// String str4="正常";
								str = str.replaceAll(str2, "<font color='red'>" + str2 + "</font>");
								str = str.replaceAll(str3, "<font color='blue'>" + str3 + "</font>");
								// str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
								tv_YzszWarn1.setText(Html.fromHtml("丰乐街道乌杨村：" + str));
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	};
	// 南雅消息返回机制
	private Handler mhandler_sqjc3 = new Handler() {

		public void handleMessage(Message msg) {
			switch (msg.what) {
			case FinalConstant.GT_QUERY_BACK_SQJC3_DATA:
				String qxz_data = msg.getData().getString(FinalConstant.BACK_INFO);

				if (qxz_data.equals("1")) {
					Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
				} else {
					try {
						JSONArray array = new JSONArray(qxz_data);
						JSONObject object = (JSONObject) array.get(0);
						String cmd = object.getString("cmd");
						int len = 0;
						len = array.length();
						if (len > 1) {
							if (cmd.equals(FinalConstant.SQZ3FIND_REBACK_SERVER)) {
								String str = judge_sqjc_params(array, FinalConstant.GT_QUERY_BACK_SQJC3_DATA);
								String str2 = "高于";
								String str3 = "低于";
								// String str4="正常";
								str = str.replaceAll(str2, "<font color='red'>" + str2 + "</font>");
								str = str.replaceAll(str3, "<font color='blue'>" + str3 + "</font>");
								// str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
								tv_YzszWarn1.setText(Html.fromHtml("南雅镇新全村：" + str));
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	};

	// 判断墒情传感器值是否高于阈值
	private String judge_sqjc_params(JSONArray array, int sqjcNum) throws NumberFormatException, JSONException {
		int et_Sqjc_Air_Temp_Up, et_Sqjc_Air_Temp_Floor; // 空气温度
		int et_Sqjc_Air_Damp_Up, et_Sqjc_Air_Damp_Floor; // 空气湿度
		int et_Sqjc_Barometric_Pressure_Up, et_Sqjc_Barometric_Pressure_Floor; // 大气压力
		int et_Sqjc_Soil_Moisture_Up, et_Sqjc_Soil_Moisture_Floor; // 土壤湿度
		int et_Sqjc_Soil_Temp_Up, et_Sqjc_Soil_Temp_Floor; // 土壤温度
		int et_Sqjc_Soil_PH_Up, et_Sqjc_Soil_PH_Floor; // 土壤PH
		int et_Sqjc_Soil_NH3N_Up, et_Sqjc_Soil_NH3N_Floor; // 土壤氨氮
		int et_Sqjc_Soil_NH4NO3_Up, et_Sqjc_Soil_NH4NO3_Floor; // 土壤硝铵
		int et_Sqjc_Soil_K_Up, et_Sqjc_Soil_K_Floor; // 土壤钾离子

		// 输出警报信息
		String et_Sqjc_Air_Temp_msg = "";
		String et_Sqjc_Air_Damp_msg = "";
		String et_Sqjc_Barometric_Pressure_msg = "";
		String et_Sqjc_Soil_Moisture_msg = "";
		String et_Sqjc_Soil_Temp_msg = "";
		String et_Sqjc_Soil_PH_msg = "";
		String et_Sqjc_Soil_NH3N_msg = "";
		String et_Sqjc_Soil_NH4NO3_msg = "";
		String et_Sqjc_Soil_K_msg = "";

		// 取传感器数据
		JSONArray arr_data = (JSONArray) array.get(1);
		JSONObject info = (JSONObject) arr_data.get(0);
		// Log.i("info", info.toString());
		double m_Sqjc_Air_Temp = Double.parseDouble(info.getString("yn_air_temp"));
		Log.i("info", String.valueOf(m_Sqjc_Air_Temp));
		double m_Sqjc_Air_Damp = Double.parseDouble(info.getString("yn_air_humi"));
		Log.i("info", String.valueOf(m_Sqjc_Air_Damp));
		double m_Sqjc_Barometric_Pressure = Double.parseDouble(info.getString("yn_air_pressure"));
		double m_Sqjc_Soil_Moisture = Double.parseDouble(info.getString("yn_water_content"));
		double m_Sqjc_Soil_Temp = Double.parseDouble(info.getString("yn_soil_temp"));
		double m_Sqjc_Soil_PH = Double.parseDouble(info.getString("yn_soil_ph"));
		double m_Sqjc_Soil_NH3N = Double.parseDouble(info.getString("yn_soil_nh3n"));
		double m_Sqjc_Soil_NH4NO3 = Double.parseDouble(info.getString("yn_soil_nh4no3"));
		double m_Sqjc_Soil_K = Double.parseDouble(info.getString("yn_soil_k"));
		// 取阈值

		HashMap<String, Object> sensordata = thresholdValue_sqjc.get(sqjcNum - 1);
		// Log.i("info","大家好才是真的好"+ thresholdValue_sqjc.toString());
		int sm = (Integer) sensordata.get("sqjcNum");
		if (sm == sqjcNum) {
			et_Sqjc_Air_Temp_Up = (Integer) sensordata.get("et_Sqjc_Air_Temp_Up");// 空气温度上限
			Log.i("info", "最新" + thresholdValue_sqjc.toString() + "--------------------" + et_Sqjc_Air_Temp_Up);
			
			et_Sqjc_Air_Temp_Floor = (Integer) sensordata.get("et_Sqjc_Air_Temp_Floor");// 空气温度下限
			Log.i("info", "----------1----------" + et_Sqjc_Air_Temp_Floor);
			
			et_Sqjc_Air_Damp_Up = (Integer) sensordata.get("et_Sqjc_Air_Damp_Up");// 空气湿度上限
			Log.i("info", "----------2----------" + et_Sqjc_Air_Damp_Up);
			
			et_Sqjc_Air_Damp_Floor = (Integer) sensordata.get("et_Sqjc_Air_Damp_Floor");// 空气湿度下限
			Log.i("info", "----------3----------" + et_Sqjc_Air_Damp_Floor);
			
			et_Sqjc_Barometric_Pressure_Up = (Integer) sensordata.get("et_Sqjc_Barometric_Pressure_Up");// 大气压力上限
			Log.i("info", "----------4----------" + et_Sqjc_Barometric_Pressure_Up);
			
			et_Sqjc_Barometric_Pressure_Floor = (Integer) sensordata.get("et_Sqjc_Barometric_Pressure_Floor"); // 大气压力下限
			Log.i("info", "----------5----------" + et_Sqjc_Barometric_Pressure_Floor);
			
			et_Sqjc_Soil_Moisture_Up = (Integer) sensordata.get("et_Sqjc_Soil_Moisture_Up");// 土壤水分上限
			Log.i("info", "----------6----------" + et_Sqjc_Soil_Moisture_Up);
			
			et_Sqjc_Soil_Moisture_Floor = (Integer) sensordata.get("et_Sqjc_Soil_Moisture_Floor");// 土壤水分下限
			Log.i("info", "----------7----------" + et_Sqjc_Soil_Moisture_Floor);
			
			et_Sqjc_Soil_Temp_Up = (Integer) sensordata.get("et_Sqjc_Soil_Temp_Up");// 土壤温度上限
			Log.i("info", "----------8----------" + et_Sqjc_Soil_Temp_Up);
			
			et_Sqjc_Soil_Temp_Floor = (Integer) sensordata.get("et_Sqjc_Soil_Temp_Floor");// 土壤温度下限
			Log.i("info", "----------9----------" + et_Sqjc_Soil_Temp_Floor);
			
			et_Sqjc_Soil_PH_Up = (Integer) sensordata.get("et_Sqjc_Soil_PH_Up");// 土壤PH上限
			Log.i("info", "----------10----------" + et_Sqjc_Soil_PH_Up);
			
			et_Sqjc_Soil_PH_Floor = (Integer) sensordata.get("et_Sqjc_Soil_PH_Floor"); // 土壤PH下限
			Log.i("info", "----------11----------" + et_Sqjc_Soil_PH_Floor);
			
			et_Sqjc_Soil_NH3N_Up = (Integer) sensordata.get("et_Sqjc_Soil_NH3N_Up");// 土壤氨氮上限
			Log.i("info", "----------12----------" + et_Sqjc_Soil_NH3N_Up);
			
			et_Sqjc_Soil_NH3N_Floor = (Integer) sensordata.get("et_Sqjc_Soil_NH3N_Floor");// 土壤氨氮下限
			Log.i("info", "----------13----------" + et_Sqjc_Soil_NH3N_Floor);
			
			et_Sqjc_Soil_NH4NO3_Up = (Integer) sensordata.get("et_Sqjc_Soil_NH4NO3_Up");// 土壤硝铵上限
			Log.i("info", "----------14----------" + et_Sqjc_Soil_NH4NO3_Up);
			
			et_Sqjc_Soil_NH4NO3_Floor = (Integer) sensordata.get("et_Sqjc_Soil_NH4NO3_Floor"); // 土壤硝铵下限
			Log.i("info", "----------15----------" + et_Sqjc_Soil_NH4NO3_Floor);
			
			et_Sqjc_Soil_K_Up = (Integer) sensordata.get("et_Sqjc_Soil_K_Up");// 土壤钾离子上限
			Log.i("info", "----------16----------" + et_Sqjc_Soil_K_Up);
			
			et_Sqjc_Soil_K_Floor = (Integer) sensordata.get("et_Sqjc_Soil_K_Floor");// 土壤钾离子下限
			Log.i("info", "----------17----------" + et_Sqjc_Soil_K_Floor);
			
			String time = info.getString("yn_time");
			Log.i("info", "----------18----------" + time);

			// 比较空气温度
			if (m_Sqjc_Air_Temp >= et_Sqjc_Air_Temp_Floor && m_Sqjc_Air_Temp <= et_Sqjc_Air_Temp_Up) {
				et_Sqjc_Air_Temp_msg = "空气温度正常";
			} else if (m_Sqjc_Air_Temp > et_Sqjc_Air_Temp_Up) {
				et_Sqjc_Air_Temp_msg = "空气温度高于阈值";
			} else {
				et_Sqjc_Air_Temp_msg = "空气温度低于阈值";
			}

			// 比较空气湿度
			if (m_Sqjc_Air_Damp >= et_Sqjc_Air_Damp_Floor && m_Sqjc_Air_Damp <= et_Sqjc_Air_Damp_Up) {
				et_Sqjc_Air_Damp_msg = "空气湿度正常";
			} else if (m_Sqjc_Air_Damp > et_Sqjc_Air_Damp_Up) {
				et_Sqjc_Air_Damp_msg = "空气湿度高于阈值";
			} else {
				et_Sqjc_Air_Damp_msg = "空气湿度低于阈值";
			}

			// 比较大气压力
			if (m_Sqjc_Barometric_Pressure >= et_Sqjc_Barometric_Pressure_Floor
					&& m_Sqjc_Barometric_Pressure <= et_Sqjc_Barometric_Pressure_Up) {
				et_Sqjc_Barometric_Pressure_msg = "大气压力正常";
			} else if (m_Sqjc_Barometric_Pressure > et_Sqjc_Barometric_Pressure_Up) {
				et_Sqjc_Barometric_Pressure_msg = "大气压力高于阈值";
			} else {
				et_Sqjc_Barometric_Pressure_msg = "大气压力低于阈值";
			}

			// 比较土壤水分
			if (m_Sqjc_Soil_Moisture >= et_Sqjc_Soil_Moisture_Floor
					&& m_Sqjc_Soil_Moisture <= et_Sqjc_Soil_Moisture_Up) {
				et_Sqjc_Soil_Moisture_msg = "土壤水分正常";
			} else if (m_Sqjc_Soil_Moisture > et_Sqjc_Soil_Moisture_Up) {
				et_Sqjc_Soil_Moisture_msg = "土壤水分高于阈值";
			} else {
				et_Sqjc_Soil_Moisture_msg = "土壤水分低于阈值";
			}

			// 比较土壤温度
			if (m_Sqjc_Soil_Temp >= et_Sqjc_Soil_Temp_Floor && m_Sqjc_Soil_Temp <= et_Sqjc_Soil_Temp_Up) {
				et_Sqjc_Soil_Temp_msg = "土壤温度正常";
			} else if (m_Sqjc_Soil_Temp > et_Sqjc_Soil_Temp_Up) {
				et_Sqjc_Soil_Temp_msg = "土壤温度高于阈值";
			} else {
				et_Sqjc_Soil_Temp_msg = "土壤温度低于阈值";
			}

			// 比较土壤PH
			if (m_Sqjc_Soil_PH >= et_Sqjc_Soil_PH_Floor && m_Sqjc_Soil_PH <= et_Sqjc_Soil_PH_Up) {
				et_Sqjc_Soil_PH_msg = "土壤PH正常";
			} else if (m_Sqjc_Soil_PH > et_Sqjc_Soil_PH_Up) {
				et_Sqjc_Soil_PH_msg = "土壤PH高于阈值";
			} else {
				et_Sqjc_Soil_PH_msg = "土壤PH低于阈值";
			}

			// 比较土壤氨氮
			if (m_Sqjc_Soil_NH3N >= et_Sqjc_Soil_NH3N_Floor && m_Sqjc_Soil_NH3N <= et_Sqjc_Soil_NH3N_Up) {
				et_Sqjc_Soil_NH3N_msg = "土壤氨氮正常";
			} else if (m_Sqjc_Soil_NH3N > et_Sqjc_Soil_NH3N_Up) {
				et_Sqjc_Soil_NH3N_msg = "土壤氨氮高于阈值";
			} else {
				et_Sqjc_Soil_NH3N_msg = "土壤氨氮低于阈值";
			}

			// 比较土壤硝铵
			if (m_Sqjc_Soil_NH4NO3 >= et_Sqjc_Soil_NH4NO3_Floor && m_Sqjc_Soil_NH4NO3 <= et_Sqjc_Soil_NH4NO3_Up) {
				et_Sqjc_Soil_NH4NO3_msg = "土壤硝铵正常";
			} else if (m_Sqjc_Soil_NH4NO3 > et_Sqjc_Soil_NH4NO3_Up) {
				et_Sqjc_Soil_NH4NO3_msg = "土壤硝铵高于阈值";
			} else {
				et_Sqjc_Soil_NH4NO3_msg = "土壤硝铵低于阈值";
			}

			// 比较土壤钾离子
			if (m_Sqjc_Soil_K >= et_Sqjc_Soil_K_Floor && m_Sqjc_Soil_K <= et_Sqjc_Soil_K_Up) {
				et_Sqjc_Soil_K_msg = "土壤钾离子正常";
			} else if (m_Sqjc_Soil_K > et_Sqjc_Soil_K_Up) {
				et_Sqjc_Soil_K_msg = "土壤钾离子高于阈值";
			} else {
				et_Sqjc_Soil_K_msg = "土壤钾离子低于阈值";
			}

			Log.i("info",
					et_Sqjc_Air_Temp_msg + "#" + et_Sqjc_Air_Damp_msg + "#" + et_Sqjc_Barometric_Pressure_msg + "#"
							+ et_Sqjc_Soil_Moisture_msg + "#" + et_Sqjc_Soil_Temp_msg + "#" + et_Sqjc_Soil_PH_msg + "#"
							+ et_Sqjc_Soil_NH3N_msg + "#" + et_Sqjc_Soil_NH4NO3_msg + "#" + et_Sqjc_Soil_K_msg);

			return "<br>"+""
					+ "<br>" +""
					+ "<br>"  +et_Sqjc_Air_Temp_msg 
					+ "<br>" +""
					+ "<br>" + et_Sqjc_Air_Damp_msg 
					+ "<br>" +""
					+ "<br>" + et_Sqjc_Barometric_Pressure_msg 
					+ "<br>" +""
					+ "<br>" + et_Sqjc_Soil_Moisture_msg 
					+ "<br>" +""
					+ "<br>" + et_Sqjc_Soil_Temp_msg 
					+ "<br>" +""
					+ "<br>" + et_Sqjc_Soil_PH_msg 
					+ "<br>" +""
					+ "<br>" + et_Sqjc_Soil_NH3N_msg
					+ "<br>" +""
					+ "<br>" + et_Sqjc_Soil_NH4NO3_msg 
					+ "<br>" +""
					+ "<br>" + et_Sqjc_Soil_K_msg 
					+ "<br>" +""
					+ "<br>" + time+ "</br>";
		}
		return null;
	}
};
