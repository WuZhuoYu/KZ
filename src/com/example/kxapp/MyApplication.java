//package com.example.kxapp;
//
//import com.baidu.mapapi.SDKInitializer;
//import com.example.service.NetSDKLib;
//
//import android.app.Application;
//
//public class MyApplication extends Application {
//
//	@Override
//	public void onCreate() {
//		// TODO Auto-generated method stub
//		super.onCreate();
//		NetSDKLib.getInstance().init();//大华硬盘录像机SDK初始化
//		SDKInitializer.initialize(getApplicationContext());//百度地图SDK初始化
//	}
//
//}


/*这篇代码是最初定义的application，用于调用百度地图的SDK初始化，并在清单文件中什么MyApplication。
 之后因为要做大华的SDK初始化，所以将百度地图的初始化语句放到了NetSDKApplication。*/
