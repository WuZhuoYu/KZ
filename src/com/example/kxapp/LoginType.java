package com.example.kxapp;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


/**
@Name:登录模式*
@Description:用作测试大华硬盘录像机的P2P功能，设置了一个登录模式选择界面 * 
@author wuzhuoyu * 
@Version:V1.00 * 
@Create Date:2018-08-01 *
*/

public class LoginType extends Activity {


	private Button btn_IPLogin;
	private Button btn_P2PLogin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_type);
		
		btn_IPLogin = (Button) findViewById(R.id.IP);
		btn_IPLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(LoginType.this, IPLoginActivity.class);
				startActivity(i);
			}
		});
		
		btn_P2PLogin = (Button) findViewById(R.id.P2P);
		btn_P2PLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(LoginType.this, P2PLoginActivity.class);
				startActivity(i);
			}
		});
	}


}
