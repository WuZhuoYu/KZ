package com.example.kxapp;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.kxapp.R;
import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.ListViewAdapter;
import com.example.service.PreferencesService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @Name:政府端事件显示 *
 * @Description: *
 * @author leisiyu*
 * @Version:V1.00 *
 * @Create Date:2013-06-10 *
 */
public class ZF_EventActivity extends Activity {

	private ListView lv_event;
	private ArrayList<HashMap<String, Object>> eventinfo;
	private String serverIP = "192.168.16.75:8080";
	private PreferencesService preservice;
	private String cmd;
	private Map<String, Object> reqparams;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_zf__event);
		initView();

	}

	private void initView() {
		// 获取网络配置参数
		preservice = new PreferencesService(getApplicationContext());
		Map<String, String> params = preservice.getPreferences();
		String serverip = params.get("serviceIP");
		if (serverip.equals("")) {
			preservice.save("192.168.16.75:8080", "192.168.16.245", "502", "61.157.134.34", "8082", "admin",
					"cdjx1234cdjx1234");
		} else {
			serverIP = serverip;
		}

		lv_event = (ListView) findViewById(R.id.zf_event);
		// 返回点击事件
		findViewById(R.id.back).setOnClickListener(new OnClickListener() {

			private boolean Thread = false;

			@Override
			public void onClick(View v) {
				Thread = false;
				finish();
			}
		});
		
		findViewById(R.id.zf_old_eventdata).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(ZF_EventActivity.this, ZF_Old_EventDataActivity.class);
				startActivity(intent);
			}
		});

		cmd = FinalConstant.REQUEST_EVENTINFO;
		reqparams = new HashMap<String, Object>();
		reqparams.put("cmd", cmd);
		new Thread(runnable).start();

	}


	// 请求事件处理信息
	Runnable runnable = new Runnable() {

		@Override
		public void run() {
			String path = "http://" + serverIP + "/AppService.php";
			try {
				String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
				Log.d("debugTest", "reqdata -- " + reqdata);
				if (reqdata != null) {
					Message msg = mhandler.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
					Bundle bundle = new Bundle();
					bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);
					msg.setData(bundle);
					mhandler.sendMessage(msg);
				}
				// 信息请求时间周期
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
	Handler mhandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			String jsondata = msg.getData().getString(FinalConstant.GT_BACK_INFO);
			if (jsondata.equals("1")) {
				Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
			} else {
				try {
					JSONArray arr = new JSONArray(jsondata);
					JSONObject tmp_cmd = (JSONObject) arr.get(0);
					String str_cmd = tmp_cmd.getString("cmd");
					int len = 0;
					len = arr.length();
					if (len > 1) {
						if (str_cmd.equals(FinalConstant.REQUEST_EVENTINFO_REBACK)) {
//							if (!arr.get(1).equals(false)) {
								eventinfo = new ArrayList<HashMap<String, Object>>();
								for (int i = 1; i <= (arr.length() - 1); i++) {
									JSONObject data = (JSONObject) arr.get(i);
									HashMap<String, Object> eventdata = new HashMap<String, Object>();

									eventdata.put("id", data.get("id"));
									eventdata.put("TAG", data.getString("TAG"));
									eventdata.put("event_name", data.getString("event_name"));
									eventdata.put("event_type", data.getString("event_type"));
									eventdata.put("event_description", data.getString("event_description"));
									eventdata.put("image", data.getString("image"));
									eventdata.put("report_person", data.getString("report_person"));
									eventdata.put("accept_unit", data.getString("accept_unit"));
									// eventdata.put("rbase", URLEncoder.encode(data.getString("TAG"),"UTF-8"));
									eventdata.put("report_time", data.getString("report_time"));
									eventdata.put("tid", data.getString("tid"));
									eventinfo.add(eventdata);
									Log.d("hello", "---第二最新----" + eventdata);
									// Log.d("hello", "---eventinfo----"+eventinfo.toString());
									
								}
								info(eventinfo);
							}
						}
//					}
				} catch (Exception e) {
					e.printStackTrace();
					// } catch (UnsupportedEncodingException e) {
					// e.printStackTrace();
				}
			}
		}

		private void info(final ArrayList<HashMap<String, Object>> eventinfo) {
			if (eventinfo != null) {
				
				ListViewAdapter adapter = new ListViewAdapter(ZF_EventActivity.this, eventinfo);
				Log.d("hello", "---第2次最新----"+adapter+toString());
				lv_event.setAdapter(adapter);
//				Log.d("hello", "---第三次最新----"+adapter+toString());
				lv_event.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						TextView tv_base = (TextView) view.findViewById(R.id.tv_base);
						TextView tv_info = (TextView) view.findViewById(R.id.tv_info);
						TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
						TextView tv_time = (TextView) view.findViewById(R.id.tv_time);
						TextView tv_accept_unit = (TextView) view.findViewById(R.id.tv_accept_unit);
						
						String TAG = tv_base.getText().toString();
						String event_description = tv_info.getText().toString();
						String report_person = tv_name.getText().toString();
						String report_time = tv_time.getText().toString();
						String accept_unit = tv_accept_unit.getText().toString();

						HashMap<String, Object> event = eventinfo.get(position);

						Bundle bundle = new Bundle();
						bundle.putString("TAG", (String) event.get("TAG"));// 基地名称
						bundle.putString("event_name", (String) event.get("event_name"));// 事件名称
						bundle.putString("event_type", (String) event.get("event_type"));// 事件类型
						bundle.putString("event_description", (String) event.get("event_description"));// 事件信息
						bundle.putString("report_person", (String) event.get("report_person"));// 上报人
						bundle.putString("accept_unit", (String) event.get("accept_unit"));// 接收单位
						bundle.putString("report_time", (String) event.get("report_time"));// 上报时间
						bundle.putString("image", (String) event.get("image"));// 图片
						// bundle.putString("TAG", (String) event.get("rtag"));//身份判定标识
						Intent intent = new Intent();
						intent.putExtras(bundle);
						intent.setClass(ZF_EventActivity.this, ZF_Event_detailsActivity.class);
						startActivity(intent);
					}

				});
			} else {
				Toast.makeText(getApplicationContext(), "请求不到网络数据！", Toast.LENGTH_SHORT).show();
			}
			
		}
	};

}
