package com.example.kxapp;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.login.LoginException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.baidu.mapapi.search.core.e;
import com.company.NetSDK.DEVICE_NET_INFO_EX;
import com.company.NetSDK.EM_LOGIN_SPAC_CAP_TYPE;
import com.company.NetSDK.FinalVar;
import com.company.NetSDK.INetSDK;
import com.company.NetSDK.NET_DEVICEINFO_Ex;
import com.example.kxapp.DialogProgress;
import com.example.kxapp.PrefsConstants;
import com.example.kxapp.R;
import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.NetSDKLib;
import com.example.service.PreferencesService;
import com.example.service.ToolKits;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.nfc.cardemulation.HostApduService;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

/**
 * @Name:大华硬盘录像机登录页面 *
 * @Description:*
 * @author:wuzhuoyu *
 * @Version:V1.00 * 按照大华的Demo做修改的登录机制，主要功能就是携带设备信息、IP信息、端口信息、账号密码与硬盘录像机搭建联系
 * @version:V2.00 * 做了一个域名解析，通过解析花生壳注册的域名，得到公网的实时动态IP，将这个动态IP传入到登录逻辑中
 * @version:V3.00 * 根据客户的要求修改了，端口号，对外展示为基地名称，内部转换为端口号。取消了ckeckbox控件，禁止了用户自行修改参数，所以部分参数设置在xml属性中。
 * @Create Date:2018-07-26 *
 */
public class IPLoginActivity extends Activity {

	private Map<String, Object> reqparams;
	private View startVideoAnimPage = null;
	private ImageView loadingVideo = null;
	private Animation loadingAnim = null;

	private String flag;
	private TextView back;
	private SharedPreferences sp;

	private EditText mEditTextOrg;
	private Spinner mEditTextPort;
	private EditText mEditTextUsername;
	private Button ButtonLogin;
	private EditText mEditTextPassword;

	private String LoginOrg;
	private String LoginPort;
	private String LoginUsername;
	private String LoginPassword;

	protected ParaActivity videoIP;
	protected ParaActivity videoPort;
	protected ParaActivity videoUser;
	protected ParaActivity videoPsw;

	private Resources res;

	public static final int NET_USER_FLASEPWD_TRYTIME = FinalVar.NET_USER_FLASEPWD_TRYTIME;
	public static final int NET_LOGIN_ERROR_PASSWORD = FinalVar.NET_LOGIN_ERROR_PASSWORD;
	public static final int NET_LOGIN_ERROR_USER = FinalVar.NET_LOGIN_ERROR_USER;
	public static final int NET_LOGIN_ERROR_TIMEOUT = FinalVar.NET_LOGIN_ERROR_TIMEOUT;
	public static final int NET_LOGIN_ERROR_RELOGGIN = FinalVar.NET_LOGIN_ERROR_RELOGGIN;
	public static final int NET_LOGIN_ERROR_LOCKED = FinalVar.NET_LOGIN_ERROR_LOCKED;
	public static final int NET_LOGIN_ERROR_BLACKLIST = FinalVar.NET_LOGIN_ERROR_BLACKLIST;
	public static final int NET_LOGIN_ERROR_BUSY = FinalVar.NET_LOGIN_ERROR_BUSY;
	public static final int NET_LOGIN_ERROR_CONNECT = FinalVar.NET_LOGIN_ERROR_CONNECT;
	public static final int NET_LOGIN_ERROR_NETWORK = FinalVar.NET_LOGIN_ERROR_NETWORK;

	public NET_DEVICEINFO_Ex mDeviceInfo;// 设备参数

	protected Runnable query_control;
	private int mErrorCode = 0;
	private long mLoginHandle;// 登录返回参数
	protected String ip = "123456";
	// Dns dns = new Dns();
	private String[] item = new String[] { "临江镇", "四圣村", "乌杨村", "南雅镇", "赵家街道" };
	private String[] item1 = new String[] { "临江镇" };
	private String[] item2 = new String[] { "四圣村" };
	private String[] item3 = new String[] { "乌杨村" };
	private String[] item4 = new String[] { "南雅镇" };
	private String[] item5 = new String[] { "赵家街道" };

	private SharedPreferences mSharedPrefs;
	private CheckBox checkBox;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Intent intent = getIntent();
		// flag = intent.getStringExtra("flag");
		flag = Flag.getFlag();
		res = getResources();
		NetSDKLib.getInstance().init();// SDK初始化
		setContentView(R.layout.activity_iplogin);
		// 调用控件
		mEditTextOrg = (EditText) findViewById(R.id.editTextServerOrg);
		mEditTextPort = (Spinner) findViewById(R.id.editTextServerPort);
		mEditTextUsername = (EditText) findViewById(R.id.editTextUsername);
		mEditTextPassword = (EditText) findViewById(R.id.editTextPassword);
		ButtonLogin = (Button) findViewById(R.id.buttonLogin);
		// checkBox = (CheckBox) findViewById(R.id.checkBox);
		// checkBox = (CheckBox)findViewById(R.id.checkBox);
		// output();

		// 返回点击事件
		back = (TextView) this.findViewById(R.id.back);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		// 选择基地
		mEditTextPort.setPrompt("请选择基地");
		if ("0".equals(flag) || "6".equals(flag)) {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, item);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mEditTextPort.setAdapter(adapter);
		}
		if (flag.equals("1")) {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, item1);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mEditTextPort.setAdapter(adapter);
		}
		if (flag.equals("2")) {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, item2);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mEditTextPort.setAdapter(adapter);
		}
		if (flag.equals("3")) {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, item3);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mEditTextPort.setAdapter(adapter);
		}
		if (flag.equals("4")) {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, item4);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mEditTextPort.setAdapter(adapter);
		}
		if (flag.equals("5")) {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, item5);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mEditTextPort.setAdapter(adapter);
		}

		mEditTextPort.setOnItemSelectedListener(new OnItemSelectedListener() {
			private String jdType;

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// jdType = parent.getSelectedItem().toString();
				if ("0".equals(flag) || "6".equals(flag)) {
					if (position == 0) {
						LoginPort = "5002";
						LoginTaskOnclick(); // 添加登录点击事件
					}
					if (position == 1) {
						LoginPort = "6002";
						LoginTaskOnclick(); // 添加登录点击事件
					}
					if (position == 2) {
						LoginPort = "7002";
						LoginTaskOnclick(); // 添加登录点击事件
					}
					if (position == 3) {
						LoginPort = "8012";
						LoginTaskOnclick(); // 添加登录点击事件
					}
					if (position == 4) {
						LoginPort = "9002";
						LoginTaskOnclick(); // 添加登录点击事件
					}
				} else {
					if (flag.equals("1")) {
						if (position == 0) {
							LoginPort = "5002";
							LoginTaskOnclick(); // 添加登录点击事件
						}
					}
					if (flag.equals("2")) {
						if (position == 0) {
							LoginPort = "6002";
							LoginTaskOnclick(); // 添加登录点击事件
						}
					}
					if (flag.equals("3")) {
						if (position == 0) {
							LoginPort = "7002";
							LoginTaskOnclick(); // 添加登录点击事件
						}
					}
					if (flag.equals("4")) {
						if (position == 0) {
							LoginPort = "8012";
							LoginTaskOnclick(); // 添加登录点击事件
						}
					}
					if (flag.equals("5")) {
						if (position == 0) {
							LoginPort = "9002";
							LoginTaskOnclick(); // 添加登录点击事件
						}
					}
				}
			}
		});

	}

	// private void output() {
	// SharedPreferences sp = getSharedPreferences("LoginData", MODE_PRIVATE);
	// String LoginOrg = sp.getString("LoginOrg", "222.182.192.72");
	// String LoginPort = sp.getString("LoginPort", "");
	// String LoginUsername = sp.getString("LoginUsername", "admin");
	// String LoginPassword = sp.getString("LoginPassword", "admin");
	// boolean ischecked1 = sp.getBoolean("isChecked", false);
	//
	// mEditTextOrg.setText(LoginOrg);
	// mEditTextPort.setText(LoginPort);
	// mEditTextUsername.setText(LoginUsername);
	// mEditTextPassword.setText(LoginPassword);
	// checkBox.setChecked(ischecked1);
	// }

	// private void input() {
	// SharedPreferences sp = getSharedPreferences("LoginData", MODE_PRIVATE);
	// SharedPreferences.Editor editor = sp.edit();
	// if(checkBox.isChecked()) {
	// getSharedPreferences("LoginData", MODE_PRIVATE);
	// editor.putString("LoginOrg",LoginOrg);
	// editor.putString("LoginPort", LoginPort);
	// editor.putString("LoginUsername", LoginUsername);
	// editor.putString("LoginPassword", LoginPassword);
	//
	// editor.putBoolean("checkBox", checkBox.isChecked());
	// Toast.makeText(getApplicationContext(), "保存里面", 0).show();
	// editor.commit();
	// }else {
	// Toast.makeText(getApplicationContext(),
	// "未勾选保存登录参数",Toast.LENGTH_LONG).show();
	// }
	// }
	public NET_DEVICEINFO_Ex getDeviceInfo() { // 携带设备信息
		return mDeviceInfo;
	}

	public long getLoginHandle() {
		return this.mLoginHandle;// 携带登录返回参数（返回值为0时无法登陆）
	}

	// 登录响应事件
	private void LoginTaskOnclick() {
		// TODO Auto-generated method stub
		ButtonLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				LoginOrg = mEditTextOrg.getText().toString();
				// LoginPort = mEditTextPort.toString();
				LoginUsername = mEditTextUsername.getText().toString();
				LoginPassword = mEditTextPassword.getText().toString();
				if (LoginOrg.length() != 0 && LoginPort.length() != 0 && LoginUsername.length() != 0
						&& LoginPassword.length() != 0) {
					LoginHandle();
				} else {
					Toast.makeText(getApplicationContext(), "登录参数不能为空", Toast.LENGTH_SHORT).show();
				}

				// getSharePrefs();
				//
				// if (checkLoginEditText()) {
				//// new Thread(DNS_Handle).start();
				// LoginHandle();
				// }else {
				//
				// getSharePrefs();
				// }

			}

			public boolean logout() {
				if (0 == mLoginHandle) {
					return false;
				}

				boolean retLogout = INetSDK.Logout(mLoginHandle);
				if (retLogout) {
					mLoginHandle = 0;
				}

				return retLogout;
			}

			public int errorCode() {
				return mErrorCode;
			}

		});
	}

	// 开启子线程进行域名解析（可用的）
	// private Runnable DNS_Handle = new Runnable() {
	//
	// @Override
	// public void run() {
	//
	// try {
	// String ip = dns.Dns_handle();
	//
	// Message msg = mhandler.obtainMessage(1);
	// Bundle bundle = new Bundle();
	// bundle.putString("ip", ip);// 将reqdata填充入句柄
	// msg.setData(bundle);// 设置一个任意数据值的Bundle对象。
	// mhandler.sendMessage(msg);
	// Thread.sleep(1000);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// }
	//
	// };

	/**
	 * @Name:录像机登录逻辑*
	 * @Description: 获取到域名解析的IP，将IP传入到该方法*
	 * @author wuzhuoyu *
	 * @Version:V2.00 *
	 * @Create Date:2018-8-12 *
	 */
	// private void LoginHandle(String ip) {
	private void LoginHandle() {
		Integer err = new Integer(0);
		mDeviceInfo = new NET_DEVICEINFO_Ex();// 实列化设备信息
		mLoginHandle = INetSDK.LoginEx2(LoginOrg, Integer.parseInt(LoginPort), LoginUsername, LoginPassword, 0, null,
				mDeviceInfo, err); // 登录句柄
		// Log.d("DebugTest", "设备返回值-----" + mDeviceInfo);

		if (0 == mLoginHandle) {
			mErrorCode = INetSDK.GetLastError();
			Toast.makeText(IPLoginActivity.this, "请检查设备是否正常工作", Toast.LENGTH_LONG).show();
			Log.d("DebugTest", "进入未登录逻辑函数--------" + mDeviceInfo);
		} else {
			// input();
			// 登录句柄和设备信息是结构体，登录成功后需要设置，否者会报空指针
			NetSDKApplication.getInstance().setLoginHandle(mLoginHandle);
			NetSDKApplication.getInstance().setDeviceInfo(mDeviceInfo);

			// 将获取的参数传到播放页面
			Intent intent = new Intent();
			intent.putExtra("IP", LoginOrg);
			// intent.putExtra("Port", LoginPort);
			intent.putExtra("User", LoginUsername);
			intent.putExtra("Psw", LoginPassword);
			intent.putExtra("DeviceINFO", mDeviceInfo.toString());
			intent.putExtra("LoginHandle", Long.toString(mLoginHandle));// mLoginHandle是一个long型，传值到播放页面时候需要转换为String类型才能在下一个页面使用

			intent.setClass(IPLoginActivity.this, LivePreviewActivity.class);
			startActivity(intent);
			Log.d("DebugTest", "------返回设备ID--------" + mLoginHandle + mDeviceInfo);
		}
	};

	// private Handler mhandler = new Handler() {
	// @SuppressLint("HandlerLeak")
	// @Override
	// public void handleMessage(Message msg) {
	// if (msg.what == 1) {
	//
	// String ip = msg.getData().getString("ip");
	// Toast.makeText(IPLoginActivity.this, "IP:" + ip, Toast.LENGTH_LONG).show();
	// if (ip.length() != 0 && LoginPort.length() != 0 && LoginUsername.length() !=
	// 0
	// && LoginPassword.length() != 0) {
	// LoginHandle(ip);// 调用登录逻辑方法
	//
	// } else {
	// Toast.makeText(IPLoginActivity.this, "编辑框不能为空", Toast.LENGTH_LONG).show();
	//
	// }
	// }
	// }
	// };

	// 创建一个域名解析的类函数，这里的到的IP传到登录逻辑里面(可用的代码)
	// class Dns {
	// public String Dns_handle() {
	// String ip = "";
	// InetAddress addresses;
	// try {
	// addresses = InetAddress.getByName(LoginOrg);
	// ip = addresses.getHostAddress();
	// } catch (UnknownHostException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// return ip;
	// }
	// }
	//
	// private void putSharePrefs() {
	// mSharedPrefs = getPreferences(MODE_PRIVATE);
	// SharedPreferences.Editor editor = mSharedPrefs.edit();
	// Toast.makeText(getApplicationContext(), "登录参数保存成功",
	// Toast.LENGTH_LONG).show();
	// if (checkBox.isChecked()) {
	// editor.putString(PrefsConstants.LOGIN_IP, LoginOrg);
	// editor.putString(PrefsConstants.LOGIN_PORT, LoginPort);
	// editor.putString(PrefsConstants.LOGIN_USERNAME, LoginUsername);
	// editor.putString(PrefsConstants.LOGIN_PASSWORD, LoginPassword);
	// editor.putBoolean(PrefsConstants.LOGIN_CHECK, true);
	// }
	// editor.commit();
	// }
	//
	// private void getSharePrefs() {
	// mSharedPrefs = getPreferences(MODE_PRIVATE);
	// SharedPreferences.Editor editor = mSharedPrefs.edit();
	// mEditTextOrg.setText(mSharedPrefs.getString(PrefsConstants.LOGIN_IP,
	// "222.182.190.73"));
	// mEditTextPort.setText(mSharedPrefs.getString(PrefsConstants.LOGIN_PORT,
	// "5002"));
	// mEditTextUsername.setText(mSharedPrefs.getString(PrefsConstants.LOGIN_USERNAME,
	// "admin"));
	// mEditTextPassword.setText(mSharedPrefs.getString(PrefsConstants.LOGIN_PASSWORD,
	// "admin"));
	// checkBox.setChecked(mSharedPrefs.getBoolean(PrefsConstants.LOGIN_CHECK,
	// false));
	//
	// editor.commit();
	// }
	//
	//
	// private boolean checkLoginEditText() {
	// LoginOrg = mEditTextOrg.getText().toString();
	// LoginPort = mEditTextPort.getText().toString();
	// LoginUsername = mEditTextUsername.getText().toString();
	// LoginPassword = mEditTextPassword.getText().toString();
	//
	// if(LoginOrg.length() == 0) {
	// ToolKits.showMessage(IPLoginActivity.this,
	// res.getString(R.string.activity_iplogin_ip_empty));
	// return false;
	// }
	// if(LoginPort.length() == 0) {
	// ToolKits.showMessage(IPLoginActivity.this,
	// res.getString(R.string.activity_iplogin_port_empty));
	// return false;
	// }
	// if(LoginUsername.length() == 0) {
	// ToolKits.showMessage(IPLoginActivity.this,
	// res.getString(R.string.activity_iplogin_username_empty));
	// return false;
	// }
	// if(LoginPassword.length() == 0) {
	// ToolKits.showMessage(IPLoginActivity.this,
	// res.getString(R.string.activity_iplogin_password_empty));
	// return false;
	// }
	//
	// try {
	// Integer.parseInt(LoginPort);
	// } catch (Exception e) {
	// e.printStackTrace();
	// ToolKits.showMessage(IPLoginActivity.this,
	// res.getString(R.string.activity_iplogin_port_err));
	// return false;
	// }
	//
	// return true;
	// }
}
