package com.example.kxapp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.kxapp.R;
import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PlantService;
import com.example.service.PreferencesService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
/**
@Name:用户端事件上报 *
@Description: * 
@author leisiyu * 
@Version:V1.00 * 
@Create Date:2018-06-06 *
*/

public class YH_EventActivity extends Activity {
	private boolean nThread;
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		nThread = false;
	}
	/**返回*/
    private TextView back;
	 
	private EditText event_reason;
	private EditText event_description;
	private EditText report_person;
	private EditText accept_unit;
	private EditText report_time;
	private EditText event_name;
	private Button   btn_paraSave;
	private ImageView im_event;
	private Button btn_select;
	private PreferencesService  preservice;
	private String   cmd;
	private Map<String, Object>  reqparams;
	int selectedRow = 0;  
    int ActivityID=1;
	private Spinner sp_eventype;
    /**服务器地址*/	
	private String serverIP = "192.168.16.75:8080";
	private EditText mEditText;
	private String      m_oIPAddr = "61.157.134.34";
	private String		m_oPort	="8081"				;
	private String		m_oUser ="admin";
	private String		m_oPsd = "cdjx1234cdjx1234";
	protected static final int REQUEST_CODE_IMAGE = 1;//请求图库
	protected static final int REQUEST_CODE_CAMERA = 2;//请求相机
	private int digree=0;
	private String flag;
	private String Tag;
	private String[]items=new String[] {"墒情","苗情","虫情","灾情","养分检测","其他"};
	//系统相册的路径
	String path=Environment.getExternalStorageDirectory()+File.separator+Environment.DIRECTORY_DCIM+File.separator;
	private Uri photoUri;
	private String picturePath;
	private Bitmap mge;
	private String str;
	private String event_type;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event);
		//获取身份标识
//		Intent intent=getIntent();
//		flag = intent.getStringExtra("flag");
		flag=Flag.getFlag();
		setTag();
		
		//事件选择器
		mEditText = (EditText) findViewById(R.id.report_time);  
        mEditText.setOnTouchListener(new OnTouchListener() {  
        	
            @Override  
            public boolean onTouch(View v, MotionEvent event) {  
                if (event.getAction() == MotionEvent.ACTION_DOWN) {  
                    showDatePickDlg();  
                    return true;  
                }  
                return false;  
            }
        });
        //声明控件
		event_description =  (EditText) this.findViewById(R.id.event_description);
		im_event = (ImageView) findViewById(R.id.im_content);
		btn_select = (Button) findViewById(R.id.btn_select_image);
		report_person =  (EditText) this.findViewById(R.id.report_person);
		accept_unit =  (EditText) this.findViewById(R.id.accept_unit);
		report_time =  (EditText) this.findViewById(R.id.report_time);
		event_name = (EditText) findViewById(R.id.event_name);
		btn_paraSave =  (Button) this.findViewById(R.id.paraSave);
		sp_eventype = (Spinner) findViewById(R.id.sp_eventype);
		btn_paraSave.setOnClickListener(new HsSubmitOnClickListener());
		back  = (TextView) this.findViewById(R.id.back);
		
		//拍照，图库选择点击事件
		btn_select.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder=new AlertDialog.Builder(YH_EventActivity.this);
				String [] item=new String[] {"从手机相册选取","拍照"};
				builder.setItems(item, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
							if(which==0) {
								//打开图库
								Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
								startActivityForResult(intent,REQUEST_CODE_IMAGE);
							}
							if(which==1) {
								//打开相机
								String state=Environment.getExternalStorageState();//只有在SD卡状态为MEDIA_MOUNTED时/mnt/sdcard目录才是可读可写，并且可以创建目录及文件。
								if(state.equals(Environment.MEDIA_MOUNTED)) {
									File file =new File(path);
									if(file.exists()) {
										file.mkdir();
									}
									String imagename=getphotoname()+".jpg";
									Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
									photoUri = Uri.fromFile(new File(path + imagename));
									intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
									startActivityForResult(intent, REQUEST_CODE_CAMERA );
								}
							}
					}

				});
				builder.show();
			}
		});
		
		//返回点击事件
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		 mEditText.setOnFocusChangeListener(new OnFocusChangeListener() {  
			  
	            @Override  
	            public void onFocusChange(View v, boolean hasFocus) {  
	                if (hasFocus) {  
	                    showDatePickDlg();  
	                }  
	            }  
	        });
		 
		 //事件类型选择器spinner
		 sp_eventype.setPrompt("请选择事件类型");
		 ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,items);
		 adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 sp_eventype.setAdapter(adapter);
		 sp_eventype.setOnItemSelectedListener(new  OnItemSelectedListener() {
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				event_type= parent.getSelectedItem().toString();
				
			}
		});
		 
		//获取网络配置
		preservice = new PreferencesService(getApplicationContext());
	    Map<String, String> params = preservice.getPreferences();
	    String serviceIP = params.get("serviceIP");
	    String vedioIP = params.get("vedioIP");
	    String vedioPort = params.get("vedioPort");
	    String vedioUser = params.get("vedioUser");
	    String vedioPsw = params.get("vedioPsw");
	    if(serviceIP.equals("")){
	    	preservice.save("192.168.16.75:8080", "192.168.16.245", "502", "61.157.134.34", "8082", "admin", "cdjx1234cdjx1234");
	    }else{
	    	serverIP = serviceIP;
	        m_oIPAddr = vedioIP;
	    	m_oPort	= vedioPort;
	    	m_oUser = vedioUser;
	    	m_oPsd = vedioPsw;
	    }
	    
	
	};
	//照片命名规则
	private String getphotoname() {
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
		return "IMG_" + dateFormat.format(date);

	} 
	//时间选择器
	protected void showDatePickDlg() {  
        Calendar calendar = Calendar.getInstance();  
        DatePickerDialog datePickerDialog = new DatePickerDialog(YH_EventActivity.this, new OnDateSetListener() {  
  
            @Override  
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {  
                YH_EventActivity.this.mEditText.setText(year + "-" + (monthOfYear+1) + "-" + dayOfMonth);  
            }  
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));  
        datePickerDialog.show();  
    }
	/**
	 * 事件信息提交
	 * @author Administrator
	 * 
	 *
	 */
	private class HsSubmitOnClickListener implements View.OnClickListener{
		@Override  
	    public void onClick(View v) {
			   String str="";
			   if(mge!=null&&Tag!=null) {
				   //图片转换格式
				   ByteArrayOutputStream baos=new ByteArrayOutputStream();
		           mge.compress(Bitmap.CompressFormat.JPEG, 10, baos);//100代表图片不压缩，10代表压缩至原有大小的1/10
			       byte []imagedata=baos.toByteArray();
			       str=Base64.encodeToString(imagedata, Base64.DEFAULT);
//				  Toast.makeText(getApplicationContext(), "进来"+str, Toast.LENGTH_LONG).show();
//		    
				   	if((event_description.length()!=0)&(report_person.length()!=0)&(accept_unit.length()!=0)&(report_time.length()!=0)&(event_name.length()!=0)&(event_type!=null)&(str!=null))
					 {
						try {
							 cmd = FinalConstant.PLANTSUBMIT_REQUEST_SERVER;
				             reqparams = new HashMap<String, Object>();	//组织参数
				        	 reqparams.put("cmd", cmd);
				        	 reqparams.put("TAG", URLEncoder.encode(Tag.toString().trim(), "UTF-8"));  //基地标识
				        	 reqparams.put("event_name", URLEncoder.encode(event_name.getText().toString().trim(), "UTF-8"));  //事件名称
				        	 reqparams.put("event_type", URLEncoder.encode(event_type,"UTF-8"));   //事件 类型
				        	 reqparams.put("event_description", URLEncoder.encode(event_description.getText().toString(), "UTF-8"));    //事件描述
				        	 reqparams.put("report_person", URLEncoder.encode(report_person.getText().toString(),"UTF-8"));   //上报人
							 reqparams.put("accept_unit", URLEncoder.encode(accept_unit.getText().toString(), "UTF-8"));   //接收单位
							 reqparams.put("report_time", URLEncoder.encode(report_time.getText().toString(), "UTF-8"));   //上报事件
					         reqparams.put("image",URLEncoder.encode(str, "UTF-8"));   //  图片
//							 Toast.makeText(EventActivity.this, "上传成功1", Toast.LENGTH_LONG).show();
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
			 			}
			        	 //提交图片
						new Thread(query).start();	
			            Toast.makeText(YH_EventActivity.this, "上传成功", Toast.LENGTH_LONG).show();
					 }else{
						Toast.makeText(YH_EventActivity.this, R.string.error, Toast.LENGTH_LONG).show();
					 }
		       }else {
		    	   Toast.makeText(getApplicationContext(), "请选择你上传的图片！", Toast.LENGTH_SHORT).show();
		       }
			
	        }
////循环压缩
//		private void mm(ByteArrayOutputStream baos) {
//		    int options = 100;
//	           while (baos.toByteArray().length/1024>100) {
//	        	   baos.reset();//重置baos即清空baos
//	                options -= 10;//每次都减少10
//	                mge.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
//				
//			}
//	          
//		};
	};
	
	//提交信息子线程
		private Runnable query = new Runnable() {
			@Override
			public void run() {
				//获取参数
				PlantService  serviceip;
				serviceip = new PlantService(getApplicationContext());
			    Map<String, String> params = serviceip.getPreferences();
			    String url = params.get("serviceip");
			    String path = "";
			    if(url.equals("")){
			    	 path ="http://"+serverIP+"/AppService.php";
			    }else{
			    	path ="http://"+serverIP+"/AppService.php";
			    }
				Log.d("debugTest","path -- "+path);
				//String path ="http://120.76.166.185:8000/testapp.php";
				try{
					  String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
					  Log.d("debugTest","reqdata -- "+reqdata);
					  if(reqdata!= null){
						  //子线程用sedMessage()方法传弟)Message对象
						  Message msg = mhandler.obtainMessage(FinalConstant.QUERY_BACK_DATA);
						  Bundle bundle = new Bundle();//创建一个句柄
				          bundle.putString(FinalConstant.BACK_INFO, reqdata);//将reqdata填充入句柄
				          msg.setData(bundle);//设置一个任意数据值的Bundle对象。
						  mhandler.sendMessage(msg);
				  }
				
				}catch(Exception e){
					e.printStackTrace();
				}
			};
		};
		//服务器返回信息
		private Handler mhandler = new Handler() {
			@SuppressLint("HandlerLeak")
			@Override
			public void handleMessage(Message msg) {
				Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_LONG).show();
					//提交成功清空控件
					 event_name.setText("");
					 event_description.setText("");
					 report_person.setText("");
					 accept_unit.setText("");
					 report_time.setText("");
					 im_event.setImageDrawable(null);


//				if (msg.what == FinalConstant.QUERY_BACK_DATA) {
//					String jsonData = msg.getData().getString(FinalConstant.BACK_INFO);
//					Log.d("debugtest", "---最新---"+jsonData);
//					try {
//							if(jsonData.equals("1"))
//							{
//								Toast.makeText(YH_EventActivity.this, "服务器没有开启或异常", Toast.LENGTH_LONG).show();
//							}else{
//								JSONArray arr = new JSONArray(jsonData);  //收到JSON数组对象解析
//							    JSONObject tmp_cmd = (JSONObject) arr.get(0);	//获取json数组对象返回命令
//								String str_cmd= tmp_cmd.getString("cmd");
//							    Log.d("debugTest","arr_data -- "+arr);
//							    int len = 0;
//							    len = arr.length();
//							    Log.d("debugTest","len -- "+len);
//							    if(len>1)
//							    {
//
//								    if(str_cmd.equals(FinalConstant.PLANTSUBMIT_REBACK_SERVER))
//								    {
//
//								    	JSONObject result_cmd = (JSONObject) arr.get(1);
//								    	if(result_cmd.getString("RESULT").equals("SUCCESS")){
//								    		Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_LONG).show();
//								    		//提交成功清空控件
//								    		 event_description.setText("");
//								    		 report_person.setText("");
//								    		 accept_unit.setText("");
//								    		 report_time.setText("");
//								    		 im_event.setImageDrawable(null);
//								    	}
//								    }
//							    }
//							}
//					} catch (JSONException e) {
//							e.printStackTrace();
//						}
//				}
		    };
		};

		private Bitmap smge;

		private byte[] imagedata;
//
//		private String str;
		//相册返回图片
		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			super.onActivityResult(requestCode, resultCode, data);
			if((requestCode==REQUEST_CODE_IMAGE)&&(resultCode==RESULT_OK)) {
				   Uri selectedImage = data.getData();
		           String[] filePathColumn = { MediaStore.Images.Media.DATA };//查询我们需要的数据
		           Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
		           cursor.moveToFirst();
		           int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		           picturePath = cursor.getString(columnIndex);
		           cursor.close();
		           
		           mge=BitmapFactory.decodeFile(picturePath);
		           ExifInterface exifInterface=null;
		           try {
					exifInterface =new ExifInterface(picturePath);
				} catch (IOException e) {
					exifInterface=null;
					e.printStackTrace();
				}
		        if(exifInterface!=null) {
		             int ori = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED); 
		        switch (ori) {
		        case ExifInterface.ORIENTATION_ROTATE_90:  
	                digree = 90;  
	                break;  
	            case ExifInterface.ORIENTATION_ROTATE_180:  
	                digree = 180;  
	                break;  
	            case ExifInterface.ORIENTATION_ROTATE_270:  
	                digree = 270;  
	                break;  
	            default:  
	                digree = 0;  
	                break;  
	            }
					}
		       
		        if(digree!=0) {
		        	 Matrix m = new Matrix();  
		             m.postRotate(digree);
		             Bitmap bitmap=null;
		             bitmap = Bitmap.createBitmap(mge, 0, 0, mge.getWidth(),mge.getHeight(), m, true); 	
		             im_event.setImageBitmap(bitmap);
		        }else {
		        	im_event.setImageBitmap(mge);
		        }   
		        
		           
			}
			if((requestCode==REQUEST_CODE_CAMERA)&&(resultCode==RESULT_OK)) {

				 Uri uri = null;
				 if (data != null && data.getData() != null) {
					 uri = data.getData();
					 }
				 if (uri == null) {
					 if (photoUri != null) {
						 uri = photoUri;
						 }
					 }
				 picturePath=photoUri.getPath();
				 mge = BitmapFactory.decodeFile(picturePath);
//				 smge = BitmapFactory.decodeFile(picturePath);
//				 
//				 int width = smge.getWidth();
//				 int height = smge.getHeight();
//				 int newWidth = 50;
//				 int newHeight = 50;
//				// 计算缩放比例
//
//				 float scaleWidth = ((float) newWidth) / width;
//
//				 float scaleHeight = ((float) newHeight) / height;
//				 // 取得想要缩放的matrix参数
//
//				 Matrix matrix = new Matrix();
//
//				 matrix.postScale(scaleWidth, scaleHeight);
//				// 得到新的图片
//
//				 Bitmap mge = Bitmap.createBitmap(smge, 0, 0,newWidth, newHeight, matrix,true);
////				 Toast.makeText(getApplicationContext(), "长："+width+"宽:"+height,Toast.LENGTH_LONG).show();
		         ExifInterface exifInterface=null;
		           try {
					exifInterface =new ExifInterface(picturePath);
				} catch (IOException e) {
					exifInterface=null;
					e.printStackTrace();
				}
		       //   获取图片的旋转角度

		        if(exifInterface!=null) {
		             int ori = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED); 
		       switch (ori) {
		      case ExifInterface.ORIENTATION_ROTATE_90:  
	              digree = 90;  
	              break;  
	          case ExifInterface.ORIENTATION_ROTATE_180:  
	              digree = 180;  
	              break;  
	          case ExifInterface.ORIENTATION_ROTATE_270:  
	              digree = 270;  
	              break;  
	          default:  
	              digree = 0;  
	              break;  
	          }
					}
		       
		        if(digree!=0) {
		        	 Matrix m = new Matrix();  
		             m.postRotate(digree);
		             Bitmap bitmap=null;
		             bitmap = Bitmap.createBitmap(mge, 0, 0, mge.getWidth(),mge.getHeight(), m, true); 
		             im_event.setImageBitmap(bitmap);
		        }else {
//		        	compressImage(mge, str);
		        	im_event.setImageBitmap(mge);
		        }  
			}
		}
//		 // 质量压缩法：
//	    private Bitmap compressImage(Bitmap mge, String str) {
//	        try {
//	            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//	            mge.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
//	            int options = 100;
//	            while (baos.toByteArray().length / 1024 > 50) {    //循环判断如果压缩后图片是否大于100kb,大于继续压缩
//	                baos.reset();//重置baos即清空baos
//	                options -= 100;//每次都减少10
//	                Log.d("hello", "---最新----");
//	                mge.compress(Bitmap.CompressFormat.JPEG, options, baos);  //这里压缩options%，把压缩后的数据存放到baos中
////	                byte []imagedata=baos.toByteArray();
////	                str = Base64.encodeToString(imagedata, Base64.DEFAULT);
////	                im_event.setImageBitmap(mge);
//	            }
//	            ByteArrayOutputStream isBm = new ByteArrayOutputStream(baos.toByteArray());
//	            //压缩好后写入文件中
//	            return mge;
//	      
//	        } catch (Exception e) {
//	            e.printStackTrace();
//	            return null;
//	        }
//	    }
    //基地标识信息判定
	public void setTag() {
		if("1".equals(flag)) {
			Tag="LJ";
		}else if("2".equals(flag)) {
			Tag="SS";
		}else if("3".equals(flag)) {
			Tag="WY";
		}else if("4".equals(flag)) {
			Tag="NY";
		}else if("5".equals(flag)) {
			Tag="ZJ";
		};
	}
}
