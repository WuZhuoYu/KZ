package com.example.widget;

import com.example.kxapp.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MapDialog extends RelativeLayout{
	
	

	private TextView mCompanyProfile;
	private TextView mSensorData;

	public MapDialog(final Context context) {
		super(context);
		//加载布局
		LayoutInflater.from(context).inflate(R.layout.map_dialog, this);
		
		mCompanyProfile = (TextView) findViewById(R.id.tv_Companyprofile);
		mSensorData = (TextView) findViewById(R.id.tv_Sensordata);
	}
		
}
